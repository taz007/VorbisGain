#ifndef VG_VORBISFILEHELPER_H
#define VG_VORBISFILEHELPER_H

#include <string>

#include <vorbis/vorbisfile.h>

#include "non_copy.h"

namespace VG
{
    class AutoOggVorbisFile : non_copy_moveable
    {
      public:
        explicit AutoOggVorbisFile(const std::string& filename);
        ~AutoOggVorbisFile();

        OggVorbis_File* get();
        ogg_int64_t pcm_total(int bitstream);
        ogg_int64_t pcm_tell();
        ::vorbis_info* info(int bitstream);
        long streams();
        vorbis_comment* comment(int bitstream);

      private:
        OggVorbis_File m_ovfile;
    };

} // namespace VG
#endif
