#include "vorbis.h"

#include <algorithm>
#include <charconv>
#include <cmath>
#include <iomanip>
#include <memory>
#include <string>
#include <string_view>
#include <vector>

#include <vorbis/codec.h>

#include "c_calls.h"
#include "gain_analysis.h"
#include "globals.h"
#include "iofile.h"
#include "meta_ts.h"
#include "misc.h"
#include "path_to.h"
#include "path_to_stat.h"
#include "print.h"
#include "stringify.h"
#include "unreachable.h"
#include "vcedit.h"
#include "vorbisfilehelper.h"
#include "vorbisgainexception.h"

namespace VG
{
    static constexpr std::string_view TEMP_NAME_TEMPLATE = "vorbisgain.tmpXXXXXX";

    std::string format_peak_to_str(const float& f)
    {
        return Stringify() << std::fixed << std::setw(1) << std::setprecision(8) << f;
    }

    std::string format_gain_to_str(const float& f)
    {
        return Stringify() << std::fixed << std::showpos << std::setw(2) << std::setprecision(2) << f << TAGVALUE_GAIN_SUFFIX;
    }

    template<typename CB>
    bool parse_check_needed_comments(const path_to_a_file& filename, CB cb)
    {
        ifile file(filename);
        vcedit_state state(file);

        const auto& comments = state.getComments();

        for (const auto& pair : comments)
        {
            const bool all_found = cb(pair);
            if (all_found)
                return true;
        }
        return false;
    }

    /**
     * \brief See if a vorbis file contains ReplayGain tags.
     *
     * See if a vorbis file contain replay gain tags. A file is considered to
     * have the tags if all required tags (track gain and peak, optionally album
     * gain and peak) are present.
     *
     * \param filename   name of the file to check.
     * \param album_tags if true, check for the album tags (gain and peek) as
     *                   well.
     * \param any_tags   if true, the presence of any tag will be enough for a
     *                   file to contain tags. album_tags is ignored if any_tags
     *                   is true.
     * \param settings   global settings and data.
     * \return 1 if the file contains all the tags, 0 if the file didn't contain
     *         the tags and -1 if an error occured (in which case a message has
     *         been printed).
     */
    bool has_tags(const path_to_a_file& filename, has_tags_category tagtype)
    {
        bool found_tg = false;
        bool found_tp = false;
        bool found_ag = false;
        bool found_ap = false;

        const auto loop_body_any = [&](const auto& pair)
        {
            const auto& t = pair.first;
            if (t == VG::TAG_TRACK_GAIN || t == VG::TAG_TRACK_PEAK || t == VG::TAG_ALBUM_GAIN || t == VG::TAG_ALBUM_PEAK)
                return true;
            else
                return false;
        };

        const auto loop_body_track = [&](const auto& pair)
        {
            const auto& t = pair.first;
            if (t == VG::TAG_TRACK_GAIN)
                found_tg = true;
            else if (t == VG::TAG_TRACK_PEAK)
                found_tp = true;

            return found_tg && found_tp;
        };

        const auto loop_body_trackalbum = [&](const auto& pair)
        {
            const auto& t = pair.first;
            if (t == VG::TAG_TRACK_GAIN)
                found_tg = true;
            else if (t == VG::TAG_TRACK_PEAK)
                found_tp = true;
            else if (t == VG::TAG_ALBUM_GAIN)
                found_ag = true;
            else if (t == VG::TAG_ALBUM_PEAK)
                found_ap = true;

            return found_tg && found_tp && found_ag && found_ap;
        };

        switch (tagtype)
        {
        case has_tags_category::any:
            return parse_check_needed_comments(filename, loop_body_any);
        case has_tags_category::track:
            return parse_check_needed_comments(filename, loop_body_track);
        case has_tags_category::track_album:
            return parse_check_needed_comments(filename, loop_body_trackalbum);
        }
        unreachable();
    }

    std::optional<float> readfloat_if_possible(const std::string& str)
    {
        float tmpval;
        const auto [p, ec] = std::from_chars(str.data(), str.data() + str.size(), tmpval);
        if (ec == std::errc() && p == str.data() + str.size())
        {
            return tmpval;
        }
        return {};
    }

    std::optional<float> readfloatDB_if_possible(const std::string& str)
    {
        if (str.size() <= TAGVALUE_GAIN_SUFFIX.size())
            return {};

        float tmpval;
        const auto [p, ec] = std::from_chars(str.data(), str.data() + str.size(), tmpval);
        if (ec == std::errc() && p == str.data() + str.size() - TAGVALUE_GAIN_SUFFIX.size() &&
            std::string_view(p) == TAGVALUE_GAIN_SUFFIX)
        {
            return tmpval;
        }

        return {};
    }

    /**
     * \brief Get old style tag values.
     *
     * Get the old style tag values from a file.
     *
     * \param filename   name of the file to check.
     * \param album_tag  if true, require file to contain album gain tag. If
     *                   false, album gain is still returned if present.
     * \param track_gain track gain is stored here.
     * \param track_peak track peak is stored here.
     * \param album_gain album gain is stored here.
     * \param settings   global settings and data.
     * \return 1 if the file contains all the tags and their values coould be
     *         parsed, 0 if the file didn't contain all the tags.
     */
    bool get_old_tags(const path_to_a_file& filename, bool album_tag, std::optional<float>& track_peak,
                      std::optional<float>& track_gain, std::optional<float>& album_gain)
    {
        const auto loop_body_trackalbum = [&](const auto& pair)
        {
            const auto& t = pair.first;
            if (t == VG::TAG_TRACK_GAIN_OLD)
            {
                track_gain = readfloatDB_if_possible(pair.second);
            }
            else if (t == VG::TAG_TRACK_PEAK_OLD)
            {
                track_peak = readfloat_if_possible(pair.second);
            }
            else if (t == VG::TAG_ALBUM_GAIN_OLD)
            {
                album_gain = readfloatDB_if_possible(pair.second);
            }

            return track_gain.has_value() && track_peak.has_value() && album_gain.has_value();
        };

        const bool found_all = parse_check_needed_comments(filename, loop_body_trackalbum);

        if (album_tag)
        {
            return found_all;
        }
        else
        {
            return track_gain.has_value() && track_peak.has_value();
        }
    }

    /**
     * \brief Show/update a simple progress bar for a file.
     *
     * Show/update a simple progress bar for a file. The name will be truncated
     * to fit within the current console's with, if possible.
     *
     * \param vf         Vorbis file being processed.
     * \param filename   name of the file being processed.
     * \param position   position in the file, in percent (0-100).
     */
    void show_progress(const path_to_a_file& filename, unsigned int position)
    {
        static constexpr auto MIN_FILENAME_SIZE = 5U;

        unsigned int width;
        unsigned int height;
        std::string short_name;
        std::string filename_str = filename.str();
        const std::string* name = &filename_str;

        if (get_console_size(&width, &height))
        {
            /* Size of PROGRESS_FORMAT when printed, excluding the filename */
            constexpr unsigned int PROGRESS_FORMAT_SIZE = 8;
            constexpr unsigned int MIN_MIDDLE_TRUNCATE_SIZE = 20;

            if (filename_str.length() + PROGRESS_FORMAT_SIZE > width)
            {
                /* Need room for cursor as well. */
                unsigned int short_length = width - PROGRESS_FORMAT_SIZE - 1;

                /* Not useful to cut it too short. */
                short_length = std::max(short_length, MIN_FILENAME_SIZE + 3);

                /* Need space for "..." */
                short_length -= 3;

                /* Put "..." in the middle if we have "enough" chars available. */
                if (short_length > MIN_MIDDLE_TRUNCATE_SIZE)
                {
                    short_name.assign(filename_str, 0, short_length / 2);
                    short_name += "...";
                    /* Round upwards here, so that all chars are used. */
                    short_name += &filename_str[filename_str.length() - ((short_length + 1) / 2)];
                }
                else
                {
                    short_name = "...";
                    short_name += &filename_str[filename_str.length() - short_length];
                }

                name = &short_name;
            }
        }

        print_flush(Stringify() << std::setw(3) << position << "% - " << *name << '\r');
    }

    /**
     * Get the replaygain and peak value for a file.
     *
     * \param filename   name of the file to get the gain and peak for.
     * \param track_peak where to store track peak value.
     * \param track_gain where to store track gain value.
     * \param settings   global settings and data.
     * \return 0 if successful and -1 if an error occurred (in which case a
     *         message has been printed).
     */
    class show_progress_ctxt : non_copy_moveable
    {
        static constexpr unsigned int minimal_time_interval = 1;

      public:
        explicit show_progress_ctxt(const path_to_a_file& file, ogg_int64_t len)
            : m_file_length(len), m_filename(file), m_previous_percentage(), m_previous_time(0)
        {
        }

        void show_if_needed(unsigned int percent)
        {
            if (m_previous_percentage.has_value() && percent == m_previous_percentage)
                return;

            time_t curr_time;
            time(&curr_time);
            if (percent == 100 || difftime(curr_time, m_previous_time) >= minimal_time_interval)
            {
                show_progress(m_filename, percent);
                m_previous_percentage = percent;
                m_previous_time = curr_time;
            }
        }

        [[nodiscard]] constexpr const ogg_int64_t& length() const noexcept
        {
            return m_file_length;
        }

      private:
        const ogg_int64_t m_file_length;
        const path_to_a_file& m_filename;
        std::optional<unsigned int> m_previous_percentage;
        time_t m_previous_time;
    };

    void get_gain(const path_to_a_file& filename, std::optional<float>& track_peak, std::optional<float>& track_gain,
                  const SETTINGS& settings, ProcessingContext& ctxt, std::unique_ptr<ContextOneAlbum>& albumCtxt)
    {
        try
        {
            // todo move to unsigned type
            std::optional<show_progress_ctxt> show_ctxt;
            int previous_section = 0;
            int current_section;

            AutoOggVorbisFile vf(filename.str());

            ::vorbis_info* vi = vf.info(-1);

            if ((vi->channels != 1) && (vi->channels != 2))
            {
                throw Exception::Generic(VG::Stringify() << "Unsupported number of channels (" << vi->channels << ") in '"
                                                         << filename.str() << "'.");
            }

            const bool isStereo = (vi->channels == 2) ? true : false;

            /* Only initialize gain analysis once per album, when in album mode */
            if (ctxt.first_file || !settings.album)
            {
                albumCtxt = std::make_unique<ContextOneAlbum>();
            }

            const auto rate = static_cast<unsigned int>(vi->rate);
            const auto channels = vi->channels;

            ContextOneSong currentSong(isStereo, rate);

            if (ctxt.first_file)
            {
                if (!settings.quiet)
                {
                    print_line("Analyzing files...");
                    print_line();
                    print_line("   Gain   |  Peak  | Scale | New Peak | Track");
                    print_line("----------+--------+-------+----------+------");
                }

                ctxt.first_file = false;
            }

            if (!settings.quiet && settings.show_progress)
            {
                show_ctxt.emplace(filename, vf.pcm_total(-1));
                show_ctxt->show_if_needed(0);
            }

            while (true)
            {
                float** pcm;
                const long ret = ov_read_float(vf.get(), &pcm, 1024, &current_section);

                if (ret == 0)
                {
                    break;
                }
                else if (ret < 0)
                {
                    throw Exception::Generic(Stringify() << "Error reading stream: " << VorbisErrorToString(ret));
                }
                else
                {
                    /* We can't handle changes in sample rate or number of channels.
                     * Channels would probably be OK (at least 1 to 2 and vice versa),
                     * but... :)
                     */
                    if (current_section != previous_section)
                    {
                        previous_section = current_section;
                        vi = vf.info(-1);

                        if ((channels != vi->channels) || (rate != static_cast<unsigned int>(vi->rate)))
                        {
                            Stringify sgf;
                            sgf << "Can't process chained file '" << filename.str() << "'with changing stream properties (from "
                                << rate << " Hz, " << channels << " channel(s)"
                                << "to " << vi->rate << " Hz, " << vi->channels << " channel(s).";

                            throw Exception::Generic(sgf);
                        }
                    }

                    for (long i = 0; i < ret; ++i)
                    {
                        pcm[0][i] *= 32767.f;

                        if (vi->channels == 2)
                        {
                            pcm[1][i] *= 32767.f;
                        }
                    }

                    currentSong.AnalyzeSamples(pcm, static_cast<unsigned int>(ret));
                }

                if (show_ctxt.has_value())
                {
                    /* ov_time_tell is slow, so we use ov_pcm_tell instead */
                    const ogg_int64_t file_pos = vf.pcm_tell();

                    const auto tmppercentage = (static_cast<double>(file_pos) / static_cast<double>(show_ctxt->length())) * 100;
                    const auto percentage = static_cast<unsigned int>(std::round(tmppercentage));

                    show_ctxt->show_if_needed(percentage);
                }
            }

            albumCtxt->updateWithCurrentSong(currentSong);

            track_peak = currentSong.getPeak();
            if (track_peak)
                track_peak.value() = track_peak.value() / 32767.f;

            track_gain = currentSong.GetTitleGain();

            if (!settings.quiet)
            {
                Stringify s;

                // fprintf(stdout, _("%+6.2f dB | %6.0f | %5.2f | %8.0f | %s\n"), *track_gain, peak, scale, new_peak,
                // filename.c_str());

                if (track_gain)
                    s << std::showpos << std::setw(6) << std::fixed << std::setprecision(2) << *track_gain << " dB | ";
                else
                    s << "   N/A    | ";

                // we use currentSong.getPeak() to show the non normalized value
                if (track_peak)
                    s << std::noshowpos << std::setw(6) << std::fixed << std::setprecision(0) << currentSong.getPeak().value()
                      << " | ";
                else
                    s << "  N/A  | ";

                if (track_gain)
                {
                    const auto scale = static_cast<float>(pow(10., *track_gain / 20.));
                    const auto new_peak = currentSong.getPeak().value() * scale;

                    s << std::setw(5) << std::fixed << std::setprecision(2) << scale << " | " << std::setw(8) << std::fixed
                      << std::setprecision(0) << new_peak;
                }
                else
                {
                    s << " N/A  |   N/A   ";
                }

                s << " | " << filename.str();

                print_line(s);
            }
        }
        catch (const std::runtime_error& ex)
        {
            if (!settings.skip)
            {
                print_error(std::string_view(ex.what()));
            }
            throw;
        }
    }

    /**
     * \brief Write the replay gain tags to a file.
     *
     * Write the replay gain tags to a file, removing any old style tags. Any
     * other tags remain unchanged. track_gain, album_gain, track_peak and album_peak
     * are optional. If not set then no corresponding tag is written but any already
     * present tag of the corresponding type will remain.
     *
     * \param filename    name of the file to write the tags to.
     * \param track_peak  optional track peak value to write.
     * \param track_gain  optional track replay gain value to write.
     * \param album_peak  optional album peak value to write.
     * \param album_gain  optional album gain value to write.
     * \param verbose     print processing messages.
     * \param remove_tags if true, remove all replay gain tags from the file
     *                    (both old and new style). Any track or album values are
     *                    ignored.
     */
    void write_gains(const path_to_a_file& filename, const std::optional<float>& track_peak,
                     const std::optional<float>& track_gain, const std::optional<float>& album_peak,
                     const std::optional<float>& album_gain, const SETTINGS& settings, bool remove_tags)
    {
        ifile infile(filename);

        vcedit_state state(infile);

        auto& comments = state.getComments();
        using commenttype = rem_cvref_t<decltype(comments)>::value_type;

        auto remove_existing_tag = [&](const commenttype& fullcomment)
        {
            /* Remove none, except those we wish to change.
             * Alternatively, remove all ReplayGain tags.
             */

            /* Remove any old style tags */
            if ((fullcomment.first == TAG_TRACK_GAIN_OLD) || (fullcomment.first == TAG_TRACK_PEAK_OLD) ||
                (fullcomment.first == TAG_ALBUM_GAIN_OLD))
            {
                return true;
            }
            /* Check for tags */
            else if ((fullcomment.first == TAG_TRACK_GAIN) && (remove_tags || track_gain.has_value()))
            {
                return true;
            }
            else if ((fullcomment.first == TAG_TRACK_PEAK) && (remove_tags || track_peak.has_value()))
            {
                return true;
            }
            else if ((fullcomment.first == TAG_ALBUM_GAIN) && (remove_tags || album_gain.has_value()))
            {
                return true;
            }
            else if ((fullcomment.first == TAG_ALBUM_PEAK) && (remove_tags || album_peak.has_value()))
            {
                return true;
            }

            return false;
        };

        comments.erase(std::remove_if(comments.begin(), comments.end(), remove_existing_tag), comments.end());

        /* Add new tags - unless ReplayGain tags are to be removed */
        if (!remove_tags)
        {
            if (track_peak.has_value())
            {
                const auto strval = format_peak_to_str(*track_peak);
                comments.emplace_back(std::make_pair(TAG_TRACK_PEAK, std::move(strval)));
            }

            if (track_gain.has_value())
            {
                const auto strval = format_gain_to_str(*track_gain);
                comments.emplace_back(std::make_pair(TAG_TRACK_GAIN, std::move(strval)));
            }

            if (album_peak.has_value())
            {
                const auto strval = format_peak_to_str(*album_peak);
                comments.emplace_back(std::make_pair(TAG_ALBUM_PEAK, std::move(strval)));
            }

            if (album_gain.has_value())
            {
                const auto strval = format_gain_to_str(*album_gain);
                comments.emplace_back(std::make_pair(TAG_ALBUM_GAIN, std::move(strval)));
            }
        }

        /* Make sure temp is in same folder as file */
        const path_to_a_file random_filename = make_random_filename(filename.dir(), TEMP_NAME_TEMPLATE);
        // todo add on_failure(remove(random_filename)), !scope

        if (!settings.quiet)
        {
            using namespace std::literals;

            const auto msg = remove_tags ? "Removing tags from "sv : "Writing tags to "sv;
            print_line(Stringify() << msg << filename.str());
            print_flush();
        }

        vcedit_write(&state, random_filename);
        // file is now written
        // update meta info

        // get original meta info
        const path_to_stat_nofail stat_filename(filename);
        const auto ts = stat_filename.get_timestamps();

        // set meta info
        C::chmod::call(random_filename.str().c_str(), stat_filename.get_mode());

        if (settings.keep_timestamps)
            set_timestamps(random_filename, ts);

        // rename new to original
        const path_to_a_file random_filename_oldfile = make_random_filename(filename.dir(), TEMP_NAME_TEMPLATE);

        C::rename::call(filename.str().c_str(), random_filename_oldfile.str().c_str());
        C::rename::call(random_filename.str().c_str(), filename.str().c_str());

        C::remove::call(random_filename_oldfile.str().c_str());
    }
} // namespace VG
