#ifndef VG_IOFILE_INTERNAL_H
#define VG_IOFILE_INTERNAL_H

#include "c_calls.h"
#include "path_to.h"
#include "unique_resource.h"

namespace VG::zzzz_details
{
    std::size_t myfread(std::FILE* f, std::byte* buf, std::size_t count);
    void myfwrite(std::FILE* f, const std::byte* buf, std::size_t count);
    std::FILE* myfopen(const char* filename, const char* flags);

    class iofile_base
    {
      public:
        enum class iofile_openmode
        {
            RO,
            WO,
        };

        iofile_base(iofile_openmode mode, const path_to_a_file& filename);

        void close();

      protected:
        using stdfclose_deleter = deleter<VG::C::stdfclose, std::FILE*, nullptr, true>;
        using unique_stdFILE = unique_resource<stdfclose_deleter>;

        unique_stdFILE m_file;

      private:
        static const char* iofile_mode_to_flags(iofile_openmode mode);

        static unique_stdFILE do_open(iofile_openmode mode, const path_to_a_file& filename);
    };
} // namespace VG::zzzz_details

#endif
