#include "stringify.h"

namespace VG
{
    Stringify::operator std::string() const
    {
        return m_oss.str();
    }

    Stringify& Stringify::operator<<(const Stringify& s)
    {
        m_oss << s.operator std::string();
        return *this;
    }

    Stringify& Stringify::operator<<(const mystring_view& str)
    {
        m_oss << str.get();
        return *this;
    }

    void Stringify::write(const std::byte* s, std::size_t count)
    {
        m_oss.write(reinterpret_cast<const char*>(s), static_cast<std::streamsize>(count));
    }
} // namespace VG
