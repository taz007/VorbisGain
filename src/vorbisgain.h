#ifndef VG_VORBISGAIN_H
#define VG_VORBISGAIN_H

#include <optional>
#include <regex>
#include <utility>
#include <vector>

#include "path_to.h"

namespace VG
{
    /** Information about a file to process */
    struct file_list_entry
    {
        template<typename... ARGS>
        explicit file_list_entry(std::in_place_t, ARGS&&... args) : file(std::forward<ARGS>(args)...), skip(false)
        {
        }

        const path_to_a_file file;
        std::optional<float> track_gain;
        std::optional<float> track_peak;
        std::optional<float> album_gain; /* File's ablum gain value; only used when converting tags */
        bool skip;                       /* Don't process this file (already tagged or not a Vorbis file) */
    };

    using FILE_LIST = std::vector<file_list_entry>;

    /** Settings and misc other global data */
    struct SETTINGS
    {
        std::optional<float> album_gain; /* Album gain specified on the command line */
        bool album = false;              /* Calculate album gain values as well */
        bool clean = false;              /* Remove all replay gain tags */
        bool convert = false;            /* Convert old format tags to new format */
        bool display_only = false;
        bool fast = false; /* Skip files that already have all needed tags */
        bool quiet = false;
        bool recursive = false;
        std::optional<std::regex> regex; /* optional regex to filter files */
        bool skip = false;               /* Skip non-vorbis files */
        bool show_progress = true;
        bool keep_timestamps = false; /* keep original file timestamp */
    };

    class ProcessingContext
    {
      public:
        FILE_LIST file_list;    /* Files to process (possibly as an album) */
        bool first_file = true; /* About to process the first file in an album */
    };

    void process_files(FILE_LIST& file_list, const SETTINGS& settings, ProcessingContext& ctxt);
    int parse_and_run(int argc, char** argv);
} // namespace VG
#endif /* VG_VORBISGAIN_H */
