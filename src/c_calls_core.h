#ifndef VG_C_CALLS_CORE_H
#define VG_C_CALLS_CORE_H

#include <cstdio>
#include <cstdlib>
#include <string>
#include <utility>

#include <unistd.h>

#include "vorbisgainexception.h"

namespace VG::C
{
    template<typename FCT>
    struct mysyscallthrow
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args)
        {
            const auto ret = FCT::call(args...);
            if (!FCT::verifretok::verif_all(ret, args...))
            {
                const auto errno_copy = errno;
                FCT::handle_error::handle(ret, errno_copy, args...);
            }
            return ret;
        }
    };

    template<typename FCT, typename EXCEPTION>
    struct get_error_msg_and_throw
    {
        template<typename RET, typename ERRNBR, typename... ARGS>
        [[noreturn]] static void handle(RET&& ret, ERRNBR&& errnbr, ARGS&&... args)
        {
            auto msg = FCT::getErrorMsg(std::forward<ARGS>(args)...);
            EXCEPTION::throwEx(ret, errnbr, std::move(msg));
        }
    };

    // no error msg here, as we are probably from a fork(), can only call async-signal-safe functions
    struct exit_errno
    {
        template<typename... ARGS>
        [[noreturn]] static void handle(int /*ret*/, int errnbr, ARGS&&...) noexcept
        {
            const int retval = (errnbr > 0) ? errnbr : EXIT_FAILURE;
            ::_exit(retval);
        }
    };

    template<typename CHILD>
    struct VerifAll_return_only
    {
        template<typename... ARGS>
        static auto verif_all(ARGS&&... args) noexcept
        {
            return verif_split_ret(std::forward<ARGS>(args)...);
        }

      private:
        template<typename ARG, typename... ARGS>
        static auto verif_split_ret(ARG&& arg, ARGS&&...)
        {
            return CHILD::verif(arg);
        }
    };

    template<bool V>
    struct VerifAlways : VerifAll_return_only<VerifAlways<V>>
    {
        template<typename TYPE>
        constexpr static bool verif(const TYPE&) noexcept
        {
            return V;
        }
    };

    template<typename TYPE, TYPE val>
    struct VerifRetEqual : VerifAll_return_only<VerifRetEqual<TYPE, val>>
    {
        constexpr static bool verif(const TYPE& arg) noexcept
        {
            return arg == val;
        }
    };

    template<typename TYPE, TYPE val>
    struct VerifRetNotEqual : VerifAll_return_only<VerifRetNotEqual<TYPE, val>>
    {
        constexpr static bool verif(const TYPE& arg) noexcept
        {
            return arg != val;
        }
    };

    template<typename TYPE, TYPE val>
    struct VerifRetGreaterOrEqual : VerifAll_return_only<VerifRetGreaterOrEqual<TYPE, val>>
    {
        constexpr static bool verif(const TYPE& arg) noexcept
        {
            return arg >= val;
        }
    };

    template<int E>
    struct VerifRetEqualZeroOrMinusOneAnd : VerifAll_return_only<VerifRetEqualZeroOrMinusOneAnd<E>>
    {
        template<typename T>
        static bool verif(const T& arg) noexcept
        {
            return (arg == 0) || ((arg == -1) && (errno == E));
        }
    };

    template<int E>
    struct VerifRetEqualZeroOrEOFAnd : VerifAll_return_only<VerifRetEqualZeroOrEOFAnd<E>>
    {
        template<typename T>
        static bool verif(const T& arg) noexcept
        {
            return (arg == 0) || ((arg == EOF) && (errno == E));
        }
    };

    struct VerifRetGreaterZeroOrMinusOneAndIntr : VerifAll_return_only<VerifRetGreaterZeroOrMinusOneAndIntr>
    {
        template<typename T>
        static bool verif(const T& arg) noexcept
        {
            return (arg > 0) || ((arg == -1) && (errno == EINTR));
        }
    };

    struct VerifRetGreaterEqualZeroOrMinusOneAndIntr : VerifAll_return_only<VerifRetGreaterEqualZeroOrMinusOneAndIntr>
    {
        template<typename T>
        static bool verif(const T& arg) noexcept
        {
            return (arg >= 0) || ((arg == -1) && (errno == EINTR));
        }
    };

    template<int E>
    struct VerifRetNotNullOr : VerifAll_return_only<VerifRetNotNullOr<E>>
    {
        template<typename T>
        static bool verif(const T& arg) noexcept
        {
            return (arg != nullptr) || (errno == E);
        }
    };

    struct VerifRetNotNullOrErrnoNotSet : VerifAll_return_only<VerifRetNotNullOrErrnoNotSet>
    {
        template<typename T>
        static bool verif(const T& arg) noexcept
        {
            return (arg != nullptr) || (errno == 0);
        }
    };

    struct SystemErrorExceptionFctorHelper
    {
        template<typename RETTYPE>
        [[noreturn]] static void throwEx(RETTYPE&& /*ret*/, int errnbr, const std::string& what_arg)
        {
            throw Exception::SystemError(errnbr, what_arg);
        }
    };
} // namespace VG::C
#endif
