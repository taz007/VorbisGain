#ifndef VG_PATH_TO_H
#define VG_PATH_TO_H

#include <string>
#include <utility>
#include <vector>

#include "mystring_view.h"

namespace VG
{
    class path_node
    {
      public:
        path_node(const path_node&) = default;
        path_node(path_node&&) = default;
        path_node& operator=(const path_node&) = default;
        path_node& operator=(path_node&&) = default;

        template<typename STR>
        static path_node make(STR&& str)
        {
            return path_node(std::in_place, std::forward<STR>(str));
        }

        [[nodiscard]] const std::string& name() const noexcept;
        [[nodiscard]] std::string& name() noexcept;

      protected:
        std::string m_entry;

      private:
        template<typename STR>
        explicit path_node(std::in_place_t, STR&& str) : m_entry(std::forward<STR>(str))
        {
        }
    };

    // this node name is always '/' terminated
    class path_node_dir : public path_node
    {
      public:
        static constexpr char dir_separator = '/';

        path_node_dir(const path_node_dir&) = default;
        path_node_dir(path_node_dir&&) = default;
        path_node_dir& operator=(const path_node_dir&) = default;
        path_node_dir& operator=(path_node_dir&&) = default;

        template<typename STR>
        static path_node_dir make(STR&& str)
        {
            return path_node_dir(std::in_place, std::forward<STR>(str));
        }

      private:
        template<typename STR>
        explicit path_node_dir(std::in_place_t, STR&& str)
            //@TODO C++20
            //[[expects: str.len() > 0]]
            : path_node(path_node::make(std::forward<STR>(str)))
        {
            if (m_entry.empty() || *m_entry.rbegin() != dir_separator)
            {
                m_entry += dir_separator;
            }
        }
    };

    class path_to_a_dir
    {
      public:
        path_to_a_dir() = default;
        path_to_a_dir(const path_to_a_dir&) = default;
        path_to_a_dir(path_to_a_dir&&) = default;
        path_to_a_dir& operator=(const path_to_a_dir&) = default;
        path_to_a_dir& operator=(path_to_a_dir&&) = default;

        template<typename... NODES>
        path_to_a_dir(std::in_place_t, NODES&&... n)
        {
            parse_nodes(std::forward<NODES>(n)...);
        }

        [[nodiscard]] std::string str() const;

        template<typename T>
        path_to_a_dir& add_subdir(T&& t)
        {
            m_dirs.emplace_back(path_node_dir::make(std::forward<T>(t)));
            return *this;
        }

      private:
        constexpr void parse_nodes() noexcept {};

        template<typename NODE, typename... NODES>
        void parse_nodes(NODE&& n, NODES&&... nodes)
        {
            add_subdir(std::forward<NODE>(n));
            parse_nodes(std::forward<NODES>(nodes)...);
        }

        std::vector<path_node_dir> m_dirs;
    };

    namespace zzzz_details
    {
        class path_to_a_node
        {
          public:
            template<typename D, typename F>
            explicit path_to_a_node(D&& d, F&& f)
                : m_pathdir(std::forward<D>(d)), m_node_file(path_node::make(std::forward<F>(f)))
            {
            }

            [[nodiscard]] std::string str() const;
            [[nodiscard]] const path_to_a_dir& dir() const noexcept;

          protected:
            [[nodiscard]] const path_node& node() const noexcept;
            [[nodiscard]] path_node& node() noexcept;

          private:
            path_to_a_dir m_pathdir;
            path_node m_node_file;
        };
    } // namespace zzzz_details

    class path_to_something : public zzzz_details::path_to_a_node
    {
      public:
        using zzzz_details::path_to_a_node::path_to_a_node;

        [[nodiscard]] const path_node& something() const noexcept;
    };

    // use path_to_a_file by default, unless you really don't know if it points to a file
    // then use path_to_something
    // this is for semantic purposes only
    class path_to_a_file : public zzzz_details::path_to_a_node
    {
      public:
        using zzzz_details::path_to_a_node::path_to_a_node;

        [[nodiscard]] const path_node& file_node() const noexcept;
        [[nodiscard]] path_node& file_node() noexcept;
    };

    path_to_a_file make_path_to_a_file(const mystring_view& raw_path_to_file);

} // namespace VG

#endif
