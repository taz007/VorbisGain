#ifndef VG_VCEDIT_H
#define VG_VCEDIT_H

#include <cstdint>
#include <string>
#include <vector>

#include <ogg/ogg.h>
#include <vorbis/codec.h>

#include "iofile.h"
#include "path_to.h"

namespace VG
{
    class vorbis_comment
    {
      public:
        vorbis_comment();
        ~vorbis_comment();

        [[nodiscard]] std::string_view vendor() const;

        // TODO remove this
        ::vorbis_comment& raw() noexcept;

      private:
        void init() noexcept;
        void clear() noexcept;

        ::vorbis_comment m_vc;
    };

    class ogg_page
    {
      public:
        void write_to_file(ofile& out) const;

        bool eos();
        int serialno() noexcept;
        // TODO remove this
        ::ogg_page& raw() noexcept;

      private:
        void write_header(ofile& out) const;
        void write_body(ofile& out) const;
        ::ogg_page m_opage;
    };

    class ogg_packet
    {
      public:
        // TODO remove this
        ogg_packet();
        ogg_packet(const ogg_packet&) = delete;
        ogg_packet(ogg_packet&& op) noexcept;
        ogg_packet& operator=(const ogg_packet&) = delete;
        ogg_packet& operator=(ogg_packet&&) = delete;
        ogg_packet& operator=(const ::ogg_packet& op);

        ~ogg_packet();

        // TODO remove this
        ::ogg_packet& raw() noexcept;

      private:
        void copy(const ::ogg_packet& op);
        void copy_raw(const unsigned char* src, unsigned int len);
        void clear();
        void init();
        bool m_managed;

        ::ogg_packet m_op;
    };

    class oggpack
    {
      public:
        oggpack();
        ~oggpack();

        void write1(bool val) noexcept;
        void write8(uint8_t val) noexcept;
        void write32(uint32_t val) noexcept;
        void writestr(const mystring_view& str) noexcept;

        [[nodiscard]] ogg_packet get_packet();

      private:
        ::oggpack_buffer m_opb;
    };

    class ogg_stream
    {
      public:
        // TODO remove these 2 after complete refactoring
        ogg_stream() = default;
        void init(int serial);

        explicit ogg_stream(int serial);
        ~ogg_stream();

        void add_page(ogg_page& page);
        void add_packet(ogg_packet& packet);
        [[nodiscard]] std::optional<ogg_packet> get_next_packet();
        [[nodiscard]] std::optional<ogg_page> flush_to_page();
        void flush_all_to_file(ofile& f);
        [[nodiscard]] std::optional<ogg_page> pageout();

        void set_eos(bool is_eos) noexcept;

      private:
        ::ogg_stream_state m_os;
    };

    class ogg_sync
    {
      public:
        explicit ogg_sync(VG::ifile& in);
        ~ogg_sync();

        [[nodiscard]] std::optional<ogg_page> get_next_page(unsigned int max_reads = 0);

      private:
        [[nodiscard]] unsigned long read();
        void wrote(std::size_t bytes);

        VG::ifile& m_infile;
        ::ogg_sync_state m_oy;
    };

    class vorbis_info
    {
      public:
        vorbis_info();
        ~vorbis_info();

        long packet_blocksize(VG::ogg_packet& op);

        // TODO remove this
        ::vorbis_info& raw() noexcept;

      private:
        ::vorbis_info m_vi;
    };

    class vcedit_state
    {
      public:
        explicit vcedit_state(ifile& f);

        using Comments = std::vector<std::pair<std::string, std::string>>;

        [[nodiscard]] const Comments& getComments() const noexcept;
        [[nodiscard]] Comments& getComments() noexcept;

      public:
        VG::ogg_sync oy;
        VG::ogg_stream os;

        VG::vorbis_info vi;

        int serial = 0;
        VG::ogg_packet main;
        VG::ogg_packet book;
        std::string vendor;
        long prevW = 0;
        bool extrapage = false;
        bool eosin = false;

      private:
        void open();

        Comments m_comments;
    };

    void vcedit_write(vcedit_state* state, const path_to_a_file& out);
} // namespace VG

#endif /* __VCEDIT_H */
