#ifndef VG_RECURSE_H
#define VG_RECURSE_H

#include <regex>

#include <dirent.h>

#include "path_to.h"
#include "vorbisgain.h"

namespace VG
{
    void process_argument(const char* path, const SETTINGS& settings, ProcessingContext& ctxt);

    class DirsAndFilesList
    {
      public:
        [[nodiscard]] const std::vector<path_to_a_dir>& dirs() const noexcept;
        [[nodiscard]] FILE_LIST& files() noexcept;
        [[nodiscard]] bool printcwd() const noexcept;

      protected:
        explicit DirsAndFilesList(bool printcwd);
        std::vector<path_to_a_dir> m_dirs; // dirs only
        FILE_LIST m_files;                 // files only
        const bool m_printcwd;
    };

    // contains a cached copy of the CWD content, "." and ".." have been skipped
    // files must match the regex if one has been provided
    class DIRECTORY : public DirsAndFilesList
    {
      public:
        explicit DIRECTORY(const std::optional<std::regex>& fileFilter);

      private:
        void readContentDir(DIR* dir, const std::optional<std::regex>& fileFilter);
    };

    class ArgsToDirsAndFilesList : public DirsAndFilesList
    {
      public:
        explicit ArgsToDirsAndFilesList(const char* const* begin, const char* const* end);
    };

    void processDirsAndFiles(DirsAndFilesList& daf, const SETTINGS& settings, ProcessingContext& ctxt);
} // namespace VG
#endif /* VG_RECURSE_H */
