#include "print.h"

namespace VG::zzzz_details
{
    void print_actual(std::ostream& out, const mystring_view& str)
    {
        out << str.get();
    }

    void print_actual(std::ostream& out, const Stringify& str)
    {
        out << std::string(str);
    }

} // namespace VG::zzzz_details
