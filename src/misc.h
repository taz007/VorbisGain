#ifndef VG_MISC_H
#define VG_MISC_H

#include <string>
#include <utility>

#include "c_calls.h"
#include "path_to.h"
#include "unique_resource.h"

namespace VG
{
    [[nodiscard]] char my_toupper_ascii(char c);

    [[nodiscard]] std::string VorbisErrorToString(long vorbis_error);
    void UPPERCASE(std::string& str);

    [[nodiscard]] std::string getcwd();

    [[nodiscard]] bool get_console_size(unsigned int* columns, unsigned int* rows);

    namespace fs
    {
        int mymkstemp(char* filename_template);
    } // namespace fs

    using close_deleter = deleter<C::close, int, -1, true>;
    using unique_fd = unique_resource<close_deleter>;

    template<typename STR>
    [[nodiscard]] path_to_a_file make_random_filename(const path_to_a_dir& dir, STR&& filename_template_)
    {
        const auto template_path = path_to_a_file(dir, std::forward<STR>(filename_template_));
        std::string full_filename_template = template_path.str();
        const unique_fd fd(fs::mymkstemp(full_filename_template.data()));
        // todo add on_failure(removefile())
        const auto pos = full_filename_template.size() - template_path.file_node().name().size();
        std::string random_filename = full_filename_template.substr(pos);
        return path_to_a_file(dir, std::move(random_filename));
    }
} // namespace VG

#endif /* VG_MISC_H */
