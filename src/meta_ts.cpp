#include "meta_ts.h"

#include <array>

#include "c_calls.h"

namespace VG
{
    std::chrono::system_clock::time_point timespec_to_timepoint(const timespec& ts)
    {
        using namespace std::chrono;

        const auto duration = seconds{ts.tv_sec} + nanoseconds{ts.tv_nsec};

        return std::chrono::system_clock::time_point(duration);
    }

    timespec timepoint_to_timespec(const std::chrono::system_clock::time_point& t)
    {
        using namespace std::chrono;

        timespec ts;

        const auto tp_secs = time_point_cast<seconds>(t);
        const auto dur_ns = time_point_cast<nanoseconds>(t) - time_point_cast<nanoseconds>(tp_secs);

        // ts.tv_sec will truncate here if time_t is still 32bits
        ts.tv_sec = static_cast<decltype(ts.tv_sec)>(tp_secs.time_since_epoch().count());
        ts.tv_nsec = static_cast<decltype(ts.tv_nsec)>(dur_ns.count());

        return ts;
    }

    void set_timestamps(const path_to_a_file& file, const meta_filetimestamps& timestamps)
    {
        using namespace std::chrono;

        std::array<timespec, 2> times;

        times[0] = timepoint_to_timespec(timestamps.atime);
        times[1] = timepoint_to_timespec(timestamps.mtime);

        C::utimensat::call(AT_FDCWD, file.str().c_str(), times.data(), 0);
    }
} // namespace VG
