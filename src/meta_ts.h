#ifndef VG_META_TS_H
#define VG_META_TS_H

#include <chrono>

#include "path_to.h"

namespace VG
{
    struct meta_filetimestamps
    {
        std::chrono::system_clock::time_point atime;
        std::chrono::system_clock::time_point mtime;

        template<typename T, typename U>
        meta_filetimestamps(T&& a, U&& m) : atime(std::forward<T>(a)), mtime(std::forward<U>(m))
        {
        }
    };

    [[nodiscard]] std::chrono::system_clock::time_point timespec_to_timepoint(const timespec& ts);

    void set_timestamps(const path_to_a_file& file, const meta_filetimestamps& timestamps);

} // namespace VG

#endif
