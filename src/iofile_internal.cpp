#include "iofile_internal.h"

#include "unreachable.h"

namespace VG
{
    std::FILE* zzzz_details::myfopen(const char* filename, const char* flags)
    {
        while (true)
        {
            const auto ret = C::stdfopen::call(filename, flags);
            if (ret != nullptr)
                return ret;
            // can only return nullptr if eintr
        }
    }

    zzzz_details::iofile_base::iofile_base(iofile_openmode mode, const path_to_a_file& filename) : m_file(do_open(mode, filename))
    {
    }

    void zzzz_details::iofile_base::close()
    {
        m_file.reset();
    }

    const char* zzzz_details::iofile_base::iofile_mode_to_flags(zzzz_details::iofile_base::iofile_openmode mode)
    {
        switch (mode)
        {
        case iofile_openmode::RO:
            return "r";
        case iofile_openmode::WO:
            return "w";
        }
        unreachable();
    }

    zzzz_details::iofile_base::unique_stdFILE zzzz_details::iofile_base::do_open(iofile_openmode mode,
                                                                                 const path_to_a_file& filename)
    {
        const auto str = filename.str();
        const char* const flags = iofile_mode_to_flags(mode);

        return unique_stdFILE(myfopen(str.c_str(), flags));
    }

    void zzzz_details::myfwrite(std::FILE* f, const std::byte* buf, std::size_t count)
    {
        C::stdfwrite::call(buf, count, 1U, f);
    }

    std::size_t zzzz_details::myfread(std::FILE* f, std::byte* buf, std::size_t count)
    {
        return C::stdfread::call(buf, 1U, count, f);
    }

} // namespace VG
