#include "ignore_exception.h"

#include "print.h"
#include "stringify.h"

void VG::zzzz_details::ignore_exception_print(const std::exception& ex) noexcept
{
    print_error(Stringify() << "This error had to be ignored: " << std::string_view(ex.what()));
}
