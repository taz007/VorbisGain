#include "vorbisgainexception.h"

#include <vorbis/vorbisfile.h>

#include "stringify.h"

namespace VG
{
    const char* VorbisFileErrorCategory::name() const noexcept
    {
        return "vorbis";
    }

    std::string VorbisFileErrorCategory::message(int ev) const
    {
        switch (ev)
        {
        case OV_EREAD:
            return "Read error while fetching compressed data for decode";
            break;

        case OV_ENOTVORBIS:
            return "File does not contain Vorbis data";
            break;

        case OV_EVERSION:
            return "Vorbis version mismatch";
            break;

        case OV_EBADHEADER:
            return "Invalid Vorbis bitstream header";
            break;

        case OV_EFAULT:
            return "Internal Vorbis error";
            break;

        case OV_EINVAL:
            return "Invalid argument value";
            break;

        case OV_EBADLINK:
            return "Invalid stream section or requested link corrupt";
            break;

        case OV_ENOSEEK:
            return "File is not seekable";
            break;

        case OV_EIMPL:
            return "Function not yet implemented";
            break;

        case OV_HOLE:
            return "Interruption in the data";
            break;

        default:
            return Stringify() << "An unknown error occured (" << ev << ")";
            break;
        }
    }

    namespace Exception
    {
        const VorbisFileErrorCategory VorbisFile::errorCategory;

        VorbisFile::VorbisFile(int vorbiserror, const std::string& what_arg)
            : std::system_error(vorbiserror, errorCategory, what_arg)
        {
        }

        SystemError::SystemError(int errnbr, const std::string& what_arg)
            : std::system_error(errnbr, std::generic_category(), what_arg)
        {
        }

        Generic::Generic(const std::string& what) : std::runtime_error(what)
        {
        }
    } // namespace Exception
} // namespace VG
