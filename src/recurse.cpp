#include "recurse.h"

#include <cstring>
#include <string_view>

#include <dirent.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>

#include "c_calls.h"
#include "ignore_exception.h"
#include "misc.h"
#include "path_to_stat.h"
#include "print.h"
#include "stringify.h"
#include "vorbisgainexception.h"

namespace VG
{
    struct AutoDir
    {
        explicit AutoDir(DIR* d) : dir(d)
        {
        }

        ~AutoDir()
        {
            ignore_exception([&] { C::closedir::call(dir); });
        }

        DIR* const dir;
    };

    const std::vector<path_to_a_dir>& DirsAndFilesList::dirs() const noexcept
    {
        return m_dirs;
    }

    FILE_LIST& DirsAndFilesList::files() noexcept
    {
        return m_files;
    }

    bool DirsAndFilesList::printcwd() const noexcept
    {
        return m_printcwd;
    }

    DirsAndFilesList::DirsAndFilesList(bool printcwd) : m_printcwd(printcwd)
    {
    }

    DIRECTORY::DIRECTORY(const std::optional<std::regex>& fileFilter) : DirsAndFilesList(true)
    {
        AutoDir dir(VG::C::opendir::call("."));

        readContentDir(dir.dir, fileFilter);
    }

    void DIRECTORY::readContentDir(DIR* dir, const std::optional<std::regex>& fileFilter)
    {
        while (true)
        {
            auto const entry = VG::C::readdir::call(dir);
            if (entry == nullptr)
            {
                break;
            }
            else
            {
                if (std::strcmp(entry->d_name, ".") == 0 || std::strcmp(entry->d_name, "..") == 0)
                    continue;

                path_to_stat_nofail stat_something(path_to_something(path_to_a_dir(), entry->d_name));

                if (stat_something.is_a_dir())
                {
                    m_dirs.emplace_back(std::in_place, entry->d_name);
                }
                else if (fileFilter.has_value() && !std::regex_match(entry->d_name, fileFilter.value()))
                {
                    continue;
                }
                else
                {
                    m_files.emplace_back(std::in_place, path_to_a_dir(), entry->d_name);
                }
            }
        }
    }

    ArgsToDirsAndFilesList::ArgsToDirsAndFilesList(const char* const* begin, const char* const* end) : DirsAndFilesList(false)
    {
        while (begin != end)
        {
            const char* const entry = *begin;

            path_to_stat_nofail stat_something(path_to_something(path_to_a_dir(), entry));

            if (stat_something.is_a_dir())
                m_dirs.emplace_back(std::in_place, entry);
            else
                m_files.emplace_back(std::in_place, path_to_a_dir(), entry);

            ++begin;
        }
    }

    /**
     * \brief Process all files in a directory.
     *
     * Process all files in a directory. If settings->album is set, assume the
     * files make up one album. If settings->recursive is set, process all
     * subdirectories as well.
     *
     * \param current   "display name" (i.e., not neccessarily the full name) of
     *                  the current path, used to display the name of the
     *                  directory being processed. path will be appended to
     *                  current and passed on recursively, if needed.
     * \param path      name of folder to process.
     * \param settings  settings and global variables.
     * \return  0 if successful and -1 if an error occured (in which case a
     *          message has been printed).
     */
    static void process_directory(const path_to_a_dir& path, const SETTINGS& settings, ProcessingContext& ctxt)
    {
        const auto old_path = VG::getcwd();

        VG::C::chdir::call(path.str().c_str());

        DIRECTORY directory(settings.regex); // uses CWD

        processDirsAndFiles(directory, settings, ctxt);

        VG::C::chdir::call(old_path.c_str());
    }

    /**
     * \brief Process an argument.
     *
     * Process an argument. Check for wildcard at end of path, then process the
     * file or folder specified.
     *
     * \param path      path argument to process.
     * \param settings  settings and global variables.
     * \return  0 if successful and -1 if an error occured (in which case a
     *          message has been printed).
     */
    void processDirsAndFiles(DirsAndFilesList& daf, const SETTINGS& settings, ProcessingContext& ctxt)
    {
        if (settings.recursive)
        {
            for (const auto& d : daf.dirs())
            {
                try
                {
                    process_directory(d, settings, ctxt);
                }
                catch (const std::runtime_error& ex)
                {
                    if (settings.skip)
                        print_error(std::string_view(ex.what()));
                    else
                        throw;
                }
            }
        }
        else if (!daf.dirs().empty())
        {
            throw Exception::Generic("Directory given as parameter but not in recursive mode!");
        }

        if (!daf.files().empty())
        {
            if (!settings.quiet && daf.printcwd())
            {
                print_line();
                print_line(Stringify() << "Processing directory '" << VG::getcwd() << "'");
            }
            process_files(daf.files(), settings, ctxt);
        }
    }
} // namespace VG
