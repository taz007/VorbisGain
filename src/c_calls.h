#ifndef VG_C_CALLS_H
#define VG_C_CALLS_H

#include <cstddef>
#include <cstdio>
#include <cstdlib>
#include <string>
#include <utility>

#include <dirent.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <utime.h>
#include <vorbis/vorbisfile.h>

#include "c_calls_core.h"
#include "vorbisgainexception.h"

namespace VG::C
{
    struct VorbisFileExceptionFctorHelper
    {
        [[noreturn]] static void throwEx(int ret, int /*errnbr*/, const std::string& what_arg);
    };

    struct VorbisGainGenericErrorExceptionFctorHelper
    {
        template<typename RETTYPE>
        [[noreturn]] static void throwEx(RETTYPE&& /*ret*/, int /*errnbr*/, const std::string& what_arg)
        {
            throw Exception::Generic(what_arg);
        }
    };

    struct removeFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return std::remove(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* filename);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<removeFctor, SystemErrorExceptionFctorHelper>;
    };

    struct stdfopenFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return std::fopen(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname, const char* flags);

        using verifretok = VerifRetNotNullOr<EINTR>;
        using handle_error = get_error_msg_and_throw<stdfopenFctor, SystemErrorExceptionFctorHelper>;
    };

    struct stdfcloseFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return std::fclose(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(std::FILE* stream);

        using verifretok = VerifRetEqualZeroOrEOFAnd<EINTR>;
        using handle_error = get_error_msg_and_throw<stdfcloseFctor, SystemErrorExceptionFctorHelper>;
    };

    struct Verif_fwrite_noferror
    {
        static bool verif_all(std::size_t, const void*, std::size_t, std::size_t, std::FILE* stream) noexcept
        {
            return std::ferror(stream) == 0;
        }
    };

    struct stdfwriteFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return std::fwrite(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const void* buffer, std::size_t size, std::size_t count, std::FILE* stream);

        using verifretok = Verif_fwrite_noferror;
        using handle_error = get_error_msg_and_throw<stdfwriteFctor, SystemErrorExceptionFctorHelper>;
    };

    struct Verif_fread_noferror
    {
        static bool verif_all(std::size_t, void*, std::size_t, std::size_t, std::FILE* stream) noexcept
        {
            return std::ferror(stream) == 0;
        }
    };

    struct stdfreadFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return std::fread(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(void* buffer, std::size_t size, std::size_t count, std::FILE* stream);

        using verifretok = Verif_fread_noferror;
        using handle_error = get_error_msg_and_throw<stdfreadFctor, SystemErrorExceptionFctorHelper>;
    };

    struct statFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::stat(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname, struct ::stat* statbuf);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<statFctor, SystemErrorExceptionFctorHelper>;
    };

    struct fstatFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::fstat(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fd, struct ::stat* statbuf);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<fstatFctor, SystemErrorExceptionFctorHelper>;
    };

    struct stat_or_noentFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::stat(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname, struct ::stat* statbuf);

        using verifretok = VerifRetEqualZeroOrMinusOneAnd<ENOENT>;
        using handle_error = get_error_msg_and_throw<stat_or_noentFctor, SystemErrorExceptionFctorHelper>;
    };

    struct renameFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return std::rename(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* old_filename, const char* new_filename);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<renameFctor, SystemErrorExceptionFctorHelper>;
    };

    struct chmodFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::chmod(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname, mode_t mode);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<chmodFctor, SystemErrorExceptionFctorHelper>;
    };

    struct utimeFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::utime(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* path, const utimbuf* times);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<utimeFctor, SystemErrorExceptionFctorHelper>;
    };

    struct closedirFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::closedir(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(DIR* dirp);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<closedirFctor, SystemErrorExceptionFctorHelper>;
    };

    struct getcwdFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::getcwd(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(char* buf, size_t size);

        using verifretok = VerifRetNotNullOr<ERANGE>;
        using handle_error = get_error_msg_and_throw<getcwdFctor, SystemErrorExceptionFctorHelper>;
    };

    struct chdirFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::chdir(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* path);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<chdirFctor, SystemErrorExceptionFctorHelper>;
    };

    struct mkstempFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::mkstemp(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(char* templat);

        using verifretok = VerifRetGreaterEqualZeroOrMinusOneAndIntr;
        using handle_error = get_error_msg_and_throw<mkstempFctor, SystemErrorExceptionFctorHelper>;
    };

    struct opendirFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::opendir(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* name);

        using verifretok = VerifRetNotEqual<DIR*, nullptr>;
        using handle_error = get_error_msg_and_throw<opendirFctor, SystemErrorExceptionFctorHelper>;
    };

    struct readdirFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            errno = 0;
            return ::readdir(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(DIR* dirp);

        using verifretok = VerifRetNotNullOrErrnoNotSet;
        using handle_error = get_error_msg_and_throw<readdirFctor, SystemErrorExceptionFctorHelper>;
    };

    struct fdopenFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::fdopen(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fd, const char* mode);

        using verifretok = VerifRetNotEqual<FILE*, nullptr>;
        using handle_error = get_error_msg_and_throw<fdopenFctor, SystemErrorExceptionFctorHelper>;
    };

    struct openFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::open(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname, int flags, mode_t mode);

        using verifretok = VerifRetGreaterEqualZeroOrMinusOneAndIntr;
        using handle_error = get_error_msg_and_throw<openFctor, SystemErrorExceptionFctorHelper>;
    };

    struct readFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::read(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fd, void* buf, size_t count);

        using verifretok = VerifRetGreaterEqualZeroOrMinusOneAndIntr;
        using handle_error = get_error_msg_and_throw<readFctor, SystemErrorExceptionFctorHelper>;
    };

    struct writeFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::write(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fd, const void* buf, size_t count);

        using verifretok = VerifRetGreaterEqualZeroOrMinusOneAndIntr;
        using handle_error = get_error_msg_and_throw<writeFctor, SystemErrorExceptionFctorHelper>;
    };

    struct VerifRetGreaterZeroOrMinusOneAndEOF
    {
        static bool verif_all(ssize_t ret, char**, size_t*, FILE* stream) noexcept
        {
            return (ret > 0) || ((ret == -1) && (std::feof(stream) != 0));
        }
    };

    struct getlineFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::getline(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(char** lineptr, size_t* n, FILE* stream);

        using verifretok = VerifRetGreaterZeroOrMinusOneAndEOF;
        using handle_error = get_error_msg_and_throw<getlineFctor, SystemErrorExceptionFctorHelper>;
    };

    struct closeFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::close(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fd);

        using verifretok = VerifRetEqualZeroOrMinusOneAnd<EINTR>;
        using handle_error = get_error_msg_and_throw<closeFctor, SystemErrorExceptionFctorHelper>;
    };

    struct utimensatFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::utimensat(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fd, const char* path, const timespec*, int flags);

        using verifretok = VerifRetNotEqual<int, -1>;
        using handle_error = get_error_msg_and_throw<utimensatFctor, SystemErrorExceptionFctorHelper>;
    };

    struct ov_fopenFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            errno = 0;
            return ::ov_fopen(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* path, OggVorbis_File* vf);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<ov_fopenFctor, VorbisFileExceptionFctorHelper>;
    };

    struct ov_infoFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ov_info(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(OggVorbis_File* vf, int link);

        using verifretok = VerifRetNotEqual<vorbis_info*, nullptr>;
        using handle_error = get_error_msg_and_throw<ov_infoFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ov_pcm_totalFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ov_pcm_total(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(OggVorbis_File* vf, int i);

        using verifretok = VerifRetNotEqual<ogg_int64_t, OV_EINVAL>;
        using handle_error = get_error_msg_and_throw<ov_pcm_totalFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ov_pcm_tellFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ov_pcm_tell(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(OggVorbis_File* vf);

        using verifretok = VerifRetNotEqual<ogg_int64_t, OV_EINVAL>;
        using handle_error = get_error_msg_and_throw<ov_pcm_tellFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct vorbis_packet_blocksizeFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::vorbis_packet_blocksize(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(vorbis_info* vi, ogg_packet* op);

        using verifretok = VerifRetGreaterOrEqual<long, 0>;
        using handle_error = get_error_msg_and_throw<vorbis_packet_blocksizeFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct _ogg_mallocFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::_ogg_malloc(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(size_t size);

        using verifretok = VerifRetNotEqual<void*, nullptr>;
        using handle_error = get_error_msg_and_throw<_ogg_mallocFctor, SystemErrorExceptionFctorHelper>;
    };

    struct ogg_sync_bufferFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ogg_sync_buffer(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(ogg_sync_state* oy, long size);

        using verifretok = VerifRetNotEqual<char*, nullptr>;
        using handle_error = get_error_msg_and_throw<ogg_sync_bufferFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ogg_sync_wroteFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ogg_sync_wrote(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(ogg_sync_state* oy, long bytes);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<ogg_sync_wroteFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ogg_stream_initFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ogg_stream_init(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(ogg_stream_state* os, int serial);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<ogg_stream_initFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ogg_stream_packetinFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ogg_stream_packetin(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(ogg_stream_state* os, ogg_packet* op);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<ogg_stream_packetinFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ogg_stream_pageinFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ogg_stream_pagein(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(ogg_stream_state* os, ogg_page* og);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<ogg_stream_pageinFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    struct ogg_stream_packetoutFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::ogg_stream_packetout(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(ogg_stream_state* os, ogg_packet* op);

        using verifretok = VerifRetGreaterOrEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<ogg_stream_packetoutFctor, VorbisGainGenericErrorExceptionFctorHelper>;
    };

    using remove = mysyscallthrow<removeFctor>;
    using stdfopen = mysyscallthrow<stdfopenFctor>;
    using stdfclose = mysyscallthrow<stdfcloseFctor>;
    using stdfread = mysyscallthrow<stdfreadFctor>;
    using stdfwrite = mysyscallthrow<stdfwriteFctor>;
    using stat = mysyscallthrow<statFctor>;
    using stat_or_noent = mysyscallthrow<stat_or_noentFctor>;
    using fstat = mysyscallthrow<fstatFctor>;
    using rename = mysyscallthrow<renameFctor>;
    using chmod = mysyscallthrow<chmodFctor>;
    using utime = mysyscallthrow<utimeFctor>;
    using closedir = mysyscallthrow<closedirFctor>;
    using getcwd = mysyscallthrow<getcwdFctor>;
    using chdir = mysyscallthrow<chdirFctor>;
    using mkstemp = mysyscallthrow<mkstempFctor>;
    using opendir = mysyscallthrow<opendirFctor>;
    using readdir = mysyscallthrow<readdirFctor>;
    using fdopen = mysyscallthrow<fdopenFctor>;
    using open = mysyscallthrow<openFctor>;
    using close = mysyscallthrow<closeFctor>;
    using read = mysyscallthrow<readFctor>;
    using write = mysyscallthrow<writeFctor>;
    using getline = mysyscallthrow<getlineFctor>;
    using utimensat = mysyscallthrow<utimensatFctor>;

    using ov_fopen = mysyscallthrow<ov_fopenFctor>;
    using ov_info = mysyscallthrow<ov_infoFctor>;
    using ov_pcm_total = mysyscallthrow<ov_pcm_totalFctor>;
    using ov_pcm_tell = mysyscallthrow<ov_pcm_tellFctor>;

    using vorbis_packet_blocksize = mysyscallthrow<vorbis_packet_blocksizeFctor>;

    using _ogg_malloc = mysyscallthrow<_ogg_mallocFctor>;
    using ogg_sync_buffer = mysyscallthrow<ogg_sync_bufferFctor>;
    using ogg_sync_wrote = mysyscallthrow<ogg_sync_wroteFctor>;
    using ogg_stream_init = mysyscallthrow<ogg_stream_initFctor>;
    using ogg_stream_packetin = mysyscallthrow<ogg_stream_packetinFctor>;
    using ogg_stream_pagein = mysyscallthrow<ogg_stream_pageinFctor>;
    using ogg_stream_packetout = mysyscallthrow<ogg_stream_packetoutFctor>;
} // namespace VG::C

#endif
