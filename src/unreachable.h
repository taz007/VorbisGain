#ifndef VG_UNREACHABLE_H
#define VG_UNREACHABLE_H

namespace VG
{
    [[noreturn]] void unreachable() noexcept;
} // namespace VG

#endif /* VG_MISC_H */
