#ifndef VG_VORBIS_H
#define VG_VORBIS_H

#include "gain_analysis.h"
#include "path_to.h"
#include "vorbisgain.h"

namespace VG
{
    enum class has_tags_category
    {
        any,
        track,
        track_album
    };
    [[nodiscard]] bool has_tags(const path_to_a_file& filename, has_tags_category tagtype);
    [[nodiscard]] bool get_old_tags(const path_to_a_file& filename, bool album_tag, std::optional<float>& track_peak,
                                    std::optional<float>& track_gain, std::optional<float>& album_gain);
    void get_gain(const path_to_a_file& filename, std::optional<float>& track_peak, std::optional<float>& track_gain,
                  const SETTINGS& settings, ProcessingContext& ctxt, std::unique_ptr<ContextOneAlbum>& albumCtxt);
    void write_gains(const path_to_a_file& filename, const std::optional<float>& track_peak,
                     const std::optional<float>& track_gain, const std::optional<float>& album_peak,
                     const std::optional<float>& album_gain, const SETTINGS& settings, bool remove_tags);
    [[nodiscard]] std::optional<float> readfloat_if_possible(const std::string& str);
    [[nodiscard]] std::optional<float> readfloatDB_if_possible(const std::string& str);
} // namespace VG
#endif /* VG_VORBIS_H */
