#include "misc.h"

#include <algorithm>
#include <vector>

#include <sys/stat.h>
#include <sys/types.h>
#include <unistd.h>
#include <vorbis/codec.h>

#include "c_calls.h"
#include "config.h"
#include "get_console_size_ioctl.h"
#include "stringify.h"
#include "vorbisgainexception.h"

namespace VG
{
    void UPPERCASE(std::string& str)
    {
        std::transform(str.begin(), str.end(), str.begin(), [](auto c) -> auto { return my_toupper_ascii(c); });
    }

    std::string VorbisErrorToString(long vorbis_error)
    {
        using namespace std::string_literals;

        switch (vorbis_error)
        {
        case OV_EREAD:
            return "Read error while fetching compressed data for decode"s;

        case OV_ENOTVORBIS:
            return "File does not contain Vorbis data"s;

        case OV_EVERSION:
            return "Vorbis version mismatch"s;

        case OV_EBADHEADER:
            return "Invalid Vorbis bitstream header"s;

        case OV_EFAULT:
            return "Internal Vorbis error"s;

        case OV_EINVAL:
            return "Invalid argument value"s;

        case OV_EBADLINK:
            return "Invalid stream section or requested link corrupt"s;

        case OV_ENOSEEK:
            return "File is not seekable"s;

        case OV_EIMPL:
            return "Function not yet implemented"s;

        case OV_HOLE:
            return "Interruption in the data"s;

        default:
            return Stringify() << "An unknown error occured (" << vorbis_error << ")";
        }
    }

    /**
     * \breif Get the size - in columns and rows - of the output console.
     *
     * Get the size - in columns and rows - of the output console, if available.
     * \param columns  output argument for number of columns.
     * \param rows     output argument for number of rows.
     * \return  true for success and false for failure.
     */
    bool get_console_size(unsigned int* columns, unsigned int* rows)
    {
        if constexpr (ENABLE_WINSIZE)
        {
            return get_console_size_ioctl(columns, rows);
        }
        else
        {
            return false;
        }
    }

    std::string getcwd()
    {
        constexpr unsigned int default_cwd_size = 1024;
        using VC = std::vector<char>;

        VC cwd(default_cwd_size);

        while (true)
        {
            // can only return with a ptr or null (errno=ERANGE)
            const auto ret = VG::C::getcwd::call(&cwd[0], cwd.size());
            if (ret == nullptr)
            {
                if (cwd.size() == cwd.max_size())
                    throw VG::Exception::Generic("current working directory name is too big");

                VC::size_type newsize;
                if (cwd.size() <= cwd.max_size() / 2)
                    newsize = cwd.size() * 2;
                else
                    newsize = cwd.max_size();

                cwd.resize(newsize);
            }
            else
            {
                return std::string(&cwd[0]);
            }
        }
    }

    int fs::mymkstemp(char* filename_template)
    {
        while (true)
        {
            const int fd = C::mkstemp::call(filename_template);
            // only error here is EINTR
            if (fd != -1)
                return fd;
        }
    }

    char my_toupper_ascii(char c)
    {
        if (c >= 'a' && c <= 'z')
            return c - ('a' - 'A');
        return c;
    }

} // namespace VG
