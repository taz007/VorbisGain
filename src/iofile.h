#ifndef VG_IOFILE_H
#define VG_IOFILE_H

#include <cstddef>
#include <optional>
#include <string>

#include "iofile_internal.h"
#include "path_to.h"

namespace VG
{
    class ifile : public zzzz_details::iofile_base
    {
        using parent = zzzz_details::iofile_base;

      public:
        ifile(const path_to_a_file& str);

        std::optional<std::string> getline();
        std::size_t read(std::byte* buf, std::size_t count);
    };

    class ofile : public zzzz_details::iofile_base
    {
        using parent = zzzz_details::iofile_base;

      public:
        ofile(const path_to_a_file& str);

        void write(const std::byte* buf, std::size_t count);

        ofile& operator<<(char c);
        ofile& operator<<(const mystring_view& str);

        void copyfile(ifile& in);
    };
} // namespace VG

#endif
