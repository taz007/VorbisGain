#include "c_calls.h"

#include "stringify.h"

namespace VG::C
{
    void VorbisFileExceptionFctorHelper::VorbisFileExceptionFctorHelper::throwEx(int ret, int /*errnbr*/,
                                                                                 const std::string& what_arg)
    {
        throw Exception::VorbisFile(ret, what_arg);
    }

    std::string removeFctor::getErrorMsg(const char* filename)
    {
        return Stringify() << "Call to std::remove() failed with filename='" << std::string_view(filename) << "'";
    }

    std::string stdfcloseFctor::getErrorMsg(std::FILE*)
    {
        return "Call to std::fclose() failed";
    }

    std::string statFctor::getErrorMsg(const char* pathname, struct ::stat*)
    {
        return Stringify() << "Call to stat() failed with pathname='" << std::string_view(pathname) << "'";
    }

    std::string stat_or_noentFctor::getErrorMsg(const char* pathname, struct ::stat* st)
    {
        return statFctor::getErrorMsg(pathname, st);
    }

    std::string renameFctor::getErrorMsg(const char* old_filename, const char* new_filename)
    {
        return Stringify() << "Call to std::rename() failed with old_filename='" << std::string_view(old_filename)
                           << "' new_filename='" << std::string_view(new_filename) << "'";
    }

    std::string chmodFctor::getErrorMsg(const char* pathname, mode_t mode)
    {
        return Stringify() << "Call to chmod() failed with pathname='" << std::string_view(pathname) << "' mode='" << mode << "'";
    }

    std::string utimeFctor::getErrorMsg(const char* path, const utimbuf*)
    {
        return Stringify() << "Call to utime() failed with path='" << std::string_view(path) << "'";
    }

    std::string closedirFctor::getErrorMsg(DIR*)
    {
        return "Call to closedir() failed";
    }

    std::string getcwdFctor::getErrorMsg(char*, size_t)
    {
        return "Call to getcwd() failed";
    }

    std::string chdirFctor::getErrorMsg(const char* path)
    {
        return Stringify() << "Call to chdir() failed with path='" << std::string_view(path) << "'";
    }

    std::string mkstempFctor::getErrorMsg(char*)
    {
        return "Call to mkstemp() failed";
    }

    std::string opendirFctor::getErrorMsg(const char* name)
    {
        return Stringify() << "Call to opendir() failed with name='" << std::string_view(name) << "'";
    }

    std::string readdirFctor::getErrorMsg(DIR*)
    {
        return "Call to readdir() failed";
    }

    std::string fdopenFctor::getErrorMsg(int, const char* mode)
    {
        return Stringify() << "Call to fdopen() failed with mode='" << std::string_view(mode) << "'";
    }

    std::string openFctor::getErrorMsg(const char* pathname, int flags, mode_t mode)
    {
        return Stringify() << "Call to open() failed for '" << std::string_view(pathname) << "' with flags=" << flags
                           << " and mode=" << mode;
    }

    std::string closeFctor::getErrorMsg(int fd)
    {
        return Stringify() << "Call to close() failed with fd=" << fd;
    }

    std::string ov_fopenFctor::getErrorMsg(const char* path, OggVorbis_File*)
    {
        return Stringify() << "Call to ov_fopen() failed with path='" << std::string_view(path) << "'";
    }

    std::string ov_infoFctor::getErrorMsg(OggVorbis_File*, int)
    {
        return "Call to ov_info() failed";
    }

    std::string _ogg_mallocFctor::getErrorMsg(size_t size)
    {
        return Stringify() << "Call to _ogg_malloc() failed with size=" << size;
    }

    std::string ogg_sync_bufferFctor::getErrorMsg(ogg_sync_state*, long size)
    {
        return Stringify() << "Call to ogg_sync_buffer() failed with size=" << size;
    }

    std::string ogg_sync_wroteFctor::getErrorMsg(ogg_sync_state*, long bytes)
    {
        return Stringify() << "Call to ogg_sync_wrote() failed with bytes=" << bytes;
    }

    std::string ov_pcm_totalFctor::getErrorMsg(OggVorbis_File*, int)
    {
        return "Call to ov_pcm_total() failed";
    }

    std::string ov_pcm_tellFctor::getErrorMsg(OggVorbis_File*)
    {
        return "Call to ov_pcm_tell() failed";
    }

    std::string ogg_stream_initFctor::getErrorMsg(ogg_stream_state*, int)
    {
        return "Call to ogg_stream_init() failed";
    }

    std::string vorbis_packet_blocksizeFctor::getErrorMsg(vorbis_info*, ogg_packet*)
    {
        return "Call to vorbis_packet_blocksize() failed";
    }

    std::string ogg_stream_packetinFctor::getErrorMsg(ogg_stream_state*, ogg_packet*)
    {
        return "Call to ogg_stream_packetin() failed";
    }

    std::string ogg_stream_pageinFctor::getErrorMsg(ogg_stream_state*, ogg_page*)
    {
        return "Call to ogg_stream_pagein() failed";
    }

    std::string ogg_stream_packetoutFctor::getErrorMsg(ogg_stream_state*, ogg_packet*)
    {
        return "Call to ogg_stream_packetout() failed";
    }

    std::string readFctor::getErrorMsg(int fd, void*, size_t count)
    {
        return Stringify() << "Call to read() failed with count=" << count << " for fd=" << fd;
    }

    std::string writeFctor::getErrorMsg(int fd, const void*, size_t count)
    {
        return Stringify() << "Call to write() failed with count=" << count << " for fd=" << fd;
    }

    std::string stdfopenFctor::getErrorMsg(const char* pathname, const char* flags)
    {
        return Stringify() << "Call to std::fopen() failed for '" << std::string_view(pathname)
                           << "' with flags: " << std::string_view(flags);
    }

    std::string stdfwriteFctor::getErrorMsg(const void*, std::size_t size, std::size_t count, FILE*)
    {
        return Stringify() << "Call to std::fwrite() failed with size=" << size << " and count=" << count;
    }

    std::string stdfreadFctor::getErrorMsg(void*, std::size_t size, std::size_t count, FILE*)
    {
        return Stringify() << "Call to std::fread() failed with size=" << size << " and count=" << count;
    }

    std::string getlineFctor::getErrorMsg(char**, size_t*, FILE*)
    {
        return "Call to ::getline() failed";
    }

    std::string utimensatFctor::getErrorMsg(int fd, const char* path, const timespec*, int flags)
    {
        return Stringify() << "Call to utimensat() failed with for fd=" << fd << " and path='" << std::string_view(path)
                           << "' and flags=" << flags;
    }

    std::string fstatFctor::getErrorMsg(int fd, struct ::stat*)
    {
        return Stringify() << "Call to fstat() failed with fd=" << fd;
    }
} // namespace VG::C
