#ifndef VG_GLOBALS_H
#define VG_GLOBALS_H

#include <string_view>

namespace VG
{
    constexpr std::string_view TAG_TRACK_GAIN = "REPLAYGAIN_TRACK_GAIN";
    constexpr std::string_view TAG_TRACK_PEAK = "REPLAYGAIN_TRACK_PEAK";
    constexpr std::string_view TAG_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN";
    constexpr std::string_view TAG_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK";
    constexpr std::string_view TAG_TRACK_GAIN_OLD = "RG_RADIO";
    constexpr std::string_view TAG_TRACK_PEAK_OLD = "RG_PEAK";
    constexpr std::string_view TAG_ALBUM_GAIN_OLD = "RG_AUDIOPHILE";
    constexpr std::string_view TAGVALUE_GAIN_SUFFIX = " dB";
} // namespace VG

#endif
