#include "vorbisfilehelper.h"

#include "c_calls.h"

namespace VG
{
    AutoOggVorbisFile::AutoOggVorbisFile(const std::string& filename)
    {
        VG::C::ov_fopen::call(filename.c_str(), &m_ovfile);
    }

    AutoOggVorbisFile::~AutoOggVorbisFile()
    {
        ov_clear(&m_ovfile);
    }

    OggVorbis_File* AutoOggVorbisFile::get()
    {
        return &m_ovfile;
    }

    ogg_int64_t AutoOggVorbisFile::pcm_total(int bitstream)
    {
        return VG::C::ov_pcm_total::call(&m_ovfile, bitstream);
    }

    ogg_int64_t AutoOggVorbisFile::pcm_tell()
    {
        return VG::C::ov_pcm_tell::call(&m_ovfile);
    }

    ::vorbis_info* AutoOggVorbisFile::info(int bitstream)
    {
        return VG::C::ov_info::call(&m_ovfile, bitstream);
    }

    long AutoOggVorbisFile::streams()
    {
        return ov_streams(&m_ovfile);
    }

    vorbis_comment* AutoOggVorbisFile::comment(int bitstream)
    {
        return ov_comment(&m_ovfile, bitstream);
    }

} // namespace VG
