#ifndef VG_PATH_TO_STAT_H
#define VG_PATH_TO_STAT_H

#include "c_calls.h"
#include "meta_ts.h"

namespace VG
{

    class path_to_stat_base
    {
      public:
        template<typename T>
        path_to_stat_base(const T& /*path*/)
        {
            static_assert(std::is_base_of_v<zzzz_details::path_to_a_node, T> || std::is_base_of_v<path_to_a_dir, T>);
        }

        [[nodiscard]] bool is_a_dir() const noexcept;
        [[nodiscard]] bool is_a_file() const noexcept;

        using file_size_type = std::make_unsigned_t<decltype(::stat::st_size)>;
        [[nodiscard]] file_size_type file_size() const noexcept;

        [[nodiscard]] meta_filetimestamps get_timestamps() const noexcept;
        [[nodiscard]] decltype(::stat::st_mode) get_mode() const noexcept;

      protected:
        struct ::stat m_stat;
    };

    class path_to_stat : public path_to_stat_base
    {
      public:
        template<typename T>
        explicit path_to_stat(const T& path) : path_to_stat_base(path)
        {
            auto ret = C::stat_or_noent::call(path.str().c_str(), &m_stat);
            if (ret != 0)
            {
                m_isvalid = false;
            }
            else
            {
                m_isvalid = true;
            }
        }

        [[nodiscard]] bool is_valid() const noexcept;

      private:
        bool m_isvalid;
    };

    class path_to_stat_nofail : public path_to_stat_base
    {
      public:
        template<typename T>
        explicit path_to_stat_nofail(const T& path) : path_to_stat_base(path)
        {
            C::stat::call(path.str().c_str(), &m_stat);
        }
    };

} // namespace VG

#endif
