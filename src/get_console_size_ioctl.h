#ifndef VG_GET_CONSOLE_SIZE_IOCTL_H
#define VG_GET_CONSOLE_SIZE_IOCTL_H

namespace VG
{
    bool get_console_size_ioctl(unsigned int* columns, unsigned int* rows);
} // namespace VG

#endif /* VG_MISC_H */
