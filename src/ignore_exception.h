#ifndef VG_IGNORE_EXCEPTION_H
#define VG_IGNORE_EXCEPTION_H

#include <exception>

namespace VG
{
    namespace zzzz_details
    {
        void ignore_exception_print(const std::exception& ex) noexcept;
    }

    template<typename FCT>
    void ignore_exception(FCT f) noexcept
    {
        try
        {
            f();
        }
        catch (const std::exception& ex)
        {
            zzzz_details::ignore_exception_print(ex);
        }
    }
} // namespace VG

#endif
