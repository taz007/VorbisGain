#ifndef VG_NON_COPY_H
#define VG_NON_COPY_H

namespace VG
{
    class non_copy_moveable
    {
      protected:
        non_copy_moveable() = default;
        ~non_copy_moveable() = default;

      public:
        non_copy_moveable(const non_copy_moveable&) = delete;
        non_copy_moveable(non_copy_moveable&&) = delete;
        non_copy_moveable& operator=(const non_copy_moveable&) = delete;
        non_copy_moveable& operator=(non_copy_moveable&&) = delete;
    };
} // namespace VG

#endif
