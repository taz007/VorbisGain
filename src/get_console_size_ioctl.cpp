#include "get_console_size_ioctl.h"

#include <sys/ioctl.h>
#include <unistd.h>

namespace VG
{
    bool get_console_size_ioctl(unsigned int* columns, unsigned int* rows)
    {
        if (isatty(STDOUT_FILENO))
        {
            struct winsize size;

            if (!ioctl(STDOUT_FILENO, TIOCGWINSZ, &size))
            {
                *columns = size.ws_col;
                *rows = size.ws_row;
                return true;
            }
        }
        return false;
    }
} // namespace VG
