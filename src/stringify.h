#ifndef VG_STRINGIFY_H
#define VG_STRINGIFY_H

#include <sstream>
#include <string>

#include "mystring_view.h"
#include "non_copy.h"

namespace VG
{
    class Stringify : non_copy_moveable
    {
      public:
        [[nodiscard]] operator std::string() const;

        Stringify& operator<<(const Stringify& s);
        Stringify& operator<<(const mystring_view& str);

        template<class T, typename = std::enable_if_t<!is_same_cvr_v<T, const char*>>>
        Stringify& operator<<(const T& s)
        {
            m_oss << s;
            return *this;
        }

        void write(const std::byte* s, std::size_t count);

      private:
        std::ostringstream m_oss;
    };
} // namespace VG
#endif
