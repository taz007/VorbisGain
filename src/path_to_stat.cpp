#include "path_to_stat.h"

namespace VG
{
    bool path_to_stat::is_valid() const noexcept
    {
        return m_isvalid;
    }

    bool path_to_stat_base::is_a_dir() const noexcept
    {
        return S_ISDIR(m_stat.st_mode);
    }

    bool path_to_stat_base::is_a_file() const noexcept
    {
        return S_ISREG(m_stat.st_mode);
    }

    path_to_stat_base::file_size_type path_to_stat_base::file_size() const noexcept
    {
        return static_cast<file_size_type>(m_stat.st_size);
    }

    meta_filetimestamps path_to_stat_base::get_timestamps() const noexcept
    {

        meta_filetimestamps ts{timespec_to_timepoint(m_stat.st_atim), timespec_to_timepoint(m_stat.st_mtim)};
        return ts;
    }

    decltype(::stat::st_mode) path_to_stat_base::get_mode() const noexcept
    {
        return m_stat.st_mode;
    }

} // namespace VG
