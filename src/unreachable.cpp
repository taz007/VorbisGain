#include "unreachable.h"

#include <exception>

namespace VG
{
    [[noreturn]] void unreachable() noexcept
    {
        std::terminate();
        //@TODO add support for __builtin_unreachable()
    }
} // namespace VG
