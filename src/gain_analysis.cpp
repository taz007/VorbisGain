/*
 *  Here's the deal. Call
 *
 *    InitGainAnalysis ( long samplefreq );
 *
 *  to initialize everything. Call
 *
 *    AnalyzeSamples ( const Float_t*  left_samples,
 *                     const Float_t*  right_samples,
 *                     size_t          num_samples,
 *                     int             num_channels );
 *
 *  as many times as you want, with as many or as few samples as you want.
 *  If mono, pass the sample buffer in through left_samples, leave
 *  right_samples NULL, and make sure num_channels = 1.
 *
 *    GetTitleGain()
 *
 *  will return the recommended dB level change for all samples analyzed
 *  SINCE THE LAST TIME you called GetTitleGain() OR InitGainAnalysis().
 *
 *    GetAlbumGain()
 *
 *  will return the recommended dB level change for all samples analyzed
 *  since InitGainAnalysis() was called and finalized with GetTitleGain().
 *
 *  Pseudo-code to process an album:
 *
 *    Float_t       l_samples [4096];
 *    Float_t       r_samples [4096];
 *    size_t        num_samples;
 *    unsigned int  num_songs;
 *    unsigned int  i;
 *
 *    InitGainAnalysis ( 44100 );
 *    for ( i = 1; i <= num_songs; i++ ) {
 *        while ( ( num_samples = getSongSamples ( song[i], left_samples, right_samples ) ) > 0 )
 *            AnalyzeSamples ( left_samples, right_samples, num_samples, 2 );
 *        fprintf ("Recommended dB change for song %2d: %+6.2f dB\n", i, GetTitleGain() );
 *    }
 *    fprintf ("Recommended dB change for whole album: %+6.2f dB\n", GetAlbumGain() );
 */

/*
 *  So here's the main source of potential code confusion:
 *
 *  The filters applied to the incoming samples are IIR filters,
 *  meaning they rely on up to <filter order> number of previous samples
 *  AND up to <filter order> number of previous filtered samples.
 *
 *  I set up the AnalyzeSamples routine to minimize memory usage and interface
 *  complexity. The speed isn't compromised too much (I don't think), but the
 *  internal complexity is higher than it should be for such a relatively
 *  simple routine.
 *
 *  Optimization/clarity suggestions are welcome.
 */

#include "gain_analysis.h"

#include <cmath>
#include <cstring>
#include <numeric>

#include "stringify.h"
#include "vorbisgainexception.h"

namespace VG
{
    constexpr float PINK_REF = 64.82f; /* 298640883795 */ /* calibration value */
    constexpr double RMS_PERCENTILE = 0.95;               /* percentile which is louder than the proposed level */

    constexpr unsigned int freqToSampleWin(unsigned int freq) noexcept
    {
        // round up
        const unsigned int roundup = (freq % RMS_WINDOW_RATIO) ? 1 : 0;
        const auto ret = (freq / RMS_WINDOW_RATIO) + roundup;
        return ret;
    }

    constexpr FilterInfo filterInfo_48000 = {
        {{0.13919314567432f, -0.86984376593551f, 2.75465861874613f, -5.87257861775999f, 9.48293806319790f, -12.28759895145294f,
          13.05504219327545f, -11.34170355132042f, 7.81501653005538f, -3.84664617118067f},
         {0.00288463683916f, 0.00012025322027f, 0.00306428023191f, 0.00594298065125f, -0.02074045215285f, 0.02161526843274f,
          -0.01655260341619f, -0.00009291677959f, -0.00123395316851f, -0.02160367184185f, 0.03857599435200f}},
        {{0.97261396931306f, -1.97223372919527f}, {0.98621192462708f, -1.97242384925416f, 0.98621192462708f}},
        freqToSampleWin(48000)};
    constexpr FilterInfo filterInfo_44100 = {
        {{0.13149317958808f, -0.75104302451432f, 2.19611684890774f, -4.39470996079559f, 6.85401540936998f, -8.81498681370155f,
          9.47693607801280f, -8.54751527471874f, 6.36317777566148f, -3.47845948550071f},
         {-0.00187763777362f, 0.00674613682247f, -0.00240879051584f, 0.01624864962975f, -0.02596338512915f, 0.02245293253339f,
          -0.00834990904936f, -0.00851165645469f, -0.00848709379851f, -0.02911007808948f, 0.05418656406430f}},
        {{0.97022847566350f, -1.96977855582618f}, {0.98500175787242f, -1.97000351574484f, 0.98500175787242f}},
        freqToSampleWin(44100)};
    constexpr FilterInfo filterInfo_32000 = {
        {{0.02347897407020f, -0.05032077717131f, 0.16378164858596f, -0.45953458054983f, 1.00595954808547f, -1.67148153367602f,
          2.23697657451713f, -2.64577170229825f, 2.84868151156327f, -2.37898834973084f},
         {-0.00881362733839f, 0.00651420667831f, -0.01390589421898f, 0.03174092540049f, 0.00222312597743f, 0.04781476674921f,
          -0.05588393329856f, 0.02163541888798f, -0.06247880153653f, -0.09331049056315f, 0.15457299681924f}},
        {{0.95920349965459f, -1.95835380975398f}, {0.97938932735214f, -1.95877865470428f, 0.97938932735214f}},
        freqToSampleWin(32000)};
    constexpr FilterInfo filterInfo_24000 = {
        {{0.00302439095741f, 0.02005851806501f, 0.04500235387352f, -0.22138138954925f, 0.39120800788284f, -0.22638893773906f,
          -0.16276719120440f, -0.25656257754070f, 1.07977492259970f, -1.61273165137247f},
         {-0.02950134983287f, 0.00205861885564f, -0.00000828086748f, 0.06276101321749f, -0.00584456039913f, -0.02364141202522f,
          -0.00915702933434f, 0.03282930172664f, -0.08587323730772f, -0.22613988682123f, 0.30296907319327f}},
        {{0.95124613669835f, -1.95002759149878f}, {0.97531843204928f, -1.95063686409857f, 0.97531843204928f}},
        freqToSampleWin(24000)};
    constexpr FilterInfo filterInfo_22050 = {
        {{0.02977207319925f, -0.04237348025746f, 0.08333755284107f, -0.04067510197014f, -0.12453458140019f, 0.47854794562326f,
          -0.80774944671438f, 0.12205022308084f, 0.87350271418188f, -1.49858979367799f},
         {-0.01760176568150f, -0.01635381384540f, 0.00832043980773f, 0.05724228140351f, -0.00589500224440f, -0.00469977914380f,
          -0.07834489609479f, 0.11921148675203f, -0.11828570177555f, -0.25572241425570f, 0.33642304856132f}},
        {{0.94705070426118f, -1.94561023566527f}, {0.97316523498161f, -1.94633046996323f, 0.97316523498161f}},
        freqToSampleWin(22050)};
    constexpr FilterInfo filterInfo_16000 = {
        {{0.03222754072173f, 0.05784820375801f, 0.06747620744683f, 0.00613424350682f, 0.22199650564824f, -0.42029820170918f,
          0.00213767857124f, -0.37256372942400f, 0.29661783706366f, -0.62820619233671f},
         {0.00541907748707f, -0.03193428438915f, -0.01863887810927f, 0.10478503600251f, 0.04097565135648f, -0.12398163381748f,
          0.04078262797139f, -0.01419140100551f, -0.22784394429749f, -0.14351757464547f, 0.44915256608450f}},
        {{0.93034775234268f, -1.92783286977036f}, {0.96454515552826f, -1.92909031105652f, 0.96454515552826f}},
        freqToSampleWin(16000)};
    constexpr FilterInfo filterInfo_12000 = {
        {{0.01807364323573f, 0.01639907836189f, -0.04784254229033f, 0.06739368333110f, -0.33032403314006f, 0.45054734505008f,
          0.00819999645858f, -0.26806001042947f, 0.29156311971249f, -1.04800335126349f},
         {-0.00588215443421f, -0.03788984554840f, 0.08647503780351f, 0.00647310677246f, -0.27562961986224f, 0.30931782841830f,
          -0.18901604199609f, 0.16744243493672f, 0.16242137742230f, -0.75464456939302f, 0.56619470757641f}},
        {{0.92177618768381f, -1.91858953033784f}, {0.96009142950541f, -1.92018285901082f, 0.96009142950541f}},
        freqToSampleWin(12000)};
    constexpr FilterInfo filterInfo_11025 = {
        {{0.01818801111503f, 0.02442357316099f, -0.02505961724053f, -0.05246019024463f, -0.23313271880868f, 0.38952639978999f,
          0.14728154134330f, -0.20256413484477f, -0.31863563325245f, -0.51035327095184f},
         {-0.00749618797172f, -0.03721611395801f, 0.06920467763959f, 0.01628462406333f, -0.25344790059353f, 0.15558449135573f,
          0.02377945217615f, 0.17520704835522f, -0.14289799034253f, -0.53174909058578f, 0.58100494960553f}},
        {{0.91885558323625f, -1.91542108074780f}, {0.95856916599601f, -1.91713833199203f, 0.95856916599601f}},
        freqToSampleWin(11025)};
    constexpr FilterInfo filterInfo_8000 = {
        {{0.04704409688120f, 0.05477720428674f, -0.18823009262115f, -0.17556493366449f, 0.15113130533216f, 0.26408300200955f,
          -0.04678328784242f, -0.03424681017675f, -0.43193942311114f, -0.25049871956020f},
         {-0.02217936801134f, 0.04788665548180f, -0.04060034127000f, -0.11202315195388f, -0.02459864859345f, 0.14590772289388f,
          -0.10214864179676f, 0.04267842219415f, -0.00275953611929f, -0.42163034350696f, 0.53648789255105f}},
        {{0.89487434461664f, -1.88903307939452f}, {0.94597685600279f, -1.89195371200558f, 0.94597685600279f}},
        freqToSampleWin(8000)};

    constexpr auto empty_readingcallback = [](const float&) {};

    /* When calling this procedure, make sure that ip[-order] and op[-order] point to real data! */
    template<typename FILTER_YULEBUTTER, typename READINGCALLBACK = decltype(empty_readingcallback)>
    void filter(const float* input, float* output, size_t nSamples, const FILTER_YULEBUTTER& filterab,
                READINGCALLBACK rcb = empty_readingcallback)
    {
        constexpr auto size_a = std::tuple_size_v<typename FILTER_YULEBUTTER::type_a>;
        constexpr auto size_b = std::tuple_size_v<typename FILTER_YULEBUTTER::type_b>;

        static_assert(size_b == (size_a + 1));

        for (size_t i = 0; i < nSamples; ++i)
        {
            rcb(input[MAX_ORDER + i]);

            float y = std::inner_product(&input[MAX_ORDER - size_a + i], &input[MAX_ORDER + i + 1], &filterab.b[0], 0.f);
            y -= std::inner_product(&output[MAX_ORDER - size_a + i], &output[MAX_ORDER + i], &filterab.a[0], 0.f);

            output[MAX_ORDER + i] = y;
        }
    }

    const FilterInfo& freqToFilterInfo(unsigned int freq)
    {
        switch (freq)
        {
        case 48000:
            return filterInfo_48000;
            break;
        case 44100:
            return filterInfo_44100;
            break;
        case 32000:
            return filterInfo_32000;
            break;
        case 24000:
            return filterInfo_24000;
            break;
        case 22050:
            return filterInfo_22050;
            break;
        case 16000:
            return filterInfo_16000;
            break;
        case 12000:
            return filterInfo_12000;
            break;
        case 11025:
            return filterInfo_11025;
            break;
        case 8000:
            return filterInfo_8000;
            break;
        default:
            throw VG::Exception::Generic(VG::Stringify() << "Unsupported frequency: " << freq);
        }
    }

    ContextOneSong::ContextOneSong(bool isStereo, unsigned int frequency)
        : left(), right(isStereo ? std::make_optional<ContextOneChannel>() : std::nullopt),
          filterinfo(freqToFilterInfo(frequency)), totsamp(0), m_peak{}
    {
    }

    std::size_t ContextOneSong::remainingSampleWindow() const noexcept
    {
        return filterinfo.samples_window - totsamp;
    }

    double ContextOneSong::getMeanSumChannels() const noexcept
    {
        if (isStereo())
        {
            return (left.sum + right->sum) / 2;
        }
        else
        {
            return left.sum;
        }
    }

    void ContextOneSong::resetSum()
    {
        applyToChannels([](auto& channel) { channel.sum = 0; });
    }

    void ContextOneSong::updateOutBufs()
    {
        auto f = [totsamp = totsamp](auto& channel)
        { std::move(&channel.outbuf[totsamp], &channel.outbuf[totsamp + MAX_ORDER], &channel.outbuf[0]); };

        applyToChannels(f);
    }

    void ContextOneSong::updateStepBufs()
    {
        auto f = [totsamp = totsamp](auto& channel)
        { std::move(&channel.stepbuf[totsamp], &channel.stepbuf[totsamp + MAX_ORDER], &channel.stepbuf[0]); };

        applyToChannels(f);
    }

    void ContextOneSong::updateStateFinalStep(std::size_t cursamples)
    {
        totsamp += cursamples;

        if (totsamp == filterinfo.samples_window)
        { /* Get the Root Mean Square (RMS) for this set of samples */
            const double val = STEPS_per_dB * 10. * log10(getMeanSumChannels() / static_cast<double>(totsamp) + 1.e-37);
            const int ival = static_cast<int>(val);
            std::size_t uival;
            if (ival < 0)
            {
                uival = 0;
            }
            else
            {
                uival = static_cast<std::size_t>(ival);
                if (uival >= A.data.size())
                    uival = A.data.size() - 1;
            }

            A.data[uival]++;

            resetSum();
            updateOutBufs();
            updateStepBufs();
            totsamp = 0;
        }
    }

    void ContextOneSong::AnalyzeSamples(const float* const* pcm, std::size_t nbrSamples)
    {
        // there will be at least one sample, so we can init m_peak to the minimal value if needed
        if (m_peak.has_value() == false)
            m_peak = 0.f;

        std::size_t cursamplepos = 0;

        // update prebuffer
        updatePrebuffer(pcm, nbrSamples);

        while (cursamplepos < nbrSamples)
        {
            std::size_t cursamples = std::min(nbrSamples - cursamplepos, remainingSampleWindow());

            if (cursamplepos < MAX_ORDER)
            { // if we have to use the prebuffer, limit to remaining prebuffer size
                if (cursamples > MAX_ORDER - cursamplepos)
                    cursamples = MAX_ORDER - cursamplepos;
            }

            callFilterYule(pcm, cursamplepos, cursamples);
            callFilterButter(cursamples);
            updateSum(cursamples);
            updateStateFinalStep(cursamples);

            cursamplepos += cursamples;
        }

        if (nbrSamples < MAX_ORDER)
        {
            shiftPrebuffer(nbrSamples);
            preparePreBufferforNext(pcm, nbrSamples, nbrSamples);
        }
        else
        {
            preparePreBufferforNext(pcm, MAX_ORDER, nbrSamples);
        }
    }

    void ContextOneSong::preparePreBufferforNext(const float* const* pcm, std::size_t num_samplesToCopy, std::size_t sizePcm)
    {
        auto f = [&](const float* input, auto& channel)
        { std::copy(input + sizePcm - num_samplesToCopy, input + sizePcm, &channel.inprebuf[MAX_ORDER - num_samplesToCopy]); };

        applyToChannels(pcm, f);
    }

    void ContextOneSong::shiftPrebuffer(std::size_t num_samples)
    {
        auto f = [&](auto& channel)
        { std::move(&channel.inprebuf[num_samples], &channel.inprebuf[MAX_ORDER], &channel.inprebuf[0]); };

        applyToChannels(f);
    }

    void ContextOneSong::updateSum(std::size_t cursamples)
    {
        auto f = [&](auto& channel) { updateSumForOneChannel(channel, cursamples); };

        applyToChannels(f);
    }

    void ContextOneSong::updateSumForOneChannel(ContextOneChannel& chan, std::size_t cursamples)
    {
        for (unsigned int i = 0; i < cursamples; ++i)
        {
            chan.sum += chan.outbuf[MAX_ORDER + totsamp + i] * chan.outbuf[MAX_ORDER + totsamp + i];
        }
    }

    void ContextOneChannel::updatePrebuf(const float* pcm, std::size_t nbrSamplesToCopy)
    {
        std::copy(pcm, pcm + nbrSamplesToCopy, &inprebuf[MAX_ORDER]);
    }

    const float* ContextOneSong::computeInputPtrYule(ContextOneChannel& chan, const float* pcm, std::size_t cursamplepos)
    {
        if (cursamplepos < MAX_ORDER)
            return &chan.inprebuf[cursamplepos];
        else
            return &pcm[cursamplepos - MAX_ORDER];
    }

    void ContextOneSong::callFilterYuleForOneChannel(ContextOneChannel& chan, const float* pcm, std::size_t cursamplepos,
                                                     std::size_t cursamples)
    {
        const float* const in = computeInputPtrYule(chan, pcm, cursamplepos);
        float* const out = &chan.stepbuf[totsamp];
        auto cb = [&](const float& input)
        {
            const auto val = std::abs(input);
            if (val > m_peak.value())
            {
                m_peak = val;
            }
        };
        filter(in, out, cursamples, filterinfo.yule, cb);
    }

    void ContextOneSong::updatePrebuffer(const float* const* pcm, std::size_t nbrSamples)
    {
        const std::size_t num_samplesToCopy = std::min(nbrSamples, MAX_ORDER);

        auto f = [&](const float* input, ContextOneChannel& channel) { channel.updatePrebuf(input, num_samplesToCopy); };

        applyToChannels(pcm, f);
    }

    void ContextOneSong::callFilterYule(const float* const* pcm, std::size_t cursamplepos, std::size_t cursamples)
    {
        auto f = [&](const float* input, ContextOneChannel& channel)
        { callFilterYuleForOneChannel(channel, input, cursamplepos, cursamples); };

        applyToChannels(pcm, f);
    }

    void ContextOneSong::callFilterButter(std::size_t cursamples)
    {
        auto f = [this, cursamples](ContextOneChannel& channel) { callFilterButterForOneChannel(channel, cursamples); };

        applyToChannels(f);
    }

    void ContextOneSong::callFilterButterForOneChannel(ContextOneChannel& chan, std::size_t cursamples)
    {
        const float* const in = &chan.stepbuf[totsamp];
        float* const out = &chan.outbuf[totsamp];
        filter(in, out, cursamples, filterinfo.butter);
    }

    static std::optional<float> analyzeResult(const OneReplayGainData& rg)
    {
        unsigned int elems;
        size_t i;

        elems = 0;
        for (i = 0; i < rg.data.size(); ++i)
            elems += rg.data[i];
        if (elems == 0)
            return {};

        int upper = static_cast<int>(std::ceil(elems * (1. - RMS_PERCENTILE)));
        for (i = rg.data.size(); i-- > 0;)
        {
            if ((upper -= rg.data[i]) <= 0)
                break;
        }

        return PINK_REF - static_cast<float>(i) / STEPS_per_dB;
    }

    void ContextOneAlbum::updateWithCurrentSong(const ContextOneSong& currentSong)
    {
        for (std::size_t i = 0; i < currentSong.A.data.size(); ++i)
        {
            B.data[i] += currentSong.A.data[i];
        }
    }

    std::optional<float> ContextOneSong::GetTitleGain() const
    {
        return analyzeResult(A);
    }

    std::optional<float> ContextOneAlbum::GetAlbumGain() const
    {
        return analyzeResult(B);
    }
} // namespace VG
