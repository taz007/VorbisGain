#include "vcedit.h"

#include <cstring>
#include <limits>
#include <utility>

#include <ogg/ogg.h>
#include <vorbis/codec.h>

#include "c_calls.h"
#include "misc.h"
#include "stringify.h"
#include "vorbisgainexception.h"

namespace VG
{
    vcedit_state::vcedit_state(ifile& f) : oy(f)
    {
        open();
    }

    const vcedit_state::Comments& vcedit_state::getComments() const noexcept
    {
        return m_comments;
    }

    vcedit_state::Comments& vcedit_state::getComments() noexcept
    {
        return m_comments;
    }

    static oggpack _commentheader_out(const vcedit_state::Comments& comments, const std::string& vendor)
    {
        oggpack opb;

        /* preamble */
        opb.write8(0x03);
        opb.writestr("vorbis");

        /* vendor */
        if (vendor.length() > std::numeric_limits<uint32_t>::max())
            throw Exception::Generic(Stringify() << "vendor size is too big: " << vendor.length());

        opb.write32(static_cast<uint32_t>(vendor.length()));
        opb.writestr(vendor);

        /* comments */
        const auto comms = static_cast<unsigned int>(comments.size());
        opb.write32(comms);
        for (const auto& fullcomment : comments)
        {
            const auto l = static_cast<unsigned int>(fullcomment.first.length() + 1 + fullcomment.second.length());
            opb.write32(l);
            opb.writestr(fullcomment.first);
            opb.writestr("=");
            opb.writestr(fullcomment.second);
        }

        opb.write1(true);

        return opb;
    }

    static auto _blocksize(vcedit_state* s, ogg_packet& p)
    {
        const auto thisone = s->vi.packet_blocksize(p);
        std::remove_const_t<decltype(thisone)> ret;

        if (!s->prevW)
        {
            ret = 0;
        }
        else
        {
            ret = (thisone + s->prevW) / 4;
        }

        s->prevW = thisone;
        return ret;
    }

    static int _fetch_next_packet(vcedit_state* s, std::optional<ogg_packet>& packet, std::optional<ogg_page>& page)
    {
        std::optional<ogg_packet> p = s->os.get_next_packet();

        if (p.has_value())
        {
            packet.emplace(std::move(p.value()));
            return 1;
        }
        else
        {
            if (s->eosin)
                return 0;

            page = s->oy.get_next_page();
            if (!page.has_value())
            {
                return 0;
            }
            if (page->eos())
                s->eosin = true;
            else if (page->serialno() != s->serial)
            {
                s->eosin = true;
                s->extrapage = true;
                return 0;
            }

            s->os.add_page(page.value());
            return _fetch_next_packet(s, packet, page);
        }
    }

    void vcedit_state::open()
    {
        std::optional<ogg_packet> opt_header_comments;
        std::optional<ogg_packet> opt_header_codebooks;

        /* Bail if we don't find data in the first 40 kB */
        std::optional<ogg_page> opt_og = oy.get_next_page(10);

        if (!opt_og.has_value())
        {
            throw Exception::Generic("Input empty, truncated, or not an Ogg bitstream.");
        }

        serial = opt_og->serialno();

        os.init(serial);

        os.add_page(opt_og.value());

        std::optional<ogg_packet> header_main = os.get_next_packet();

        if (!header_main.has_value())
        {
            throw Exception::Generic("Error reading initial header packet.");
        }

        VG::vorbis_comment vc;

        if (vorbis_synthesis_headerin(&vi.raw(), &vc.raw(), &header_main.value().raw()) < 0)
        {
            throw Exception::Generic("Ogg bitstream does not contain vorbis data.");
        }

        main = header_main.value().raw();
        main.raw().b_o_s = true;

        int i = 0;
        while (i < 2)
        {
            while (i < 2)
            {
                opt_og = oy.get_next_page();

                if (!opt_og.has_value())
                {
                    break; /* Too little data so far */
                }
                os.add_page(opt_og.value());
                while (i < 2)
                {
                    std::optional<ogg_packet> opt_header = os.get_next_packet();
                    if (!opt_header.has_value())
                        break;
                    vorbis_synthesis_headerin(&vi.raw(), &vc.raw(), &opt_header.value().raw());
                    if (i == 1)
                    {
                        book = opt_header.value().raw();
                    }
                    i++;
                }
            }
            if (!opt_og.has_value() && i < 2)
            {
                throw Exception::Generic("EOF before end of vorbis headers.");
            }
        }

        /* Copy the vendor tag */
        vendor = vc.vendor();

        /* Fill in comments map */
        for (i = 0; i < vc.raw().comments; i++)
        {
            const auto equalpos = std::strchr(vc.raw().user_comments[i], '=');
            if (equalpos == nullptr)
                throw Exception::Generic(Stringify() << "Non valid commment:" << vc.raw().user_comments[i]);

            const auto lsize = static_cast<unsigned int>(equalpos - &vc.raw().user_comments[i][0]);
            std::string lval(vc.raw().user_comments[i], lsize);
            VG::UPPERCASE(lval);

            const auto rsize = static_cast<unsigned int>(vc.raw().comment_lengths[i]) - (lsize + 1U);
            std::string rval(equalpos + 1, rsize);

            m_comments.emplace_back(std::make_pair(std::move(lval), std::move(rval)));
        }

        /* Headers are done! */
    }

    void vcedit_write(vcedit_state* state, const path_to_a_file& out)
    {
        try
        {
            std::optional<ogg_page> opt_ogin;
            std::optional<ogg_packet> opt_op;
            ogg_int64_t granpos = 0;

            ogg_stream streamout(state->serial);

            ofile ofile_out(out);

            auto header_comments = _commentheader_out(state->getComments(), state->vendor);
            auto header_comments_packet = header_comments.get_packet();

            streamout.add_packet(state->main);
            streamout.add_packet(header_comments_packet);
            streamout.add_packet(state->book);

            bool needflush = false, needout = false;

            streamout.flush_all_to_file(ofile_out);

            while (_fetch_next_packet(state, opt_op, opt_ogin))
            {
                // opt_op is filledin
                const auto size = _blocksize(state, opt_op.value());
                granpos += size;

                std::optional<ogg_page> opt_ogout;

                if (needflush)
                {
                    opt_ogout = streamout.flush_to_page();
                    needflush = false;
                }
                else if (needout)
                {
                    opt_ogout = streamout.pageout();
                    needout = false;
                }
                if (opt_ogout.has_value())
                {
                    opt_ogout->write_to_file(ofile_out);
                }

                if (opt_op->raw().granulepos == -1)
                {
                    opt_op->raw().granulepos = granpos;
                }
                else /* granulepos is set, validly. Use it, and force a flush to
                    account for shortened blocks (vcut) when appropriate */
                {
                    if (granpos > opt_op->raw().granulepos)
                    {
                        granpos = opt_op->raw().granulepos;
                        needflush = true;
                    }
                    else
                    {
                        needout = true;
                    }
                }
                streamout.add_packet(opt_op.value());
            }

            streamout.set_eos(true);
            streamout.flush_all_to_file(ofile_out);

            if (state->extrapage) // implies ogin has a value
            {
                opt_ogin->write_to_file(ofile_out);
            }

            while (true) /* We reached eos, not eof */
            {
                /* We copy the rest of the stream (other logical streams)
                 * through, a page at a time. */
                std::optional<ogg_page> opt_ogout = state->oy.get_next_page();

                if (!opt_ogout.has_value())
                    break;

                /* Don't bother going through the rest, we can just
                 * write the page out now */
                opt_ogout->write_to_file(ofile_out);
            }
        }
        catch (const std::runtime_error& ex)
        {
            // todo remove this case
            if (!state->eosin)
            {
                auto err = VG::Stringify();
                err << "Error writing stream to output. Output stream may be corrupted or truncated. "
                    << std::string_view(ex.what());
                throw Exception::Generic(err);
            }
        }
    }

    vorbis_comment::vorbis_comment()
    {
        init();
    }

    vorbis_comment::~vorbis_comment()
    {
        clear();
    }

    std::string_view vorbis_comment::vendor() const
    {
        return m_vc.vendor;
    }

    ::vorbis_comment& vorbis_comment::raw() noexcept
    {
        return m_vc;
    }

    void vorbis_comment::init() noexcept
    {
        vorbis_comment_init(&m_vc);
    }

    void vorbis_comment::clear() noexcept
    {
        vorbis_comment_clear(&m_vc);
    }

    void ogg_stream::init(int serial)
    {
        VG::C::ogg_stream_init::call(&m_os, serial);
    }

    ogg_stream::ogg_stream(int serial)
    {
        init(serial);
    }

    ogg_stream::~ogg_stream()
    {
        ogg_stream_clear(&m_os);
    }

    void ogg_stream::add_page(ogg_page& page)
    {
        VG::C::ogg_stream_pagein::call(&m_os, &page.raw());
    }

    void ogg_stream::add_packet(ogg_packet& packet)
    {
        VG::C::ogg_stream_packetin::call(&m_os, &packet.raw());
    }

    std::optional<ogg_packet> ogg_stream::get_next_packet()
    {
        ogg_packet op;
        const auto ret = VG::C::ogg_stream_packetout::call(&m_os, &op.raw());
        if (ret == 0)
        {
            return std::nullopt;
        }
        return op;
    }

    std::optional<ogg_page> ogg_stream::flush_to_page()
    {
        ogg_page op;
        const auto ret = ogg_stream_flush(&m_os, &op.raw());
        if (ret == 0)
        {
            return std::nullopt;
        }
        return op;
    }

    void ogg_stream::flush_all_to_file(ofile& f)
    {
        while (true)
        {
            auto op = flush_to_page();
            if (!op.has_value())
            {
                break;
            }
            op->write_to_file(f);
        }
    }

    std::optional<ogg_page> ogg_stream::pageout()
    {
        ogg_page op;
        const auto ret = ogg_stream_pageout(&m_os, &op.raw());
        if (ret == 0)
        {
            return std::nullopt;
        }
        return op;
    }

    void ogg_stream::set_eos(bool is_eos) noexcept
    {
        m_os.e_o_s = is_eos;
    }

    ogg_sync::ogg_sync(VG::ifile& in) : m_infile(in)
    {
        ogg_sync_init(&m_oy);
    }

    ogg_sync::~ogg_sync()
    {
        ogg_sync_clear(&m_oy);
    }

    std::optional<ogg_page> ogg_sync::get_next_page(unsigned int max_reads)
    {
        ogg_page page;
        for (unsigned int i = 0; ogg_sync_pageout(&m_oy, &page.raw()) <= 0; ++i)
        {
            if (max_reads != 0 && i >= max_reads)
                return std::nullopt;
            const auto bytes = read();
            if (bytes == 0)
                return std::nullopt;
        }
        return page;
    }

    void ogg_sync::wrote(std::size_t bytes)
    {
        C::ogg_sync_wrote::call(&m_oy, static_cast<long>(bytes));
    }

    unsigned long ogg_sync::read()
    {
        static constexpr size_t CHUNKSIZE = 4096;

        std::byte* const buffer = reinterpret_cast<std::byte*>(C::ogg_sync_buffer::call(&m_oy, static_cast<long>(CHUNKSIZE)));
        const auto bytes = m_infile.read(buffer, CHUNKSIZE);
        wrote(bytes);

        return bytes;
    }

    vorbis_info::vorbis_info()
    {
        vorbis_info_init(&m_vi);
    }

    vorbis_info::~vorbis_info()
    {
        vorbis_info_clear(&m_vi);
    }

    long vorbis_info::packet_blocksize(ogg_packet& op)
    {
        return C::vorbis_packet_blocksize::call(&m_vi, &op.raw());
    }

    ::vorbis_info& vorbis_info::raw() noexcept
    {
        return m_vi;
    }

    oggpack::oggpack()
    {
        oggpack_writeinit(&m_opb);
    }

    oggpack::~oggpack()
    {
        oggpack_writeclear(&m_opb);
    }

    void oggpack::write1(bool val) noexcept
    {
        oggpack_write(&m_opb, val, 1);
    }

    void oggpack::write8(uint8_t val) noexcept
    {
        oggpack_write(&m_opb, val, 8);
    }

    void oggpack::write32(uint32_t val) noexcept
    {
        oggpack_write(&m_opb, val, 32);
    }

    void oggpack::writestr(const mystring_view& str) noexcept
    {
        for (const auto& c : str.get())
        {
            write8(static_cast<uint8_t>(c));
        }
    }

    ogg_packet oggpack::get_packet()
    {
        ogg_packet op;

        const auto bytes = oggpack_bytes(&m_opb);

        op.raw().bytes = bytes;
        op.raw().packet = oggpack_get_buffer(&m_opb);

        return op;
    }

    ogg_packet::ogg_packet() : m_managed(false)
    {
        init();
    }

    void ogg_packet::copy(const ::ogg_packet& op)
    {
        clear();
        m_op = op;
        m_managed = true;

        copy_raw(op.packet, static_cast<unsigned int>(op.bytes));
    }

    void ogg_packet::copy_raw(const unsigned char* src, unsigned int len)
    {
        m_op.packet = new unsigned char[len];
        std::copy(src, src + len, m_op.packet);
    }

    void ogg_packet::clear()
    {
        if (m_managed)
            delete[] m_op.packet;
        m_op.packet = nullptr;
    }

    void ogg_packet::init()
    {
        m_op.bytes = 0;
        m_op.packet = nullptr;

        m_op.b_o_s = false;
        m_op.e_o_s = false;

        m_op.granulepos = 0;
        m_op.packetno = 0;
    }

    ogg_packet::ogg_packet(ogg_packet&& op) noexcept : m_managed(op.m_managed)
    {
        m_op = op.m_op;
        op.m_op.packet = nullptr;
    }

    ogg_packet& ogg_packet::operator=(const ::ogg_packet& op)
    {
        copy(op);
        return *this;
    }

    ogg_packet::~ogg_packet()
    {
        clear();
    }

    ::ogg_packet& ogg_packet::raw() noexcept
    {
        return m_op;
    }

    void ogg_page::write_header(ofile& out) const
    {
        out.write(reinterpret_cast<std::byte*>(m_opage.header), static_cast<std::size_t>(m_opage.header_len));
    }

    void ogg_page::write_body(ofile& out) const
    {
        out.write(reinterpret_cast<std::byte*>(m_opage.body), static_cast<std::size_t>(m_opage.body_len));
    }

    void ogg_page::write_to_file(ofile& out) const
    {
        write_header(out);
        write_body(out);
    }

    bool ogg_page::eos()
    {
        auto ret = ogg_page_eos(&m_opage);
        if (ret > 0)
            return true;
        else
            return false;
    }

    int ogg_page::serialno() noexcept
    {
        return ogg_page_serialno(&m_opage);
    }

    // todo remove
    ::ogg_page& VG::ogg_page::raw() noexcept
    {
        return m_opage;
    }
} // namespace VG
