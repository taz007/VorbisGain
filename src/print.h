#ifndef VG_PRINT_H
#define VG_PRINT_H

#include <iostream>

#include "mystring_view.h"
#include "stringify.h"

namespace VG
{
    namespace zzzz_details
    {
        enum class print_ending
        {
            new_line,
            flush
        };

        constexpr void print_actual(std::ostream&) noexcept
        {
        }

        void print_actual(std::ostream& out, const mystring_view& str);
        void print_actual(std::ostream& out, const Stringify& str);

        // todo make sure this is noexcept
        template<print_ending ending, typename... STR>
        void print(std::ostream& out, STR&&... str)
        {
            print_actual(out, std::forward<STR>(str)...);

            if constexpr (ending == print_ending::new_line)
                out << '\n';
            else if constexpr (ending == print_ending::flush)
                out << std::flush;
        }
    } // namespace zzzz_details

    template<typename... STR>
    void print_error(STR&&... str)
    {
        zzzz_details::print<zzzz_details::print_ending::new_line>(std::cerr, std::forward<STR>(str)...);
    }

    template<typename... STR>
    void print_line(STR&&... str)
    {
        zzzz_details::print<zzzz_details::print_ending::new_line>(std::cout, std::forward<STR>(str)...);
    }

    template<typename... STR>
    void print_flush(STR&&... str)
    {
        zzzz_details::print<zzzz_details::print_ending::flush>(std::cout, std::forward<STR>(str)...);
    }

    template<typename... STR>
    void print_line_to(std::ostream& out, STR&&... str)
    {
        zzzz_details::print<zzzz_details::print_ending::new_line>(out, std::forward<STR>(str)...);
    }
} // namespace VG
#endif
