#include <iostream>

#include "print.h"
#include "vorbisgain.h"

int main(int argc, char** argv)
{
    try
    {
        std::ios::sync_with_stdio(false);

        return VG::parse_and_run(argc, argv);
    }
    catch (const std::exception& ex)
    {
        VG::print_error(std::string_view(ex.what()));
        return EXIT_FAILURE;
    }
    catch (...)
    {
        VG::print_error("Unknown exception occured.");
        return EXIT_FAILURE;
    }
}
