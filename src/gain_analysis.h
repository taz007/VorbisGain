#ifndef VG_GAIN_ANALYSIS_H
#define VG_GAIN_ANALYSIS_H

#include <algorithm>
#include <array>
#include <optional>

#include "non_copy.h"

namespace VG
{
    constexpr unsigned int STEPS_per_dB = 100; /* Table entries per dB */
    constexpr unsigned int MAX_dB = 120;       /* Table entries for 0...MAX_dB (normal max. values are 70...80 dB) */

    constexpr unsigned int RMS_WINDOW_RATIO = 20;
    constexpr unsigned int MAX_SAMP_FREQ = 48000; /* maximum allowed sample frequency [Hz] */
    constexpr unsigned int MAX_SAMPLES_PER_WINDOW = (MAX_SAMP_FREQ / RMS_WINDOW_RATIO + 1); /* max. Samples per Time slice */

    constexpr std::size_t YULE_ORDER = 10;
    constexpr std::size_t BUTTER_ORDER = 2;
    constexpr std::size_t MAX_ORDER = std::max(BUTTER_ORDER, YULE_ORDER);

    struct FilterInfo
    {
        struct Yule
        {
            using type_a = std::array<float, 10>;
            using type_b = std::array<float, 11>;
            type_a a;
            type_b b;
        } yule;

        struct Butter
        {
            using type_a = std::array<float, 2>;
            using type_b = std::array<float, 3>;
            type_a a;
            type_b b;
        } butter;

        unsigned int samples_window; /* number of samples required to reach number of milliseconds required for RMS window */
    };

    struct OneReplayGainData
    {
        std::array<unsigned int, STEPS_per_dB * MAX_dB> data = {};
    };

    struct ContextOneChannel
    {
        void updatePrebuf(const float* pcm, size_t nbrSamplesToCopy);

        // the buffers are split like this : [MAX_ORDER_size| actual data....] with MAX_ORDER_size containing
        // the last MAX_ORDER input/output data from the previous call (or zero if the first call)
        std::array<float, MAX_ORDER * 2> inprebuf =
            {}; /* input samples, with pre-buffer, used when input index is less than MAX_ORDER */
        std::array<float, MAX_ORDER + MAX_SAMPLES_PER_WINDOW> stepbuf = {}; /* "first step" (i.e. post first filter) samples */
        std::array<float, MAX_ORDER + MAX_SAMPLES_PER_WINDOW> outbuf = {};  /* "out" (i.e. post second filter) samples */
        double sum = {};
    };

    struct ContextOneSong : non_copy_moveable
    {
        explicit ContextOneSong(bool isStereo, unsigned int frequency);

        [[nodiscard]] constexpr bool isStereo() const noexcept
        {
            return right.has_value();
        };

        [[nodiscard]] std::optional<float> GetTitleGain() const;

        [[nodiscard]] constexpr const std::optional<float>& getPeak() const noexcept
        {
            return m_peak;
        };

        void AnalyzeSamples(const float* const* pcm, std::size_t nbrSamples);

        ContextOneChannel left;
        std::optional<ContextOneChannel> right;
        const FilterInfo& filterinfo;
        std::size_t totsamp; // number of already analyzed samples regarding the current sampleWindow
        OneReplayGainData A;

      private:
        void updatePrebuffer(const float* const* pcm, std::size_t nbrSamples);
        [[nodiscard]] std::size_t remainingSampleWindow() const noexcept;
        void callFilterYule(const float* const* pcm, std::size_t cursamplepos, std::size_t cursamples);
        void callFilterYuleForOneChannel(ContextOneChannel& chan, const float* pcm, std::size_t cursamplepos,
                                         std::size_t cursamples);
        [[nodiscard]] const float* computeInputPtrYule(ContextOneChannel& chan, const float* pcm, std::size_t cursamplepos);
        void callFilterButter(std::size_t cursamples);
        void callFilterButterForOneChannel(ContextOneChannel& chan, std::size_t cursamples);
        void updateSum(std::size_t cursamples);
        void updateStateFinalStep(std::size_t cursamples);
        [[nodiscard]] double getMeanSumChannels() const noexcept;
        void resetSum();
        void updateOutBufs();
        void updateStepBufs();
        void shiftPrebuffer(std::size_t num_samples);
        void preparePreBufferforNext(const float* const* pcm, std::size_t num_samplesToCopy, std::size_t sizePcm);
        void updateSumForOneChannel(ContextOneChannel& chan, std::size_t cursamples);

        std::optional<float> m_peak;

        template<typename OP>
        void applyToChannels(OP op)
        {
            op(left);
            if (isStereo())
                op(*right);
        }

        template<typename InputIter, typename OP>
        void applyToChannels(InputIter input, OP op)
        {
            op(*input, left);
            if (isStereo())
            {
                ++input;
                op(*input, *right);
            }
        }
    };

    class ContextOneAlbum
    {
      public:
        [[nodiscard]] std::optional<float> GetAlbumGain() const;
        void updateWithCurrentSong(const ContextOneSong& currentSong);

      private:
        OneReplayGainData B;
    };
} // namespace VG
#endif /* GAIN_ANALYSIS_H */
