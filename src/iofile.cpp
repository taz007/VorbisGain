#include "iofile.h"

#include <array>
#include <climits>

#include "c_calls.h"

namespace VG
{
    ifile::ifile(const path_to_a_file& str) : parent(parent::iofile_openmode::RO, str)
    {
    }

    std::optional<std::string> ifile::getline()
    {
        struct freehelper
        {
            static void call(char* buf) noexcept
            {
                std::free(buf);
            }
        };

        using stdfree_deleter = deleter<freehelper, char*, nullptr, false>;
        using unique_free = unique_resource<stdfree_deleter>;

        unique_free line;
        std::size_t buflen = 0;
        ssize_t nread = 0;

        nread = C::getline::call(line.get_storage_i_know_what_im_doing(), &buflen, m_file.get());
        if (nread > 0)
        {
            // checking the last char
            // if there was a newline, which is expected, don't copy it
            // if there was no newline, we reached EOF, so no \n to remove
            if (line.get()[nread - 1] == '\n')
                --nread;

            return std::optional<std::string>(std::in_place, line.get(), nread);
        }
        else
        {
            return {};
        }
    }

    std::size_t ifile::read(std::byte* buf, std::size_t count)
    {
        return zzzz_details::myfread(m_file.get(), buf, count);
    }

    ofile::ofile(const path_to_a_file& str) : parent(parent::iofile_openmode::WO, str)
    {
    }

    void ofile::write(const std::byte* buf, std::size_t count)
    {
        zzzz_details::myfwrite(m_file.get(), buf, count);
    }

    ofile& ofile::operator<<(char c)
    {
        const std::size_t len = 1;
        const std::byte* const ptr = reinterpret_cast<std::byte*>(&c);

        write(ptr, len);

        return *this;
    }

    ofile& ofile::operator<<(const mystring_view& str)
    {
        const auto len = str.get().length();
        const auto ptr = reinterpret_cast<const std::byte*>(str.get().data());

        write(ptr, len);

        return *this;
    }

    void ofile::copyfile(ifile& in)
    {
        std::array<std::byte, 65536> buffer;

        std::size_t oneread = 0;
        while (true)
        {
            oneread = in.read(buffer.data(), buffer.size());
            if (oneread != 0)
                write(buffer.data(), oneread);
            else
                break;
        }
    }
} // namespace VG
