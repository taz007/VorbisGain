#ifndef VG_VORBISGAINEXCEPTION_H
#define VG_VORBISGAINEXCEPTION_H

#include <system_error>

namespace VG
{
    class VorbisFileErrorCategory : public std::error_category
    {
      public:
        [[nodiscard]] std::string message(int ev) const override;
        [[nodiscard]] const char* name() const noexcept override;
    };

    namespace Exception
    {
        class VorbisFile : public std::system_error
        {
          public:
            explicit VorbisFile(int vorbiserror, const std::string& what_arg);

          private:
            static const VorbisFileErrorCategory errorCategory;
        };

        class SystemError : public std::system_error
        {
          public:
            explicit SystemError(int errnbr, const std::string& what_arg);
        };

        class Generic : public std::runtime_error
        {
          public:
            explicit Generic(const std::string& what);
        };

    } // namespace Exception

} // namespace VG

#endif // VORBISGAINEXCEPTION_H
