#include "vorbisgain.h"

#include <iomanip>
#include <iostream>

#include <getopt.h>

#include "config.h"
#include "gain_analysis.h"
#include "print.h"
#include "recurse.h"
#include "stringify.h"
#include "vorbis.h"
#include "vorbisgainexception.h"

namespace VG
{
    /**
     * \brief Convert tags from old to new style.
     *
     * Convert tags from old to new style. Checks that the album gain is the same
     * for all files, when in album mode.
     *
     * \param files     list of files to process.
     * \param settings  global settings and data.
     * \return  0 if successful and -1 if an error occured (in which case a
     *          message has been printed).
     */
    void convert_files(FILE_LIST& file_list, const SETTINGS& settings)
    {
        std::optional<float> album_gain; // just for checking consistencies, not used as a value for write
        std::optional<float> album_peak;
        int convert = 0;

        for (auto& file : file_list)
        {
            bool tags;
            try
            {
                tags = get_old_tags(file.file, settings.album, file.track_peak, file.track_gain, file.album_gain);
            }
            catch (const std::runtime_error&)
            {
                if (settings.skip)
                {
                    file.skip = true;
                    continue;
                }
                else
                {
                    throw;
                }
            }
            file.skip = !tags;
            convert |= tags;

            if (settings.album)
            {
                if (!tags)
                {
                    throw Exception::Generic(Stringify() << "Tags required for album mode conversion not found in '"
                                                         << file.file.str() << "'");
                }

                if (album_gain.has_value())
                {
                    if (*file.album_gain != *album_gain)
                        throw Exception::Generic(Stringify()
                                                 << "Album gain in '" << file.file.str() << "' differs from previous files ("
                                                 << *file.album_gain << " vs " << *album_gain << ")");
                }
                else
                {
                    album_gain = *file.album_gain;
                }

                if (!album_peak.has_value() || *file.track_peak > *album_peak)
                {
                    album_peak = *file.track_peak;
                }
            }
        }

        if (!convert)
        {
            print_error("No old style ReplayGain tags found; no files processed");
            return;
        }

        for (const auto& file : file_list)
        {
            if (file.skip)
            {
                continue;
            }

            if (settings.display_only)
            {
                if (!settings.quiet)
                {
                    print_error(Stringify() << "Found old style ReplayGain tags in '" << file.file.str() << "'");
                }
            }
            else
            {
                write_gains(file.file, file.track_peak, file.track_gain, album_peak, file.album_gain, settings, false);
            }
        }
    }

    /**
     * Remove VorbisGain tags from files.
     *
     * \param files     list of files to process.
     * \param settings  global settings and data.
     * \return  0 if successful and -1 if an error occured (in which case a
     *          message has been printed).
     */
    void clean_files(const FILE_LIST& file_list, const SETTINGS& settings)
    {
        /* Remove ReplayGain tags */
        for (const auto& file : file_list)
        {
            try
            {
                const bool tags = has_tags(file.file, has_tags_category::any);

                if (tags)
                {
                    if (settings.display_only)
                    {
                        if (!settings.quiet)
                        {
                            print_line(Stringify() << "Found ReplayGain tags in '" << file.file.str() << "'");
                        }
                    }
                    else
                    {
                        write_gains(file.file, {}, {}, {}, {}, settings, true);
                    }
                }
                else
                {
                    if (!settings.quiet)
                    {
                        print_line(Stringify() << "No ReplayGain tags to remove in '" << file.file.str() << "'");
                    }
                }
            }
            catch (const std::runtime_error& ex)
            {
                if (!settings.skip)
                {
                    throw;
                }
                else
                {
                    print_error(Stringify() << "Skipping file '" << file.file.str() << "'");
                }
            }
        }
    }

    /**
     * \brief Processs the file in file_list.
     *
     * Process the files in file_list. If settings->album is set, the files are
     * considered to belong to one album.
     *
     * \param file_list  list of files to process.
     * \param settings   settings and global variables.
     * \return  0 if successful and -1 if an error occured (in which case a
     *          message has been printed).
     */
    void process_files(FILE_LIST& file_list, const SETTINGS& settings, ProcessingContext& ctxt)
    {
        std::optional<float> album_peak;
        bool analyze = true;

        if (settings.convert)
        {
            convert_files(file_list, settings);
            return;
        }

        if (settings.clean)
        {
            clean_files(file_list, settings);
            return;
        }

        ctxt.first_file = true;

        if (settings.fast)
        {
            analyze = false;

            /* Figure out which files need to be processed */
            for (auto& file : file_list)
            {
                try
                {
                    const has_tags_category cat = settings.album ? has_tags_category::track_album : has_tags_category::track;
                    const bool tags = has_tags(file.file, cat);

                    analyze |= !tags;

                    if (settings.album && analyze)
                    {
                        /* In album mode, all tags must be present in all files (this
                         * check isn't necessary, but avoids unneeded tag searches)
                         */
                        break;
                    }

                    if (tags && !settings.album)
                    {
                        file.skip = true;
                    }
                }
                catch (const std::runtime_error& ex)
                {
                    /* Skip non-vorbis files in recursive mode */
                    if (settings.skip)
                    {
                        file.skip = true;
                        print_error(Stringify() << "Skipping file '" << file.file.str() << "'");
                    }
                    else
                        throw;
                }
            }

            if (!analyze && !settings.album_gain.has_value())
            {
                /* All files fully tagged and album gain doesn't need to be changed */
                if (!settings.quiet)
                {
                    print_error("Tags present; no files processed");
                }

                return;
            }
        }

        std::unique_ptr<ContextOneAlbum> albumCtxt(std::make_unique<ContextOneAlbum>());

        if (analyze)
        {
            /* Analyze the files, write track gain if not in album mode */
            for (auto& file : file_list)
            {
                if (file.skip)
                {
                    continue;
                }

                try
                {
                    get_gain(file.file, file.track_peak, file.track_gain, settings, ctxt, albumCtxt);
                }
                catch (const std::runtime_error& ex)
                {
                    /* Skip non-vorbis and erraneous files. */
                    if (settings.skip)
                    {
                        file.skip = true;
                        continue;
                    }
                    else
                    {
                        throw;
                    }
                }

                if (settings.album && file.track_peak.has_value() &&
                    (!album_peak.has_value() || (*file.track_peak > *album_peak)))
                {
                    album_peak = *file.track_peak;
                }

                if (!settings.album && !settings.display_only)
                {
                    write_gains(file.file, file.track_peak, file.track_gain, {}, {}, settings, false);
                }
            }
        }

        if (settings.album)
        {
            using namespace std::literals;

            /* Write track and album gains */
            std::string_view gain_message;
            std::optional<float> album_gain;

            if (settings.album_gain.has_value())
            {
                album_gain = settings.album_gain;
                gain_message = "Setting Album Gain to: "sv;
            }
            else
            {
                album_gain = albumCtxt->GetAlbumGain();
                gain_message = "Recommended Album Gain: "sv;
            }

            if (!settings.quiet)
            {
                Stringify s;

                s << gain_message;
                if (album_gain)
                    s << std::showpos << std::setw(2) << std::fixed << std::setprecision(2) << *album_gain << " dB";
                else
                    s << "N/A";

                print_line();
                print_line(s);
            }

            if (!settings.display_only)
            {
                for (const auto& file : file_list)
                {
                    if (file.skip)
                    {
                        continue;
                    }

                    write_gains(file.file, file.track_peak, file.track_gain, album_peak, album_gain, settings, false);
                }
            }
        }
    }

    /**
     * Print out a list of options and the command line syntax.
     */
    static void print_help(std::ostream& os)
    {
        print_line_to(os, Stringify() << "VorbisGain " << VG_VERSION);
        print_line_to(os, "Copyright (c) 2002-2005 Gian-Carlo Pascutto <gcp@sjeng.org>");
        print_line_to(os, "                        and Magnus Holmgren <lear@algonet.se>");
        print_line_to(os, "              2016-2022 Taz <taz.007@zoho.com>");
        print_line_to(os);
        print_line_to(os, "Usage: vorbisgain [options] input.ogg [...]");
        print_line_to(os);
        print_line_to(os, "OPTIONS:");
        print_line_to(os, "  -a, --album               Run in Album (Audiophile) mode.");
        print_line_to(os, "  -g, --album-gain=n        Set Album gain to the specified value. Implies -a");
        print_line_to(os, "  -c, --clean               Remove any VorbisGain tags. No gains are calculated");
        print_line_to(os, "  -C, --convert             Convert VorbisGain tags from old to new style");
        print_line_to(os, "  -d, --display-only        Display results only. No files are modified");
        print_line_to(os, "  -f, --fast                Don't recalculate tagged files");
        print_line_to(os, "  -h, --help                Print this help text");
        print_line_to(os, "  -k, --keep-timestamps     Keep the original timestamps");
        print_line_to(os, "  -n, --no-progress         Don't show progress, just print results");
        print_line_to(os, "  -q, --quiet               Don't print any output (except errors)");
        print_line_to(os, "  -r, --recursive           Search for files recursively, each folder as an album");
        print_line_to(os, "  -x, --regex-filter=REGEX  In recursive mode, only scan files matching the given regex");
        print_line_to(os, "  -s, --skip                Skip non-Vorbis or faulty files");
        print_line_to(os, "  -v, --version             Display version number and exit");
        print_line_to(os, "INPUT FILES:");
        print_line_to(os, "  ReplayGain input files must be Ogg Vorbis I files with");
        print_line_to(os, "  1 or 2 channels and a sample rate of 48 kHz, 44.1 kHz,");
        print_line_to(os, "  32 kHz, 24 kHz, 22050 Hz, 16 kHz, 12 kHz, 11025 Hz or 8 kHz.");
        return;
    }

    static constexpr struct option long_options[] = {{"album", 0, NULL, 'a'},
                                                     {"clean", 0, NULL, 'c'},
                                                     {"convert", 0, NULL, 'C'},
                                                     {"display-only", 0, NULL, 'd'},
                                                     {"fast", 0, NULL, 'f'},
                                                     {"album-gain", 1, NULL, 'g'},
                                                     {"help", 0, NULL, 'h'},
                                                     {"keep-timestamps", 0, NULL, 'k'},
                                                     {"no-progress", 0, NULL, 'n'},
                                                     {"quiet", 0, NULL, 'q'},
                                                     {"recursive", 0, NULL, 'r'},
                                                     {"regex-filter", 1, NULL, 'x'},
                                                     {"skip", 0, NULL, 's'},
                                                     {"version", 0, NULL, 'v'},
                                                     {NULL, 0, NULL, 0}};

#define ARG_STRING "acCdfg:hknqrx:sv"

    static constexpr float MAX_GAIN_ERR = 51.f;
    static constexpr float MIN_GAIN_ERR = -51.f;
    static constexpr float MAX_GAIN_WARN = 17.f;
    static constexpr float MIN_GAIN_WARN = -23.f;

    int parse_and_run(int argc, char** argv)
    {

        SETTINGS settings;
        int option_index = 1;
        int ret;

        bool arg_print_help = false;
        bool arg_print_version = false;

        while ((ret = getopt_long(argc, argv, ARG_STRING, long_options, &option_index)) != -1)
        {
            switch (ret)
            {
            case 0:
                print_error("Internal error parsing command line options");
                return EXIT_FAILURE;

            case 'a':
                settings.album = true;
                break;

            case 'g':
                settings.album = true;

                {
                    const auto tmp_gain = readfloat_if_possible(optarg);

                    if (!tmp_gain.has_value())
                    {
                        print_error(Stringify() << "Album gain \"" << optarg << "\" not recognised");
                        return EXIT_FAILURE;
                    }

                    /* Check the value for sanity. */
                    if ((tmp_gain < MIN_GAIN_ERR) || (tmp_gain > MAX_GAIN_ERR))
                    {
                        print_error(Stringify() << "Album gain \"" << *tmp_gain << "\" is out of range (" << MIN_GAIN_ERR
                                                << " to " << MAX_GAIN_ERR << " dB)");
                        return EXIT_FAILURE;
                    }
                    else
                    {
                        if ((tmp_gain < MIN_GAIN_WARN) || (tmp_gain > MAX_GAIN_WARN))
                        {
                            print_error(Stringify() << "Note: An album gain of \"" << *tmp_gain
                                                    << "\" is unlikely (limit the album gain to be within the range "
                                                    << MIN_GAIN_WARN << " to " << MAX_GAIN_WARN << " dB to avoid this warning)");
                        }
                    }
                    settings.album_gain = tmp_gain;
                }

                break;

            case 'c':
                settings.clean = true;
                break;

            case 'C':
                settings.convert = true;
                break;

            case 'd':
                settings.display_only = true;
                break;

            case 'f':
                settings.fast = true;
                break;

            case 'n':
                settings.show_progress = false;
                break;

            case 'q':
                settings.quiet = true;
                break;

            case 'r':
                settings.recursive = true;
                break;

            case 'x':
                settings.regex.emplace(optarg, std::regex_constants::optimize);
                break;

            case 's':
                settings.skip = true;
                break;

            case 'v':
                arg_print_version = true;
                break;

            case 'h':
                arg_print_help = true;
                break;

            case 'k':
                settings.keep_timestamps = true;
                break;

            default:
                print_help(std::cerr);
                return EXIT_FAILURE;
            }
        }

        if (arg_print_help)
        {
            print_help(std::cout);
            return EXIT_SUCCESS;
        }

        if (arg_print_version)
        {
            print_line(Stringify() << "VorbisGain " << VG_VERSION);
            return EXIT_SUCCESS;
        }

        if (settings.regex.has_value() && !settings.recursive)
        {
            print_error("Filtering only available in recursive mode");
            print_help(std::cerr);
            return EXIT_FAILURE;
        }

        if (optind >= argc)
        {
            print_error("No files specified.");
            print_help(std::cerr);
            return EXIT_FAILURE;
        }

        ProcessingContext ctxt;

        ArgsToDirsAndFilesList args(&argv[optind], &argv[argc]);

        processDirsAndFiles(args, settings, ctxt);

        return EXIT_SUCCESS;
    }
} // namespace VG
