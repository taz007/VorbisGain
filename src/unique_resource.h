#ifndef VG_UNIQUE_RESOURCE_H
#define VG_UNIQUE_RESOURCE_H

#include <utility>

#include "ignore_exception.h"
#include "non_copy.h"

namespace VG
{
    template<typename Deleter>
    class unique_resource : non_copy_moveable
    {
        using T = typename Deleter::type;

      public:
        constexpr unique_resource() noexcept : m_resource(Deleter::nonvalid_value)
        {
        }

        template<typename TT>
        explicit unique_resource(TT&& arg) noexcept : m_resource(std::forward<TT>(arg))
        {
        }

        [[nodiscard]] constexpr T* get_storage_i_know_what_im_doing() noexcept
        {
            return &m_resource;
        }

        [[nodiscard]] constexpr const T& get() const noexcept
        {
            return m_resource;
        }

        void reset()
        {
            del();
            m_resource = Deleter::nonvalid_value;
        }

        template<typename TT>
        void reset(TT&& arg)
        {
            if (this == &arg)
                return;

            del();
            m_resource = std::forward<TT>(arg);
        }

        [[nodiscard]] T release() noexcept
        {
            T backup_value(std::move(m_resource));
            m_resource = Deleter::nonvalid_value;
            return backup_value;
        }

        ~unique_resource()
        {
            ignore_exception([&] { del(); });
        }

      private:
        void del()
        {
            Deleter D;
            D(m_resource);
        }

        T m_resource;
    };

    template<typename T, typename ARGTYPE, ARGTYPE nonvalid_val, bool do_check_before_delete>
    struct deleter
    {
        using type = ARGTYPE;
        static constexpr ARGTYPE nonvalid_value{nonvalid_val};

        template<typename TT>
        void operator()(TT&& arg)
        {
            if constexpr (do_check_before_delete)
            {
                if (arg == nonvalid_val)
                    return;
            }

            T::call(std::forward<TT>(arg));
        }
    };
} // namespace VG

#endif // UNIQUE_RESOURCE_H
