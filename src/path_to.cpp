#include "path_to.h"

namespace VG
{
    std::string VG::path_to_a_dir::str() const
    {
        std::string fullpath;
        for (const auto& node : m_dirs)
        {
            fullpath += node.name();
        }
        return fullpath;
    }

    namespace zzzz_details
    {
        std::string path_to_a_node::str() const
        {
            std::string fullpath = m_pathdir.str();
            fullpath += m_node_file.name();
            return fullpath;
        }

        const path_to_a_dir& path_to_a_node::dir() const noexcept
        {
            return m_pathdir;
        }

        const path_node& path_to_a_node::node() const noexcept
        {
            return m_node_file;
        }

        path_node& path_to_a_node::node() noexcept
        {
            return m_node_file;
        }
    } // namespace zzzz_details

    const std::string& path_node::name() const noexcept
    {
        return m_entry;
    }

    std::string& path_node::name() noexcept
    {
        return m_entry;
    }

    const path_node& path_to_something::something() const noexcept
    {
        return node();
    }

    const path_node& path_to_a_file::file_node() const noexcept
    {
        return node();
    }

    path_node& path_to_a_file::file_node() noexcept
    {
        return node();
    }

    path_to_a_file make_path_to_a_file(const mystring_view& raw_path_to_file)
    {
        path_to_a_dir d;
        std::string_view::size_type pos_begin = 0;
        while (true)
        {
            const auto pos_found = raw_path_to_file.get().find(path_node_dir::dir_separator, pos_begin);
            if (pos_found != std::string_view::npos)
            {
                d.add_subdir(raw_path_to_file.get().substr(pos_begin, pos_found - pos_begin));
                pos_begin = pos_found + 1;
            }
            else
                break;
        }

        path_to_a_file f(std::move(d), raw_path_to_file.get().substr(pos_begin));
        return f;
    }

} // namespace VG
