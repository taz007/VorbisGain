#ifndef VG_MYSTRINGVIEW_H
#define VG_MYSTRINGVIEW_H

#include <string>
#include <string_view>
#include <type_traits>
#include <utility>

#include "non_copy.h"

namespace VG
{
    // TODO remove this when C++20
    template<class T>
    struct rem_cvref
    {
        using type = std::remove_cv_t<std::remove_reference_t<T>>;
    };
    template<class T>
    using rem_cvref_t = typename rem_cvref<T>::type;

    template<class T, class U>
    using is_same_cvr = typename std::is_same<rem_cvref_t<T>, rem_cvref_t<U>>::type;

    template<class T, class U>
    constexpr bool is_same_cvr_v = is_same_cvr<T, U>::value;

    class mystring_view : non_copy_moveable
    {
      public:
        template<typename T, typename = std::enable_if_t<is_same_cvr_v<T, std::string_view> || is_same_cvr_v<T, std::string>>>
        constexpr mystring_view(T&& str) noexcept : m_view(std::forward<T>(str))
        {
        }

        template<std::size_t N>
        // last \0 does not count as length
        constexpr mystring_view(const char (&ar)[N]) noexcept : m_view(ar, N - 1)
        {
            static_assert(N >= 1);
        }

        constexpr operator const std::string_view&() const noexcept
        {
            return m_view;
        }

        [[nodiscard]] constexpr const std::string_view& get() const noexcept
        {
            return m_view;
        }

      private:
        const std::string_view m_view;
    };
} // namespace VG
#endif
