#ifndef VG_TEST_LIB_H
#define VG_TEST_LIB_H

#include <array>
#include <iostream>
#include <list>
#include <locale>
#include <string>
#include <vector>

#include "misc.h"
#include "ogg_generator.h"
#include "test_cpp_main.h"
#include "test_lib_c.h"
#include "test_lib_fs.h"
#include "test_lib_impl.h"

namespace VG::test_lib
{
    std::locale make_locale_comma_separator_for_numpunct();

    class scoped_global_locale : non_copy_moveable
    {
      public:
        explicit scoped_global_locale(const std::locale& loc);
        ~scoped_global_locale();

      private:
        std::locale old;
    };

    template<typename A, typename B, typename FCT>
    void iterate_both(A&& a, B&& b, FCT fct)
    {
        auto a_ite = a.begin();
        const auto a_end = a.end();
        auto b_ite = b.begin();
        const auto b_end = b.end();

        for (; a_ite != a_end && b_ite != b_end; ++a_ite, ++b_ite)
        {
            fct(*a_ite, *b_ite);
        }
        if (!(a_ite == a_end && b_ite == b_end))
        {
            throw std::runtime_error("Containers have not the same size");
        }
    }

    template<typename V, typename... T>
    constexpr auto my_make_array(T&&... t)
    {
        return std::array<V, sizeof...(T)>{std::forward<T>(t)...};
    }

    template<typename T, typename = void>
    struct is_iterable : std::false_type
    {
    };

    template<typename T>
    struct is_iterable<T, std::void_t<decltype(std::declval<T>().begin()), decltype(std::declval<T>().end())>> : std::true_type
    {
    };
    template<class T>
    constexpr bool is_iterable_v = is_iterable<T>::value;

    template<typename T, typename = void>
    struct is_c_strable : std::false_type
    {
    };

    template<typename T>
    struct is_c_strable<T, std::void_t<decltype(std::declval<T>().c_str())>> : std::true_type
    {
    };
    template<class T>
    constexpr bool is_c_strable_v = is_c_strable<T>::value;

    class temporary_env_variable : non_copy_moveable
    {
      public:
        template<typename A>
        explicit temporary_env_variable(A&& envir_name, const mystring_view& envir_value)
            : m_name(std::forward<A>(envir_name)), m_restored(false)
        {
            const char* const envptr = std::getenv(m_name.c_str());
            if (envptr)
            {
                m_value = envptr;
            }
            C::setenv::call(m_name.c_str(), envir_value.get().data(), true);
        }

        ~temporary_env_variable();

        void restore();

      private:
        const std::string m_name;
        std::optional<std::string> m_value;
        bool m_restored;
    };

    class exec_arguments : non_copy_moveable
    {
      public:
        template<typename PTAF, typename... ARGS>
        explicit exec_arguments(PTAF&& file, ARGS&&... args) : m_file(std::forward<PTAF>(file))
        {
            fill_args(m_file.file_node().name(), std::forward<ARGS>(args)...);
            m_args.emplace_back(nullptr);
        }

        // make sure C is a container of string
        template<typename PTAF, typename C,
                 typename = std::enable_if_t<is_iterable_v<C> && is_c_strable_v<decltype(*std::declval<C>().begin())>>>
        explicit exec_arguments(PTAF&& file, C&& c) : m_file(std::forward<PTAF>(file))
        {
            fill_one(m_file.file_node().name());
            for (const auto& i : c)
            {
                fill_one(i);
            }
            m_args.emplace_back(nullptr);
        }

        template<typename S>
        void dump_all(S& s) const
        {
            s << "'" << m_file.str() << "'";
            for (const auto& i : m_args)
                s << " '" << (i ? std::string_view(i) : "(NULL)") << "'";
        }

        [[nodiscard]] const std::vector<const char*>& args() const noexcept;
        [[nodiscard]] const path_to_a_file& file() const noexcept;

      private:
        template<typename S2, typename... ARGS2>
        void fill_args(S2&& str, ARGS2&&... args2)
        {
            fill_one(std::forward<S2>(str));
            fill_args(std::forward<ARGS2>(args2)...);
        }

        constexpr void fill_args() noexcept
        {
        }

        void fill_one(const mystring_view& str);

        const path_to_a_file m_file;
        std::vector<const char*> m_args;
    };

    template<typename... ARGS>
    void call_waitpid(const ARGS&... args)
    {
        while (true)
        {
            auto ret = C::waitpid::call(args...);
            if (ret != -1)
                return;
        }
    }

    template<typename VERIFY_EXEC, typename... ARGS>
    void started_ex(const path_to_a_file& file, ARGS&&... args)
    {
        fs::temporary_directory tempdir;
        fs::temporary_file child_stdout(tempdir.path_to());
        fs::temporary_file child_stderr(tempdir.path_to());
        unique_fd child_out_fd(fs::openfile(child_stdout.path_to().str(), O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR));
        unique_fd child_err_fd(fs::openfile(child_stderr.path_to().str(), O_CREAT | O_RDWR | O_TRUNC, S_IRUSR | S_IWUSR));

        const exec_arguments prepared_args(file, std::forward<ARGS>(args)...);
        const auto arg_file_str = prepared_args.file().str();

        const auto pid = C::fork::call();
        if (pid == 0) // child
        {
            // first need to change all fds before any call to dup2 with specified fd
            *child_out_fd.get_storage_i_know_what_im_doing() = fs::change_fd_nosys(child_out_fd.get());
            *child_err_fd.get_storage_i_know_what_im_doing() = fs::change_fd_nosys(child_err_fd.get());

            C::dup2_or_exit::call(child_out_fd.get(), STDOUT_FILENO);
            C::dup2_or_exit::call(child_err_fd.get(), STDERR_FILENO);

            // close duped before exec
            C::close_or_exit::call(child_out_fd.release());
            C::close_or_exit::call(child_err_fd.release());

            // can only call async-signal-safe functions here!
            // that's why prepared_args has been prepared before the fork
            char* const* const casted_args = const_cast<char* const*>(prepared_args.args().data());
            const char* const casted_file = arg_file_str.c_str();

            C::execv_or_exit::call(casted_file, casted_args);
        }
        else // parent
        {
            int wstatus;
            call_waitpid(pid, &wstatus, 0);

            test_lib_impl::dump_file(child_stdout.path_to(), std::cout);

            try
            {
                typename VERIFY_EXEC::action_wstatus()(wstatus);
                typename VERIFY_EXEC::action_stdout()(child_stdout);
                typename VERIFY_EXEC::action_stderr()(child_stderr);
            }
            catch (const std::exception& ex)
            {
                Stringify errmsg;
                errmsg << std::string_view(ex.what()) << " while starting: ";
                prepared_args.dump_all(errmsg);

                errmsg << "\ncontent of stdout: \n";
                test_lib_impl::dump_file(child_stdout.path_to(), errmsg);

                errmsg << "\ncontent of stderr: \n";
                test_lib_impl::dump_file(child_stderr.path_to(), errmsg);

                throw std::runtime_error(errmsg);
            }
        }
    }

    struct verify_exec_ok
    {
        using action_wstatus = test_lib_impl::verify_wstatus<true>;
        using action_stdout = test_lib_impl::verify_file_ignore;
        using action_stderr = test_lib_impl::verify_file_is_empty<true>;
    };

    struct verify_exec_ok_stdout
    {
        using action_wstatus = test_lib_impl::verify_wstatus<true>;
        using action_stdout = test_lib_impl::verify_file_is_empty<false>;
        using action_stderr = test_lib_impl::verify_file_is_empty<true>;
    };

    struct verify_exec_ok_stdout_no_progress
    {
        using action_wstatus = test_lib_impl::verify_wstatus<true>;
        using action_stdout = test_lib_impl::verify_file_no_progress;
        using action_stderr = test_lib_impl::verify_file_is_empty<true>;
    };

    template<bool has_found>
    struct verify_exec_ok_stdout_has_found_replaygain
    {
        using action_wstatus = test_lib_impl::verify_wstatus<true>;
        using action_stdout = test_lib_impl::verify_file_has_found_replaygain<has_found>;
        using action_stderr = test_lib_impl::verify_file_is_empty<true>;
    };

    struct verify_exec_ok_no_stdout
    {
        using action_wstatus = test_lib_impl::verify_wstatus<true>;
        using action_stdout = test_lib_impl::verify_file_is_empty<true>;
        using action_stderr = test_lib_impl::verify_file_is_empty<true>;
    };

    struct verify_exec_fail
    {
        using action_wstatus = test_lib_impl::verify_wstatus<false>;
        using action_stdout = test_lib_impl::verify_file_ignore;
        using action_stderr = test_lib_impl::verify_file_ignore;
    };

    struct verify_exec_fail_err
    {
        using action_wstatus = test_lib_impl::verify_wstatus<false>;
        using action_stdout = test_lib_impl::verify_file_ignore;
        using action_stderr = test_lib_impl::verify_file_is_empty<false>;
    };

    struct descs_togenerate_processed
    {
        const ogg_generator::description& togenerate;
        const ogg_generator::description& processed;
        const std::optional<std::string> forced_filename;

        descs_togenerate_processed(const ogg_generator::description& togen_, const ogg_generator::description& processed_);

        template<typename STR>
        descs_togenerate_processed(const ogg_generator::description& togen_, const ogg_generator::description& processed_,
                                   STR&& str)
            : togenerate(togen_), processed(processed_), forced_filename(std::in_place, std::forward<STR>(str))
        {
        }
    };

    // L is container of descs_togenerate_processed
    template<typename VERIFY_EXEC, bool USETEMPDIRASINPUT = false, typename VERIFY_FCT, typename L,
             typename = std::enable_if_t<is_iterable_v<L>>, typename... SWITCHES>
    void generate_testfiles_run_vg(VERIFY_FCT verif_fct, const L& inputs_descs, const SWITCHES&... switches)
    {
        fs::temporary_directory tempdir;
        // generate ogg files
        std::list<fs::temporary_file> outputs;
        for (const auto& i : inputs_descs)
        {
            if (i.forced_filename.has_value())
            {
                outputs.emplace_back(path_to_a_file(tempdir.path_to(), i.forced_filename.value()));
            }
            else
            {
                outputs.emplace_back(tempdir.path_to());
            }
            const auto& outputfile = outputs.back();

            ogg_generator::generate(i.togenerate, outputfile.path_to(), tempdir);
        }

        std::vector<std::string> args;
        test_lib_impl::add_vect_str(args, switches...);
        if constexpr (USETEMPDIRASINPUT)
        {
            args.emplace_back(tempdir.path_to().str());
        }
        else
        {
            for (const auto& i : outputs)
                args.emplace_back(i.path_to().str());
        }

        // start vorbisgain
        started_ex<VERIFY_EXEC>(vorbisgain_path(), args);

        auto out_ite = outputs.begin();
        for (const auto& i : inputs_descs)
        {
            const auto& outputfile = out_ite->path_to();

            verif_fct(i.processed, outputfile, tempdir);

            ++out_ite;
        }
    }

    // this is to have a shorter default
    template<typename VERIFY_EXEC, bool USETEMPDIRASINPUT = false, typename L, typename... SWITCHES>
    void generate_testfiles_run_vg(const L& inputs_descs, const SWITCHES&... switches)
    {
        generate_testfiles_run_vg<VERIFY_EXEC, USETEMPDIRASINPUT>(ogg_generator::verify, inputs_descs, switches...);
    }

    std::vector<float> make_deterministic_random_samples(std::size_t size, unsigned int seed, double scale);
    std::vector<float> make_constant_samples(std::size_t size, float value);
} // namespace VG::test_lib

#endif
