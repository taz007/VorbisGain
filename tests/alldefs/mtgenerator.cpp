#include <cstdint>
#include <fstream>
#include <iostream>
#include <random>
#include <sstream>
#include <string>

void fromFileOne(std::ifstream& in, unsigned int& a)
{
    if (!(in >> a))
        throw std::runtime_error("Unable to read/convert input.");
}

void fromFile(std::ifstream& in, unsigned int& a, unsigned int& b, unsigned int& c)
{
    fromFileOne(in, a);
    fromFileOne(in, b);
    fromFileOne(in, c);
}

void serializeUint8(std::ofstream& outputFile, const std::uint8_t& val)
{
    outputFile.put(static_cast<char>(val));
}

void serializeUint16(std::ofstream& outputFile, const std::uint16_t& val)
{
    // the values for the generated test files are expected to be serialized as 16bits little endian
    serializeUint8(outputFile, static_cast<std::uint8_t>(val));
    serializeUint8(outputFile, static_cast<std::uint8_t>(val >> 8));
}

void serializeInt16(std::ofstream& outputFile, const std::int16_t& val)
{
    serializeUint16(outputFile, static_cast<std::uint16_t>(val));
}

int main_ex(int argc, char** argv)
{
    unsigned int seed;
    unsigned int size;
    unsigned int volume;

    if (argc == 3)
    {
        std::ifstream infile(argv[1]);
        if (!infile)
            throw std::runtime_error(std::string("Cannot open file ") + argv[1] + " for reading.");
        fromFile(infile, seed, size, volume);
    }
    else
    {
        return EXIT_FAILURE;
    }

    const char* outfilename = argv[argc - 1];
    const double coef = volume / 100.;

    std::mt19937 mt(seed);

    std::ofstream outputFile(outfilename, std::ofstream::binary);
    if (!outputFile)
    {
        throw std::runtime_error(std::string("Cannot open file ") + outfilename + " for writing.");
    }

    for (unsigned int i = 0; i < size; ++i)
    {
        const auto val = mt();

        const auto val1tmp = static_cast<uint16_t>(val);
        const auto val2tmp = static_cast<uint16_t>(val >> 16);
        const auto val1 = static_cast<int16_t>(coef * (static_cast<double>(static_cast<int16_t>(val1tmp))));
        const auto val2 = static_cast<int16_t>(coef * (static_cast<double>(static_cast<int16_t>(val2tmp))));

        serializeInt16(outputFile, val1);
        serializeInt16(outputFile, val2);
    }

    outputFile.close();
    if (!outputFile)
        throw std::runtime_error(std::string("Write failed for") + outfilename);

    return EXIT_SUCCESS;
}

int main(int argc, char** argv)
{
    try
    {
        return main_ex(argc, argv);
    }
    catch (const std::exception& ex)
    {
        std::cerr << ex.what() << '\n';
        return EXIT_FAILURE;
    }
    catch (...)
    {
        std::cerr << "Unknown exception occured.\n";
        return EXIT_FAILURE;
    }
}
