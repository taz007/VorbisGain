#ifndef VG_TEST_LIB_FS_H
#define VG_TEST_LIB_FS_H

#include <string>

#include <dirent.h>
#include <sys/types.h>

#include "iofile.h"
#include "path_to.h"

namespace VG::fs
{
    [[nodiscard]] int openfile(const std::string& str, int flags, mode_t mode);
    [[nodiscard]] std::string basename(std::string path);
    [[nodiscard]] path_to_a_dir get_temp_dir();

    class Dir : non_copy_moveable
    {
      public:
        explicit Dir(const path_to_a_dir& dir);
        ~Dir();

        const struct dirent* readdir();

        void close();

      private:
        ::DIR* m_dir;
    };

    void mkdir(const path_to_a_dir& path);

    void delete_file(const path_to_a_file& file);
    void delete_directory_recurse(const path_to_a_dir& dir);
    void copy_file(const path_to_a_file& from, const path_to_a_file& to);
    void append_file(const path_to_a_file& from, ofile& to);

    bool is_same_content(const path_to_a_file& file1, const path_to_a_file& file2);

    class temporary_directory : non_copy_moveable
    {
      public:
        explicit temporary_directory();
        ~temporary_directory();

        [[nodiscard]] const path_to_a_dir& path_to() const noexcept;

        void remove();

      private:
        static path_to_a_dir make_temp_dir();

        path_to_a_dir m_dir;
        bool m_delete;
    };

    int change_fd_nosys(int fd) noexcept;

    class temporary_file : non_copy_moveable
    {
      public:
        explicit temporary_file(const path_to_a_dir& tempdir);

        template<typename PTAF>
        explicit temporary_file(PTAF&& file) : m_file(std::forward<PTAF>(file)), m_delete(false)
        {
            ofile newfile(m_file);
            m_delete = true;
        }

        ~temporary_file();

        [[nodiscard]] const path_to_a_file& path_to() const noexcept;
        void remove();

      private:
        static path_to_a_file make_temp_file_from_temp_dir(const path_to_a_dir& tempdir);

        void remove_or_ignore() noexcept;

        path_to_a_file m_file;
        bool m_delete;
    };
} // namespace VG::fs
#endif
