#include "test_lib.h"

#include <cstdlib>
#include <iostream>
#include <random>

#include "ignore_exception.h"
#include "test_lib_c.h"

namespace VG::test_lib
{
    temporary_env_variable::~temporary_env_variable()
    {
        ignore_exception([&] { restore(); });
    }

    void temporary_env_variable::restore()
    {
        if (!m_restored)
        {
            if (m_value.has_value())
            {
                VG::C::setenv::call(m_name.c_str(), m_value.value().c_str(), true);
            }
            else
            {
                VG::C::unsetenv::call(m_name.c_str());
            }
            m_restored = true;
        }
    }

    descs_togenerate_processed::descs_togenerate_processed(const ogg_generator::description& togen_,
                                                           const ogg_generator::description& processed_)
        : togenerate(togen_), processed(processed_)
    {
    }

    struct comma : std::numpunct<char>
    {
        char do_decimal_point() const override
        {
            return ',';
        }
    };

    std::locale make_locale_comma_separator_for_numpunct()
    {
        auto new_comma = std::make_unique<comma>();
        std::locale loc(std::locale::classic(), new_comma.get());
        // if it reaches here, it is safe AND obligatory to release the unique_ptr
        new_comma.release();
        return loc;
    }

    scoped_global_locale::scoped_global_locale(const std::locale& loc) : old(std::locale::global(loc))
    {
    }

    scoped_global_locale::~scoped_global_locale()
    {
        ignore_exception([&] { std::locale::global(old); });
    }

    //@param scale scale the returned values by that value
    //@return values between -32K to 32K, scaled by the scale parameter
    std::vector<float> make_deterministic_random_samples(std::size_t size, unsigned int seed, double scale)
    {
        std::vector<float> v;
        v.reserve(size);

        std::mt19937 mt(seed);
        for (std::size_t i = 0; i < size; i += 2)
        {
            const auto val = mt();
            const auto val1tmp = static_cast<uint16_t>(val);
            const auto val2tmp = static_cast<uint16_t>(val >> 16);
            const auto val1 = scale * (static_cast<double>(static_cast<int16_t>(val1tmp)));
            const auto val2 = scale * (static_cast<double>(static_cast<int16_t>(val2tmp)));

            v.push_back(static_cast<float>(val1));
            v.push_back(static_cast<float>(val2));
        }

        return v;
    }

    std::vector<float> make_constant_samples(std::size_t size, float value)
    {
        std::vector<float> v(size, value);

        return v;
    }

    const std::vector<const char*>& exec_arguments::args() const noexcept
    {
        return m_args;
    }

    const path_to_a_file& exec_arguments::file() const noexcept
    {
        return m_file;
    }

    void exec_arguments::fill_one(const mystring_view& str)
    {
        m_args.emplace_back(str.get().data());
    }

} // namespace VG::test_lib
