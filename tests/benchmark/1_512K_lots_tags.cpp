#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const std::string THIS_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK=1.19";
static const std::string THIS_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN=-11.64 dB";

static std::vector<OG::comment> gen_tags()
{
    std::vector<OG::comment> tags;
    tags.emplace_back("HMMM=WUT");
    for (int i = 0; i < 60000; ++i)
    {
        tags.emplace_back("BLABLABLABALB=DFGJDFJGDKFGLDFLGJDFG");
    }
    tags.emplace_back("WUT=HMMM");

    return tags;
}

static std::vector<OG::comment> gen_tags_andgains()
{
    std::vector<OG::comment> tags = gen_tags();

    tags.emplace_back(OG::ogg_samples::S512K_44100_2_PEAK);
    tags.emplace_back(OG::ogg_samples::S512K_44100_2_GAIN);
    tags.emplace_back(THIS_ALBUM_PEAK);
    tags.emplace_back(THIS_ALBUM_GAIN);

    return tags;
}

static const std::vector<OG::comment> tags = gen_tags();
static const std::vector<OG::comment> tags_andgains = gen_tags_andgains();

static const OG::description togen_ogg = {{{
                                              {OG::ogg_samples::S512K_44100_2, tags},
                                          }},
                                          {}};

static const OG::description processed_ogg = {{{
                                                  {OG::ogg_samples::S512K_44100_2, tags_andgains},
                                              }},
                                              {}};

TEST_CASE("1_512K_lots_tags")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;

    inputs_desc.emplace_back(togen_ogg, processed_ogg);

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, "-a");
}
