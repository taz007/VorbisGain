#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description empty_togen_ogg = {{{
                                                    {OG::ogg_samples::S0K,
                                                     {

                                                     }},
                                                }},
                                                {}};

static const OG::description& empty_processed_ogg = empty_togen_ogg;

TEST_CASE("lots_empty")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    for (int i = 0; i < 60; ++i)
    {
        inputs_desc.emplace_back(empty_togen_ogg, empty_processed_ogg);
    }

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, "-a");
}
