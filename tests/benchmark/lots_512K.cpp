#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const std::string THIS_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK=1.19";
static const std::string THIS_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN=-11.64 dB";

static const OG::description togen_ogg = {{{
                                              {OG::ogg_samples::S512K_44100_2, {}},
                                          }},
                                          {}};

static const OG::description processed_ogg = {{{
                                                  {OG::ogg_samples::S512K_44100_2,
                                                   {
                                                       OG::ogg_samples::S512K_44100_2_PEAK,
                                                       OG::ogg_samples::S512K_44100_2_GAIN,
                                                       THIS_ALBUM_PEAK,
                                                       THIS_ALBUM_GAIN,
                                                   }},
                                              }},
                                              {}};

TEST_CASE("lots_512K")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;

    for (int i = 0; i < 10; ++i)
    {
        inputs_desc.emplace_back(togen_ogg, processed_ogg);
    }

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, "-a");
}
