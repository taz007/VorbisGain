#ifndef VG_TEST_LIB_OGG_GENERATOR_H
#define VG_TEST_LIB_OGG_GENERATOR_H

#include <optional>
#include <string>
#include <vector>

#include "meta_ts.h"
#include "stringify.h"
#include "test_lib_fs.h"

namespace VG
{
    namespace test_lib
    {
        namespace ogg_generator
        {
            using comment = std::string;

            struct ogg_sample_file
            {
                std::string_view filename;
            };

            namespace ogg_samples
            {
                static constexpr ogg_sample_file S512K_44100_2{"_512K_44100_2.ogg"};
                static constexpr ogg_sample_file S512K_44100_1{"_512K_44100_1.ogg"};
                static constexpr ogg_sample_file S512K_middle_44100_2{"_512K_middle_44100_2.ogg"};
                static constexpr ogg_sample_file S512K_zero_44100_2{"_512K_zero_44100_2.ogg"};
                static constexpr ogg_sample_file S0K{"empty_44100_1.ogg"};
                static constexpr ogg_sample_file S16{"_16_44100_1.ogg"};

                // type needs to be std::string to be userfriendly with aggregate init used in test files.
                // static to make sure it's in each TU thus have internal linkage, and avoid static init order fiasco issues
                static const std::string S512K_44100_2_PEAK{"REPLAYGAIN_TRACK_PEAK=1.19"};
                static const std::string S512K_44100_2_GAIN{"REPLAYGAIN_TRACK_GAIN=-11.64 dB"};
                static const std::string S512K_44100_1_PEAK{"REPLAYGAIN_TRACK_PEAK=1.23"};
                static const std::string S512K_44100_1_GAIN{"REPLAYGAIN_TRACK_GAIN=-11.77 dB"};
                static const std::string S512K_zero_44100_2_PEAK{"REPLAYGAIN_TRACK_PEAK=0.00"};
                static const std::string S512K_zero_44100_2_GAIN{"REPLAYGAIN_TRACK_GAIN=+64.82 dB"};
                static const std::string S512K_middle_44100_2_PEAK{"REPLAYGAIN_TRACK_PEAK=0.70"};
                static const std::string S512K_middle_44100_2_GAIN{"REPLAYGAIN_TRACK_GAIN=-5.56 dB"};
                static const std::string S16_PEAK{"REPLAYGAIN_TRACK_PEAK=0.87"};
            } // namespace ogg_samples

            struct stream
            {
                const ogg_sample_file& input;
                std::vector<comment> comments;
            };

            using multiplexed_streams = std::vector<stream>;
            using chained_multiplexed_streams = std::vector<multiplexed_streams>;

            struct description
            {
                chained_multiplexed_streams cm_streams;
                std::optional<meta_filetimestamps> file_timestamps;
            };

            void dump(Stringify& s, const description& desc);

            void generate(const description& in, const path_to_a_file& outputogg, fs::temporary_directory& tempdir);

            inline constexpr float OGG_PEAK_PRECISION = 0.1F;
            inline constexpr float OGG_GAIN_PRECISION = 1.F;

            void verify(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir);
            void verify_strict(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir);
            void verify_close_enough_ts(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir);
        } // namespace ogg_generator

        bool compare_comment(const std::string& c1, const std::string& c2);
        bool fuzzy_match(const std::string& c1, const std::string& c2, float margin);

    } // namespace test_lib
} // namespace VG

#endif
