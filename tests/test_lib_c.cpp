#include "test_lib_c.h"

#include "stringify.h"

namespace VG::C
{
    std::string forkFctor::getErrorMsg()
    {
        return "Call to fork() failed";
    }

    std::string waitpidFctor::getErrorMsg(pid_t pid, int* /* wstatus */, int options)
    {
        return Stringify() << "Call to waitpid() failed for pid='" << pid << "' options='" << options << "'";
    }

    std::string rmdirFctor::getErrorMsg(const char* pathname)
    {
        return Stringify() << "Call to rmdir() failed for path='" << std::string_view(pathname) << "'";
    }

    std::string mkdtempFctor::getErrorMsg(const char* tmplate)
    {
        return Stringify() << "Call to mkdtemp() failed for template='" << std::string_view(tmplate) << "'";
    }

    std::string setenvFctor::getErrorMsg(const char* name, const char* value, int)
    {
        return Stringify() << "Call to setenv() failed for '" << std::string_view(name) << "'='" << std::string_view(value)
                           << "'";
    }

    std::string unsetenvFctor::getErrorMsg(const char* name)
    {
        return Stringify() << "Call to unsetenv() failed for '" << std::string_view(name) << "'";
    }

    std::string unlinkFctor::getErrorMsg(const char* filename)
    {
        return Stringify() << "Call to unlink() failed for '" << std::string_view(filename) << "'";
    }

    std::string lseekFctor::getErrorMsg(int, off_t, int)
    {
        return "Call to lseek() failed";
    }

    std::string mkdirFctor::getErrorMsg(const char* pathname, mode_t mode)
    {
        return Stringify() << "Call to mkdir() failed for '" << std::string_view(pathname) << "' with mode: " << mode;
    }
} // namespace VG::C
