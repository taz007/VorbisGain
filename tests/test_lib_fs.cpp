#include "test_lib_fs.h"

#include <array>
#include <cstring>

#include "ignore_exception.h"
#include "iofile.h"
#include "misc.h"
#include "path_to_stat.h"
#include "stringify.h"
#include "test_lib_c.h"

namespace VG::fs
{
    std::string basename(std::string path)
    {
        const auto& idx = path.find_last_of('/');
        if (idx != std::string::npos)
        {
            path.erase(0, idx + 1);
        }
        return path;
    }

    path_to_a_dir get_temp_dir()
    {
        const char* ptr = std::getenv("TMPDIR");
        if (ptr == nullptr)
            ptr = "/tmp";

        return path_to_a_dir(std::in_place, ptr);
    }

    Dir::Dir(const path_to_a_dir& dir)
    {
        m_dir = VG::C::opendir::call(dir.str().c_str());
    }

    Dir::~Dir()
    {
        ignore_exception([&] { close(); });
    }

    const dirent* Dir::readdir()
    {
        return VG::C::readdir::call(m_dir);
    }

    void Dir::close()
    {
        if (m_dir)
        {
            VG::C::closedir::call(m_dir);
            m_dir = nullptr;
        }
    }

    path_to_a_dir temporary_directory::make_temp_dir()
    {
        constexpr std::string_view tempdir_name_template = "VG_test_lib_tempdir_XXXXXX";

        path_to_a_dir path(get_temp_dir());

        std::string path_tmp = path.str();
        path_tmp.append(tempdir_name_template);

        VG::C::mkdtemp::call(&path_tmp[0]);

        const auto pos = path_tmp.length() - tempdir_name_template.length();
        const std::string_view new_tempdir_name(&path_tmp[pos], tempdir_name_template.length());

        path.add_subdir(new_tempdir_name);

        return path;
    }

    temporary_directory::temporary_directory() : m_dir(make_temp_dir()), m_delete(true)
    {
    }

    const path_to_a_dir& temporary_directory::path_to() const noexcept
    {
        return m_dir;
    }

    void temporary_directory::remove()
    {
        VG::fs::delete_directory_recurse(m_dir);
        m_delete = false;
    }

    temporary_directory::~temporary_directory()
    {
        if (m_delete)
        {
            ignore_exception([&] { remove(); });
        }
    }

    path_to_a_file temporary_file::make_temp_file_from_temp_dir(const path_to_a_dir& tempdir)
    {
        constexpr std::string_view tempfile_name_template = "VG_test_lib_tempfile_XXXXXX";

        std::string path_dir = tempdir.str();
        path_dir.append(tempfile_name_template);

        unique_fd fd(VG::C::mkstemp::call(&path_dir[0]));

        const auto pos = path_dir.length() - tempfile_name_template.length();
        const std::string_view new_tempfile_name(&path_dir[pos], tempfile_name_template.length());

        return path_to_a_file(tempdir, new_tempfile_name);
        ;
    }

    temporary_file::temporary_file(const path_to_a_dir& tempdir) : m_file(make_temp_file_from_temp_dir(tempdir)), m_delete(true)
    {
    }

    int change_fd_nosys(int fd) noexcept
    {
        if (fd > STDERR_FILENO)
            return fd;
        while (true)
        {
            const auto newfd = VG::C::fcntl_or_exit::call(fd, F_DUPFD, STDERR_FILENO + 1);
            if (newfd == -1 && errno == EINTR)
                continue;

            VG::C::close_or_exit::call(fd);
            return newfd;
        }
    }

    const path_to_a_file& temporary_file::path_to() const noexcept
    {
        return m_file;
    }

    void temporary_file::remove()
    {
        if (m_delete)
        {
            VG::fs::delete_file(m_file);
            m_delete = false;
        }
    }

    void temporary_file::remove_or_ignore() noexcept
    {
        if (m_delete)
        {
            ignore_exception([&] { VG::fs::delete_file(m_file); });
            m_delete = false;
        }
    }

    temporary_file::~temporary_file()
    {
        remove_or_ignore();
    }

    void delete_file(const path_to_a_file& file)
    {
        VG::C::unlink::call(file.str().c_str());
    }

    void delete_directory_recurse(const path_to_a_dir& dir)
    {
        Dir d(dir);
        for (auto e = d.readdir(); e != nullptr; e = d.readdir())
        {
            if (std::strcmp(e->d_name, ".") == 0 || std::strcmp(e->d_name, "..") == 0)
                continue;

            const auto entryname = path_to_something(dir, e->d_name);
            const path_to_stat entry_stat(entryname);

            if (!entry_stat.is_valid())
                throw VG::Exception::Generic(Stringify() << "'" << entryname.something().name() << "' is not a valid entry in '"
                                                         << entryname.dir().str() << "'");
            if (entry_stat.is_a_file())
            {
                delete_file(path_to_a_file(dir, entryname.something().name()));
            }
            else if (entry_stat.is_a_dir())
            {
                delete_directory_recurse(path_to_a_dir(dir).add_subdir(entryname.something().name()));
            }
        }
        // call delete as content of dirname is now empty
        VG::C::rmdir::call(dir.str().c_str());
    }

    void copy_file(const path_to_a_file& from, const path_to_a_file& to)
    {
        // exception thrown if src is empty file, so special case here
        ifile src(from);
        ofile dst(to);

        dst.copyfile(src);

        dst.close();
        src.close();
    }

    void append_file(const path_to_a_file& from, ofile& to)
    {
        ifile src(from);
        to.copyfile(src);

        src.close();
    }

    void mkdir(const path_to_a_dir& path)
    {
        constexpr auto mode = static_cast<mode_t>(S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
        VG::C::mkdir::call(path.str().c_str(), mode);
    }

    int openfile(const std::string& str, int flags, mode_t mode)
    {
        while (true)
        {
            const char* const charptr = str.c_str();
            const int fd = VG::C::open::call(charptr, flags, mode);
            if (fd == -1 && errno == EINTR)
                continue;

            return fd;
        }
    }

    bool is_same_content(const path_to_a_file& file1, const path_to_a_file& file2)
    {
        constexpr std::size_t BUFSIZE = 65536;

        std::array<std::byte, BUFSIZE> buffer1;
        std::array<std::byte, BUFSIZE> buffer2;

        ifile ifbuf1(file1);
        ifile ifbuf2(file2);

        while (true)
        {
            const std::size_t readchars1 = ifbuf1.read(buffer1.data(), buffer1.size());
            const std::size_t readchars2 = ifbuf2.read(buffer2.data(), buffer2.size());

            if (readchars1 != readchars2)
                return false;

            if (readchars1 == 0)
                break;

            if (std::memcmp(&buffer1[0], &buffer2[0], readchars1) != 0)
                return false;
        }
        return true;
    }

} // namespace VG::fs
