#include <catch2/catch_test_macros.hpp>

#include "misc.h"

TEST_CASE("Uppercase")
{
    std::string foo("foo");

    // from lowercase to uppercase
    VG::UPPERCASE(foo);
    REQUIRE(foo == "FOO");

    // from uppercase to uppercase
    VG::UPPERCASE(foo);
    REQUIRE(foo == "FOO");
}
