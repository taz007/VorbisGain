#include <array>
#include <catch2/catch_test_macros.hpp>
#include <cstring>

#include "iofile.h"
#include "path_to_stat.h"
#include "stringify.h"
#include "test_lib_fs.h"

TEST_CASE("ifile")
{
    // build test data
    VG::fs::temporary_directory tempdir;

    const auto empty_name = VG::path_to_a_file(tempdir.path_to(), "empty");
    {
        VG::ofile f(empty_name);
    }

    const auto five_empty_lines_name = VG::path_to_a_file(tempdir.path_to(), "5empty_lines");
    {
        VG::ofile f(five_empty_lines_name);
        f << '\n' << '\n' << '\n' << '\n' << '\n';
    }

    const auto empty_foobar_empty_name = VG::path_to_a_file(tempdir.path_to(), "empty_foobar_empty");
    {
        VG::ofile f(empty_foobar_empty_name);
        f << '\n' << "foobar\n" << '\n';
    }

    const auto two_foobar_name = VG::path_to_a_file(tempdir.path_to(), "two_foobar");
    {
        VG::ofile f(two_foobar_name);
        f << "foobar\n"
          << "foobar\n";
    }

    const auto two_foobar_2 = VG::path_to_a_file(tempdir.path_to(), "two_foobar_2");
    {
        VG::ofile f(two_foobar_2);
        f << "foobar\n"
          << "foobar";
    }

    // real test
    // 1st file
    {
        VG::ifile i(empty_name);
        const auto line = i.getline();
        REQUIRE(line.has_value() == false);

        VG::ifile i2(empty_name);
        std::array<std::byte, 128> buffer;
        const auto read = i2.read(buffer.data(), buffer.size());
        REQUIRE(read == 0);
        REQUIRE(i2.read(buffer.data(), buffer.size()) == 0);
    }

    // 2nd file
    {
        VG::ifile in(five_empty_lines_name);
        for (int i = 0; i < 5; ++i)
        {
            const auto line = in.getline();
            REQUIRE(line.has_value());
            REQUIRE(line.value() == "");
        }
        const auto line = in.getline();
        REQUIRE(line.has_value() == false);

        VG::ifile i2(five_empty_lines_name);
        std::array<std::byte, 128> buffer;
        const auto read = i2.read(buffer.data(), buffer.size());
        REQUIRE(read == 5);
        for (unsigned int i = 0; i < 5; ++i)
        {
            REQUIRE(static_cast<char>(buffer[i]) == '\n');
        }
        REQUIRE(i2.read(buffer.data(), buffer.size()) == 0);
    }

    // 3rd file
    {
        VG::ifile in(empty_foobar_empty_name);
        {
            const auto line = in.getline();
            REQUIRE(line.has_value());
            REQUIRE(line.value() == "");
        }
        {
            const auto line = in.getline();
            REQUIRE(line.has_value());
            REQUIRE(line.value() == "foobar");
        }
        {
            const auto line = in.getline();
            REQUIRE(line.has_value());
            REQUIRE(line.value() == "");
        }
        const auto line = in.getline();
        REQUIRE(line.has_value() == false);

        VG::ifile i2(empty_foobar_empty_name);
        std::array<std::byte, 128> buffer;
        const auto read = i2.read(buffer.data(), buffer.size());
        REQUIRE(read == 9);
        REQUIRE(std::memcmp(&buffer[0], "\nfoobar\n\n", read) == 0);
        REQUIRE(i2.read(buffer.data(), buffer.size()) == 0);
    }

    // 4th file
    {
        VG::ifile in(two_foobar_name);
        for (int i = 0; i < 2; ++i)
        {
            const auto line = in.getline();
            REQUIRE(line.has_value());
            REQUIRE(line.value() == "foobar");
        }
        const auto line = in.getline();
        REQUIRE(line.has_value() == false);

        VG::ifile i2(two_foobar_name);
        std::array<std::byte, 128> buffer;
        const auto read = i2.read(buffer.data(), buffer.size());
        REQUIRE(read == 14);
        REQUIRE(std::memcmp(&buffer[0], "foobar\nfoobar\n", read) == 0);
        REQUIRE(i2.read(buffer.data(), buffer.size()) == 0);
    }

    // 5th file
    {
        VG::ifile in(two_foobar_2);
        for (int i = 0; i < 2; ++i)
        {
            const auto line = in.getline();
            REQUIRE(line.has_value());
            REQUIRE(line.value() == "foobar");
        }
        const auto line = in.getline();
        REQUIRE(line.has_value() == false);

        VG::ifile i2(two_foobar_2);
        std::array<std::byte, 128> buffer;
        const auto read = i2.read(buffer.data(), buffer.size());
        REQUIRE(read == 13);
        REQUIRE(std::memcmp(&buffer[0], "foobar\nfoobar", read) == 0);
        REQUIRE(i2.read(buffer.data(), buffer.size()) == 0);
    }
}

TEST_CASE("ofile")
{
    VG::fs::temporary_directory tempdir;

    // file created empty
    const auto out1path = VG::path_to_a_file(tempdir.path_to(), "out1");
    {
        VG::ofile out1(out1path);
    }
    const VG::path_to_stat stat1(out1path);
    REQUIRE(stat1.is_valid());
    REQUIRE(stat1.is_a_file());
    REQUIRE(stat1.file_size() == 0);

    // file created empty even with close
    const auto out2path = VG::path_to_a_file(tempdir.path_to(), "out2");
    {
        VG::ofile out2(out2path);
        out2.close();
    }
    const VG::path_to_stat stat2(out2path);
    REQUIRE(stat2.is_valid());
    REQUIRE(stat2.is_a_file());
    REQUIRE(stat2.file_size() == 0);

    // non existing directory, can't work
    const auto non_exist = VG::path_to_a_file(VG::path_to_a_dir(tempdir.path_to()).add_subdir("nonexistingdirectory"), "newfile");
    REQUIRE_THROWS(VG::ofile(non_exist));

    // write small content to a file
    const auto out3path = VG::path_to_a_file(tempdir.path_to(), "out3");
    const std::string_view content = "smallcontent";
    {
        VG::ofile out3(out3path);
        out3 << content;
    }
    const VG::path_to_stat stat3(out3path);
    REQUIRE(stat3.is_valid());
    REQUIRE(stat3.is_a_file());
    REQUIRE(stat3.file_size() == content.size());
    {
        VG::ifile in3(out3path);
        auto line = in3.getline();
        REQUIRE(line.has_value());
        REQUIRE(line.value() == content);
    }

    // write small content to a file, manual close
    const auto out4path = VG::path_to_a_file(tempdir.path_to(), "out4");
    {
        VG::ofile out4(out4path);
        out4 << content;
        out4.close();
    }
    const VG::path_to_stat stat4(out4path);
    REQUIRE(stat4.is_valid());
    REQUIRE(stat4.is_a_file());
    REQUIRE(stat4.file_size() == content.size());
    {
        VG::ifile in4(out4path);
        auto line = in4.getline();
        REQUIRE(line.has_value());
        REQUIRE(line.value() == content);
    }

    // write small conent using direct write()
    const auto out5path = VG::path_to_a_file(tempdir.path_to(), "out5");
    {
        VG::ofile out5(out5path);
        out5.write(reinterpret_cast<const std::byte*>(content.data()), content.size());
    }
    const VG::path_to_stat stat5(out5path);
    REQUIRE(stat5.is_valid());
    REQUIRE(stat5.is_a_file());
    REQUIRE(stat5.file_size() == content.size());
    {
        VG::ifile in5(out5path);
        auto line = in5.getline();
        REQUIRE(line.has_value());
        REQUIRE(line.value() == content);
    }

    // write a single char
    const auto out6path = VG::path_to_a_file(tempdir.path_to(), "out6");
    {
        VG::ofile out6(out6path);
        out6 << 'a';
    }
    const VG::path_to_stat stat6(out6path);
    REQUIRE(stat6.is_valid());
    REQUIRE(stat6.is_a_file());
    REQUIRE(stat6.file_size() == 1);
    {
        VG::ifile in6(out6path);
        auto line = in6.getline();
        REQUIRE(line.has_value());
        REQUIRE(line.value() == "a");
    }

    // copy content of an existing file
    const auto out7path = VG::path_to_a_file(tempdir.path_to(), "out7");
    {
        VG::ofile out7(out7path);
        VG::ifile in5(out5path);
        out7.copyfile(in5);
    }
    const VG::path_to_stat stat7(out7path);
    REQUIRE(stat7.is_valid());
    REQUIRE(stat7.is_a_file());
    REQUIRE(stat7.file_size() == stat5.file_size());
    {
        VG::ifile in7(out7path);
        auto line = in7.getline();
        REQUIRE(line.has_value());
        REQUIRE(line.value() == content);
    }

    // mix of write operations
    // content + '\0' + content from otherfile +'\n' + "foobar"
    const std::string_view foobar = "foobar";
    const auto out8path = VG::path_to_a_file(tempdir.path_to(), "out8");
    {
        VG::ofile out8(out8path);
        VG::ifile in5(out5path);

        out8 << content;
        out8 << '\0';
        out8.copyfile(in5);
        out8 << '\n';
        out8 << foobar;
    }
    const VG::path_to_stat stat8(out8path);
    REQUIRE(stat8.is_valid());
    REQUIRE(stat8.is_a_file());
    REQUIRE(stat8.file_size() == (content.size() + 1 + stat5.file_size() + 1 + foobar.size()));
    {
        const std::string firstline = VG::Stringify() << content << '\0' << content;
        const auto& secondline = foobar;

        VG::ifile in8(out8path);
        auto line1 = in8.getline();
        REQUIRE(line1.has_value());
        REQUIRE(line1.value() == firstline);

        auto line2 = in8.getline();
        REQUIRE(line2.has_value());
        REQUIRE(line2.value() == secondline);

        auto line3 = in8.getline();
        REQUIRE(!line3.has_value());
    }
}
