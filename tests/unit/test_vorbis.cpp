#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"
#include "vorbis.h"

TEST_CASE("readfloat_if_possible")
{
    {
        const std::optional<float> f = VG::readfloat_if_possible("1.23");
        REQUIRE(f.has_value());
        REQUIRE(f.value() == 1.23f);
    }
    {
        const std::optional<float> f = VG::readfloat_if_possible("-1.23");
        REQUIRE(f.has_value());
        REQUIRE(f.value() == -1.23f);
    }
    {
        const std::optional<float> f = VG::readfloat_if_possible("-1.23f");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloat_if_possible("-1.23 f");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloat_if_possible("-1.23 ");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloat_if_possible("");
        REQUIRE(!f.has_value());
    }
}

TEST_CASE("readfloatDB_if_possible")
{
    {
        const std::optional<float> f = VG::readfloatDB_if_possible("-1.23 dB");
        REQUIRE(f.has_value());
        REQUIRE(f.value() == -1.23f);
    }
    {
        const std::optional<float> f = VG::readfloatDB_if_possible("-1.23 dB ");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloatDB_if_possible("-1.23 eF");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloatDB_if_possible("-1.23");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloatDB_if_possible("");
        REQUIRE(!f.has_value());
    }
}

// must not be locale dependant
TEST_CASE("readfloat_if_possible_locale")
{
    const auto comma_locale = VG::test_lib::make_locale_comma_separator_for_numpunct();
    VG::test_lib::scoped_global_locale newlocale(comma_locale);
    {
        const std::optional<float> f = VG::readfloat_if_possible("1,23");
        REQUIRE(!f.has_value());
    }
    {
        const std::optional<float> f = VG::readfloatDB_if_possible("1,23 dB");
        REQUIRE(!f.has_value());
    }
}
