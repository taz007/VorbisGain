#include <catch2/catch_test_macros.hpp>

#include "mystring_view.h"

TEST_CASE("mystring_view")
{
    {
        VG::mystring_view a("foo");
        REQUIRE(a.get().length() == 3);
        REQUIRE(a.get() == "foo");
    }
    {
        VG::mystring_view a("");
        REQUIRE(a.get().length() == 0);
        REQUIRE(a.get() == "");
    }
}
