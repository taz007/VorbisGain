#include <catch2/catch_test_macros.hpp>
#include <cmath>
// todo remove this, use catch_matchers_floating_point
#include <catch2/catch_approx.hpp>

#include "gain_analysis.h"
#include "test_lib.h"

TEST_CASE("ContextOneSong sampleWindow")
{
    REQUIRE(VG::ContextOneSong(false, 8000).filterinfo.samples_window == 400);
    REQUIRE(VG::ContextOneSong(false, 11025).filterinfo.samples_window == 552);
    REQUIRE(VG::ContextOneSong(false, 12000).filterinfo.samples_window == 600);
    REQUIRE(VG::ContextOneSong(false, 16000).filterinfo.samples_window == 800);
    REQUIRE(VG::ContextOneSong(false, 22050).filterinfo.samples_window == 1103);
    REQUIRE(VG::ContextOneSong(false, 24000).filterinfo.samples_window == 1200);
    REQUIRE(VG::ContextOneSong(false, 32000).filterinfo.samples_window == 1600);
    REQUIRE(VG::ContextOneSong(false, 44100).filterinfo.samples_window == 2205);
    REQUIRE(VG::ContextOneSong(false, 48000).filterinfo.samples_window == 2400);
}

static constexpr float RAW_PEAK_PRECISION = 0.0001F;
static constexpr float RAW_GAIN_PRECISION = 0.02F;

TEST_CASE("ContextOneSong peak mono")
{
    VG::ContextOneSong c8K(false, 8000);
    REQUIRE(c8K.getPeak().has_value() == false);

    const std::array zerosL{0.f, 0.f, 0.f, 0.f};
    const std::array zeros{zerosL.data()};
    c8K.AnalyzeSamples(zeros.data(), zerosL.size());
    REQUIRE(c8K.getPeak().has_value());
    REQUIRE(c8K.getPeak() == 0.f);

    const std::array arr1{56.f, 7805.f, 27650.f, 5613.f};
    const std::array arr1s{arr1.data()};
    c8K.AnalyzeSamples(arr1s.data(), arr1.size());
    REQUIRE(c8K.getPeak().has_value());
    REQUIRE(c8K.getPeak() == 27650.f);

    const std::array arr2{156.f, -7805.f, -30027.f, 0.f};
    const std::array arr2s{arr2.data()};
    c8K.AnalyzeSamples(arr2s.data(), arr2.size());
    REQUIRE(c8K.getPeak().has_value());
    REQUIRE(c8K.getPeak() == 30027.f);
}

TEST_CASE("ContextOneSong peak stereo")
{
    VG::ContextOneSong c8K(true, 8000);
    REQUIRE(c8K.getPeak().has_value() == false);

    const std::array zerosL{0.f, 0.f, 0.f, 0.f};
    const std::array zerosR{0.f, 0.f, 0.f, 0.f};
    const std::array zeros{zerosL.data(), zerosR.data()};
    c8K.AnalyzeSamples(zeros.data(), zerosL.size());
    REQUIRE(c8K.getPeak().has_value());
    REQUIRE(c8K.getPeak() == 0.f);

    const std::array arr1L{56.f, 7805.f, 27650.f, 5613.f};
    const std::array arr1R{55.f, 7806.f, 27649.f, 5612.f};
    const std::array arr1s{arr1L.data(), arr1R.data()};
    c8K.AnalyzeSamples(arr1s.data(), arr1L.size());
    REQUIRE(c8K.getPeak().has_value());
    REQUIRE(c8K.getPeak() == 27650.f);

    const std::array arr2L{156.f, -7805.f, -30026.f, 0.f};
    const std::array arr2R{156.f, -7805.f, -30027.f, 0.f};
    const std::array arr2s{arr2L.data(), arr2R.data()};
    c8K.AnalyzeSamples(arr2s.data(), arr2L.size());
    REQUIRE(c8K.getPeak().has_value());
    REQUIRE(c8K.getPeak() == 30027.f);
}

TEST_CASE("ContextOneSong gain mono low samples")
{
    VG::ContextOneSong c8K(false, 8000);
    REQUIRE(c8K.GetTitleGain().has_value() == false);

    const std::array zerosL{0.f, 1.f, 2.f, 3.f};
    const std::array zeros{zerosL.data()};
    c8K.AnalyzeSamples(zeros.data(), zerosL.size());
    REQUIRE(c8K.GetTitleGain().has_value() == false);
}

Catch::Approx make_approx(float val, float precision)
{
    Catch::Approx a(val);
    a.epsilon(0).margin(precision);

    return a;
}

Catch::Approx make_approx_gain(float val)
{
    return make_approx(val, RAW_GAIN_PRECISION);
}

Catch::Approx make_approx_peak(float val)
{
    return make_approx(val, RAW_PEAK_PRECISION);
}

TEST_CASE("ContextOneSong gain mono allfreq zero samples")
{
    for (const auto freq : {8000U, 11025U, 12000U, 16000U, 22050U, 24000U, 32000U, 44100U, 48000U})
    {
        VG::ContextOneSong c1s(false, freq);

        const auto zerosL = VG::test_lib::make_constant_samples(c1s.filterinfo.samples_window * 2, 0.f);
        const std::array zeros{zerosL.data()};
        c1s.AnalyzeSamples(zeros.data(), zerosL.size());

        const auto gain = c1s.GetTitleGain();
        const auto peak = c1s.getPeak();

        REQUIRE(gain.has_value());
        REQUIRE(gain.value() == make_approx_gain(64.82f));
        REQUIRE(peak.has_value());
        REQUIRE(peak.value() == 0.f);
    }
}

TEST_CASE("ContextOneSong gain mono allfreq random samples")
{
    struct freq_gain_peak
    {
        unsigned int freq;
        float gain;
        float peak;
    };

    // values also verified with flac --replay-gain
    const auto fgps = VG::test_lib::my_make_array<freq_gain_peak>(
        freq_gain_peak{8000, -17.26f, 32768.f}, freq_gain_peak{11025, -17.47f, 32768.f}, freq_gain_peak{12000, -17.24f, 32768.f},
        freq_gain_peak{16000, -16.12f, 32768.f}, freq_gain_peak{22050, -14.89f, 32768.f}, freq_gain_peak{24000, -14.59f, 32768.f},
        freq_gain_peak{32000, -13.33f, 32768.f}, freq_gain_peak{44100, -11.94f, 32768.f},
        freq_gain_peak{48000, -11.57f, 32768.f});

    // this is the reference data, matching the above values
    const auto samples_1_L = std::make_tuple(VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 1.f), 1.f);
    // same data but scaled with various coeficients
    const auto samples_01_L = std::make_tuple(VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 0.1f), 0.1f);
    const auto samples_05_L = std::make_tuple(VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 0.5f), 0.5f);
    const auto samples_2_L = std::make_tuple(VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 2.f), 2.f);

    for (const auto& samples_ptr : {&samples_1_L, &samples_01_L, &samples_05_L, &samples_2_L})
    {
        const auto& vect = std::get<0>(*samples_ptr);
        const auto& coef = std::get<1>(*samples_ptr);
        const std::array allsamples{vect.data()};

        for (const auto& fgp : fgps)
        {
            VG::ContextOneSong c1s(false, fgp.freq);
            c1s.AnalyzeSamples(allsamples.data(), vect.size());

            const auto gain = c1s.GetTitleGain();
            const auto peak = c1s.getPeak();

            INFO("Current freq: " << fgp.freq << " coef: " << coef);

            // in case the coef is not 1, we need to compute/adjust the expected gain (based on the reference value)
            const auto audjusted_fgp_gain = fgp.gain + std::log10(1.f / coef) * 20;

            REQUIRE(gain.has_value());
            REQUIRE(gain.value() == make_approx_gain(audjusted_fgp_gain));
            REQUIRE(peak.has_value());
            REQUIRE(peak.value() == make_approx_peak(fgp.peak * coef));
        }
    }
}

TEST_CASE("ContextOneSong gain stereo 0_random samples")
{
    struct freq_gain_peak
    {
        unsigned int freq;
        float gain;
        float peak;
    };

    const auto fgp = freq_gain_peak{8000, -17.26f, 32768.f};
    const auto samples_1 = VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 1.f);
    const auto samples_0 = VG::test_lib::make_constant_samples(512 * 1024, 0.f);

    const std::array allsamples_00{samples_0.data(), samples_0.data()};
    const std::array allsamples_01{samples_0.data(), samples_1.data()};
    const std::array allsamples_10{samples_1.data(), samples_0.data()};
    const std::array allsamples_11{samples_1.data(), samples_1.data()};

    {
        VG::ContextOneSong c1s(true, fgp.freq);
        c1s.AnalyzeSamples(allsamples_00.data(), samples_0.size());

        const auto gain = c1s.GetTitleGain();
        const auto peak = c1s.getPeak();

        INFO("Current stereo case: 00");

        const auto audjusted_fgp_gain = 64.82f;

        REQUIRE(gain.has_value());
        REQUIRE(gain.value() == make_approx_gain(audjusted_fgp_gain));
        REQUIRE(peak.has_value());
        REQUIRE(peak.value() == 0.f);
    }
    {
        VG::ContextOneSong c1s(true, fgp.freq);
        c1s.AnalyzeSamples(allsamples_01.data(), samples_0.size());

        const auto gain = c1s.GetTitleGain();
        const auto peak = c1s.getPeak();

        INFO("Current stereo case: 01");

        const auto audjusted_fgp_gain = -14.25f;

        REQUIRE(gain.has_value());
        REQUIRE(gain.value() == make_approx_gain(audjusted_fgp_gain));
        REQUIRE(peak.has_value());
        REQUIRE(peak.value() == make_approx_peak(fgp.peak));
    }
    {
        VG::ContextOneSong c1s(true, fgp.freq);
        c1s.AnalyzeSamples(allsamples_10.data(), samples_0.size());

        const auto gain = c1s.GetTitleGain();
        const auto peak = c1s.getPeak();

        INFO("Current stereo case: 10");

        const auto audjusted_fgp_gain = -14.25f;

        REQUIRE(gain.has_value());
        REQUIRE(gain.value() == make_approx_gain(audjusted_fgp_gain));
        REQUIRE(peak.has_value());
        REQUIRE(peak.value() == make_approx_peak(fgp.peak));
    }
    {
        VG::ContextOneSong c1s(true, fgp.freq);
        c1s.AnalyzeSamples(allsamples_11.data(), samples_0.size());

        const auto gain = c1s.GetTitleGain();
        const auto peak = c1s.getPeak();

        INFO("Current stereo case: 11");

        const auto audjusted_fgp_gain = fgp.gain;

        REQUIRE(gain.has_value());
        REQUIRE(gain.value() == make_approx_gain(audjusted_fgp_gain));
        REQUIRE(peak.has_value());
        REQUIRE(peak.value() == make_approx_peak(fgp.peak));
    }
}

TEST_CASE("ContextOneSong gain stereo varied buffer sizes")
{
    struct freq_gain_peak
    {
        unsigned int freq;
        float gain;
        float peak;
    };

    const auto fgp = freq_gain_peak{8000, -17.26f, 32768.f};
    const auto samples_1 = VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 1.f);

    for (const auto& buff_size : {1U, 2U, 3U, 7U, 8U, 9U, 16U, 32U, 64U, 127U, 128U, 129U, 255U, 256U, 257U, 1023U, 1024U, 1025U})
    {
        VG::ContextOneSong c1s(true, fgp.freq);

        for (size_t idx = 0; idx < samples_1.size(); idx += buff_size)
        {
            const std::array partial_samples_11{&samples_1[idx], &samples_1[idx]};
            const auto capped_size = std::min(static_cast<size_t>(buff_size), samples_1.size() - idx);

            c1s.AnalyzeSamples(partial_samples_11.data(), capped_size);
        }

        const auto gain = c1s.GetTitleGain();
        const auto peak = c1s.getPeak();

        INFO("Current buf size: " << buff_size);

        REQUIRE(gain.has_value());
        REQUIRE(gain.value() == make_approx_gain(fgp.gain));
        REQUIRE(peak.has_value());
        REQUIRE(peak.value() == make_approx_peak(fgp.peak));
    }
}

TEST_CASE("ContextOneAlbum gain mono random samples")
{
    struct freq_gain_peak
    {
        unsigned int freq;
        float gain;
        float peak;
    };

    const auto fgp = freq_gain_peak{8000, -17.26f, 32768.f};

    const auto samples_01_L = std::make_tuple(VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 0.1f), 0.1f);
    const auto samples_1_L = std::make_tuple(VG::test_lib::make_deterministic_random_samples(512 * 1024, 0, 1.f), 1.f);

    const std::array allsamples_01{std::get<0>(samples_01_L).data()};
    const std::array allsamples_1{std::get<0>(samples_1_L).data()};

    VG::ContextOneAlbum c1a;

    REQUIRE(c1a.GetAlbumGain().has_value() == false);

    VG::ContextOneSong c1s(false, fgp.freq);
    c1s.AnalyzeSamples(allsamples_01.data(), std::get<0>(samples_01_L).size());

    c1a.updateWithCurrentSong(c1s);

    const auto gain1 = c1a.GetAlbumGain();
    const auto audjusted_fgp_gain1 = fgp.gain + std::log10(1.f / std::get<1>(samples_01_L)) * 20;

    REQUIRE(gain1.has_value());
    REQUIRE(gain1.value() == make_approx_gain(audjusted_fgp_gain1));

    VG::ContextOneSong c1s2(false, fgp.freq);
    c1s2.AnalyzeSamples(allsamples_1.data(), std::get<0>(samples_1_L).size());

    c1a.updateWithCurrentSong(c1s2);

    const auto gain2 = c1a.GetAlbumGain();
    // magic value from previous run
    const auto audjusted_fgp_gain2 = -17.12f;

    REQUIRE(gain2.has_value());
    REQUIRE(gain2.value() == make_approx_gain(audjusted_fgp_gain2));
}
