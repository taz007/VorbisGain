#include <catch2/catch_test_macros.hpp>

#include "path_to.h"

TEST_CASE("path_to_a_dir_ctor")
{
    REQUIRE(VG::path_to_a_dir().str() == "");
    REQUIRE(VG::path_to_a_dir(std::in_place).str() == "");
    REQUIRE(VG::path_to_a_dir(std::in_place, "/").str() == "/");

    REQUIRE(VG::path_to_a_dir(std::in_place, "foo").str() == "foo/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "foo/").str() == "foo/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "foo", "bar").str() == "foo/bar/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "foo", "bar/").str() == "foo/bar/");

    REQUIRE(VG::path_to_a_dir(std::in_place, "/foo").str() == "/foo/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "/foo/").str() == "/foo/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "/foo", "bar").str() == "/foo/bar/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "/foo", "bar/").str() == "/foo/bar/");

    REQUIRE(VG::path_to_a_dir(std::in_place, "foo", "bar").str() == "foo/bar/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "/", "foo", "bar").str() == "/foo/bar/");
}

TEST_CASE("path_to_a_dir_add")
{
    REQUIRE(VG::path_to_a_dir().add_subdir("foo").str() == "foo/");
    REQUIRE(VG::path_to_a_dir(std::in_place, "/").add_subdir("foo").str() == "/foo/");
}

TEST_CASE("path_to_a_file_ctor")
{
    REQUIRE(VG::path_to_a_file(VG::path_to_a_dir(std::in_place, "/foo", "bar"), "meh.txt").str() == "/foo/bar/meh.txt");
    REQUIRE(VG::path_to_a_file(VG::path_to_a_dir(std::in_place, "foo", "bar"), "meh.txt").str() == "foo/bar/meh.txt");
    REQUIRE(VG::path_to_a_file(VG::path_to_a_dir(), "meh.txt").str() == "meh.txt");
}
