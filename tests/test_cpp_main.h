#ifndef VG_TEST_CPP_MAIN_H
#define VG_TEST_CPP_MAIN_H

#include "path_to.h"

const VG::path_to_a_file& vorbisgain_path();
const VG::path_to_a_file& vorbiscomment_path();
const VG::path_to_a_dir& alldefs_path();
const VG::path_to_a_dir& current_test_path();

#endif
