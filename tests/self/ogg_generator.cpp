#include <catch2/catch_test_macros.hpp>
#include <list>
#include <utility>

#include "test_lib.h"
#include "test_lib_fs.h"

TEST_CASE("ogg_generator_types")
{
    using namespace VG::test_lib::ogg_generator;

    const comment c = "fooo=bar";
    REQUIRE(!c.empty());

    const stream s = {{"filename.ogg"}, {"a=b", "b=c"}};
    REQUIRE(!s.input.filename.empty());
    REQUIRE(s.comments.size() == 2);

    const multiplexed_streams ms = {{{"filename.ogg"}, {"a=b", "b=c"}}, {{"filename2.ogg"}, {"a=b", "b=c"}}};
    REQUIRE(ms.size() == 2);

    const description d = {{{{{"filename.ogg"}, {"a=b", "b=c"}}, {{"filename2.ogg"}, {"a=b", "b=c"}}},
                            {{{"filename3.ogg"}, {"a=b", "b=c"}}, {{"filename4.ogg"}, {"a=b", "b=c"}}}},
                           {}};
    REQUIRE(d.cm_streams.size() == 2);
    REQUIRE(d.file_timestamps.has_value() == false);

    const description d2{{{{{"filename.ogg"}, {"a=b", "b=c"}}, {{"filename2.ogg"}, {"a=b", "b=c"}}},
                          {{{"filename3.ogg"}, {"a=b", "b=c"}}, {{"filename4.ogg"}, {"a=b", "b=c"}}}},
                         VG::meta_filetimestamps{std::chrono::system_clock::now(), std::chrono::system_clock::now()}};
    REQUIRE(d2.cm_streams.size() == 2);
    REQUIRE(d2.file_timestamps.has_value() == true);
}

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                         {
                                             " Z=z",
                                             "A=A",
                                             "B=B",
                                             "c=c",
                                         }}}},
                                      {}};

static const OG::description a2_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                          {
                                              " Z=Z",
                                              "A=A",
                                              "B=B",
                                              "c=c",
                                          }}}},
                                       {}};

static const OG::description a3_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                          {
                                              "A=A",
                                              "B=B",
                                              "c=c",
                                              " Z=z",
                                          }}}},
                                       {}};

using namespace std::chrono_literals;

// -24h is to avoid having the same time for ts1 than for a file without a meta_filetimestamps specified
// as they would be the same otherwise
static const auto ts_now = std::chrono::system_clock::now();

static const VG::meta_filetimestamps ts1 = {ts_now - 24h, ts_now - 24h};
static const VG::meta_filetimestamps ts2 = {ts1.atime - 24h, ts1.mtime};
static const VG::meta_filetimestamps ts3 = {ts1.atime - 24h, ts1.mtime - 24h};

static const OG::description b0_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                          {
                                              "FOO=BAR",
                                          }}}},
                                       {}};

static const OG::description b1_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                          {
                                              "FOO=BAR",
                                          }}}},
                                       ts1};

static const OG::description b2_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                          {
                                              "FOO=BAR",
                                          }}}},
                                       ts2};

static const OG::description b3_ogg = {{{{OG::ogg_samples::S512K_44100_1,
                                          {
                                              "FOO=BAR",
                                          }}}},
                                       ts3};

static const OG::description nocomment_ogg = {{
                                                  {{OG::ogg_samples::S512K_44100_1, {}}},
                                              },
                                              {}};

static const OG::description nostrict_ogg = {{{
                                                 {OG::ogg_samples::S512K_44100_1,
                                                  {
                                                      "REPLAYGAIN_TRACK_PEAK=0.8567",
                                                      "REPLAYGAIN_TRACK_GAIN=-6 dB",
                                                  }},
                                             }},
                                             {}};

static const OG::description nostrict_ok_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_1,
                                                     {
                                                         "REPLAYGAIN_TRACK_PEAK=0.8567001",
                                                         "REPLAYGAIN_TRACK_GAIN=-6.010203 dB",
                                                     }},
                                                }},
                                                {}};

static const OG::description nostrict_nok_ogg = {{{
                                                     {OG::ogg_samples::S512K_44100_1,
                                                      {
                                                          "REPLAYGAIN_TRACK_PEAK=0.1",
                                                          "REPLAYGAIN_TRACK_GAIN=-6 dB",
                                                      }},
                                                 }},
                                                 {}};

static const auto inputs_desc =
    VG::test_lib::my_make_array<const OG::description*>(&a_ogg, &a2_ogg, &a3_ogg, &b0_ogg, &b1_ogg, &b2_ogg, &b3_ogg,
                                                        &nocomment_ogg, &nostrict_ogg, &nostrict_ok_ogg, &nostrict_nok_ogg);

TEST_CASE("ogg_generator")
{
    VG::fs::temporary_directory tempdir;

    std::list<VG::fs::temporary_file> outfiles;
    // generate phase
    for (const auto& d : inputs_desc)
    {
        outfiles.emplace_back(tempdir.path_to());
        const auto& outputfile = outfiles.back().path_to();

        VG::test_lib::ogg_generator::generate(*d, outputfile, tempdir);

        // deterministic test
        const VG::path_to_a_file outputfile_copy(outputfile.dir(), outputfile.file_node().name() + ".copy");
        VG::test_lib::ogg_generator::generate(*d, outputfile_copy, tempdir);
        // must be binary identical
        REQUIRE(VG::fs::is_same_content(outputfile, outputfile_copy));
    }

    constexpr bool T = true;
    constexpr bool F = false;
    // verify phase
    constexpr std::array<std::array<bool, 11>, 11> truth_strict = {{
        {T, F, F, F, F, F, F, F, F, F, F}, // a
        {F, T, F, F, F, F, F, F, F, F, F}, // a2,
        {F, F, T, F, F, F, F, F, F, F, F}, // a3
        {F, F, F, T, T, T, T, F, F, F, F}, // b0
        {F, F, F, F, T, F, F, F, F, F, F}, // b1
        {F, F, F, F, F, T, F, F, F, F, F}, // b2
        {F, F, F, F, F, F, T, F, F, F, F}, // b3
        {F, F, F, F, F, F, F, T, F, F, F}, // nocomment
        {F, F, F, F, F, F, F, F, T, F, F}, // nostrict
        {F, F, F, F, F, F, F, F, F, T, F}, // nostrict_ok
        {F, F, F, F, F, F, F, F, F, F, T}, // nostrict_nok
    }};
    constexpr std::array<std::array<bool, 11>, 11> truth_nostrict = {{
        {T, F, F, F, F, F, F, F, F, F, F}, // a
        {F, T, F, F, F, F, F, F, F, F, F}, // a2,
        {F, F, T, F, F, F, F, F, F, F, F}, // a3
        {F, F, F, T, T, T, T, F, F, F, F}, // b0
        {F, F, F, F, T, F, F, F, F, F, F}, // b1
        {F, F, F, F, F, T, F, F, F, F, F}, // b2
        {F, F, F, F, F, F, T, F, F, F, F}, // b3
        {F, F, F, F, F, F, F, T, F, F, F}, // nocomment
        {F, F, F, F, F, F, F, F, T, T, F}, // nostrict
        {F, F, F, F, F, F, F, F, T, T, F}, // nostrict_ok
        {F, F, F, F, F, F, F, F, F, F, T}, // nostrict_nok
    }};

    {
        unsigned int i_idx = 0;
        for (const auto& d_i : inputs_desc)
        {
            unsigned int j_idx = 0;
            for (const auto& outfile : outfiles)
            {
                const auto& outputfile = outfile.path_to();

                INFO("i:" << i_idx << " j:" << j_idx);

                if (truth_strict[i_idx][j_idx])
                    VG::test_lib::ogg_generator::verify_strict(*d_i, outputfile, tempdir);
                else
                    REQUIRE_THROWS(VG::test_lib::ogg_generator::verify_strict(*d_i, outputfile, tempdir));

                if (truth_nostrict[i_idx][j_idx])
                    VG::test_lib::ogg_generator::verify(*d_i, outputfile, tempdir);
                else
                    REQUIRE_THROWS(VG::test_lib::ogg_generator::verify(*d_i, outputfile, tempdir));

                ++j_idx;
            }
            ++i_idx;
        }
    }
}

TEST_CASE("compare_comments")
{
    REQUIRE(VG::test_lib::fuzzy_match("REPLAYGAIN_TRACK_GAIN=0.5 db", "REPLAYGAIN_TRACK_GAIN=0.5 db", 0.0001f) == true);
    REQUIRE(VG::test_lib::fuzzy_match("REPLAYGAIN_TRACK_GAIN=0.5 db", "REPLAYGAIN_ALBUM_GAIN=0.5 db", 0.0001f) == false);
    REQUIRE(VG::test_lib::fuzzy_match("REPLAYGAIN_TRACK_GAIN=0.5 db", "REPLAYGAIN_TRACK_GAIN=0.6 db", 0.0001f) == false);
    REQUIRE(VG::test_lib::fuzzy_match("REPLAYGAIN_TRACK_GAIN=0.5 db", "REPLAYGAIN_TRACK_GAIN=0.6 db", 0.2f) == true);

    REQUIRE(VG::test_lib::compare_comment("FOO", "BAR") == false);
    REQUIRE(VG::test_lib::compare_comment("FOO", "FOO") == true);
    REQUIRE(VG::test_lib::compare_comment("T=T", "T=T") == true);
    REQUIRE(VG::test_lib::compare_comment("T=T", "T=U") == false);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_GAIN=-6 dB", "REPLAYGAIN_TRACK_GAIN=-6 dB") == true);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_GAIN=-6 dB", "REPLAYGAIN_TRACK_GAIN=-6 DB") == false);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_GAIN=-6 dB", "REPLAYGAIN_TRACK_GAIN=-6.001 dB") == true);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_GAIN=-6 dB", "REPLAYGAIN_TRACK_GAIN=-6.1 dB") == true);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_GAIN=-6 dB", "REPLAYGAIN_TRACK_GAIN=-4.999 dB") == false);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_PEAK=0.8567", "REPLAYGAIN_TRACK_PEAK=0.8567") == true);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_PEAK=0.8567", "REPLAYGAIN_TRACK_PEAK=0.8567890") == true);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_PEAK=0.8567", "REPLAYGAIN_TRACK_PEAK=0.86") == true);
    REQUIRE(VG::test_lib::compare_comment("REPLAYGAIN_TRACK_PEAK=0.8567", "REPLAYGAIN_TRACK_PEAK=1.8567890") == false);
}
