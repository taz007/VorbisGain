#include "test_lib.h"

#include <catch2/catch_test_macros.hpp>
#include <cstdlib>

#include "misc.h"
#include "test_lib_c.h"

TEST_CASE("test_lib_start")
{
    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(VG::make_path_to_a_file("/bin/true"));
    VG::test_lib::started_ex<VG::test_lib::verify_exec_fail>(VG::make_path_to_a_file("/bin/false"));
    VG::test_lib::started_ex<VG::test_lib::verify_exec_fail>(VG::make_path_to_a_file("doesntexist"));
    VG::test_lib::started_ex<VG::test_lib::verify_exec_fail>(VG::make_path_to_a_file(""));
}

TEST_CASE("test_lib_exec_arguments")
{
    VG::test_lib::exec_arguments args(VG::make_path_to_a_file("/bin/foo/mysoftware"), "arg1", "arg2");
    REQUIRE(std::string_view(args.args()[0]) == "mysoftware");
    REQUIRE(args.args().size() == 4);

    const auto v = std::vector<std::string>{"arg1", "arg2"};
    VG::test_lib::exec_arguments args2(VG::make_path_to_a_file("/bin/foo/mysoftware"), v);
    REQUIRE(std::string_view(args2.args()[0]) == "mysoftware");
    REQUIRE(args2.args().size() == 4);
}

TEST_CASE("locale_comma")
{
    const auto comma_locale = VG::test_lib::make_locale_comma_separator_for_numpunct();
    std::stringstream ss;
    ss.imbue(comma_locale);
    ss.precision(2);
    const double val = 123;
    ss << std::showpoint << std::fixed << val;
    REQUIRE(ss.str() == "123,00");
}

TEST_CASE("scoped_global_locale")
{
    const auto comma_locale = VG::test_lib::make_locale_comma_separator_for_numpunct();

    {
        VG::test_lib::scoped_global_locale newlocale(comma_locale);

        std::stringstream ss;
        ss.precision(2);
        const double val = 123;
        ss << std::showpoint << std::fixed << val;
        REQUIRE(ss.str() == "123,00");
    }
    std::stringstream ss;
    ss.precision(2);
    const double val = 123;
    ss << std::showpoint << std::fixed << val;
    REQUIRE(ss.str() == "123.00");
}
