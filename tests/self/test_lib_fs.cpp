#include "test_lib_fs.h"

#include <array>
#include <catch2/catch_test_macros.hpp>
#include <fstream>
#include <string>
#include <string_view>

#include "iofile.h"
#include "path_to_stat.h"
#include "test_lib.h"

TEST_CASE("test_lib_fs_basename")
{
    REQUIRE(VG::fs::basename("") == "");
    REQUIRE(VG::fs::basename("something") == "something");
    REQUIRE(VG::fs::basename("/foo/bar") == "bar");
    REQUIRE(VG::fs::basename("meh/foo/bar") == "bar");
    REQUIRE(VG::fs::basename("meh/foo/bar/") == "");
    REQUIRE(VG::fs::basename("//") == "");
    REQUIRE(VG::fs::basename("/") == "");
}

TEST_CASE("test_lib_fs_is_same_content")
{
    VG::fs::temporary_directory tempdir;
    VG::fs::temporary_file file_empty(tempdir.path_to());
    VG::fs::temporary_file file_tiny(tempdir.path_to());
    VG::fs::temporary_file file_tiny_copy(tempdir.path_to());
    VG::fs::temporary_file file_tiny_not_copy(tempdir.path_to());
    VG::fs::temporary_file file_huge(tempdir.path_to());
    VG::fs::temporary_file file_huge_copy(tempdir.path_to());
    VG::fs::temporary_file file_huge_not_copy(tempdir.path_to());

    {
        VG::ofile ofs_file_tiny(file_tiny.path_to());
        VG::ofile ofs_file_tiny_copy(file_tiny_copy.path_to());
        VG::ofile ofs_file_tiny_not_copy(file_tiny_not_copy.path_to());
        VG::ofile ofs_file_huge(file_huge.path_to());
        VG::ofile ofs_file_huge_copy(file_huge_copy.path_to());
        VG::ofile ofs_file_huge_not_copy(file_huge_not_copy.path_to());

        static const std::string hi0there = std::string("hi").operator+=('\0').append("there");
        static const std::string hinthere = std::string("hi\nthere");

        ofs_file_tiny << hi0there;
        ofs_file_tiny_copy << hi0there;
        ofs_file_tiny_not_copy << hinthere;

        constexpr std::string_view hugestr = "HUUUUUUUUUGE";
        constexpr std::string_view hugestr2 = "HUUUUUUUUUge";
        constexpr int loop_huge = 1024 * 100;

        for (int i = 0; i < loop_huge; ++i)
        {
            ofs_file_huge << hugestr;
            ofs_file_huge_copy << hugestr;
            ofs_file_huge_not_copy << hugestr2;
        }

        ofs_file_tiny.close();
        ofs_file_tiny_copy.close();
        ofs_file_tiny_not_copy.close();
        ofs_file_huge.close();
        ofs_file_huge_copy.close();
        ofs_file_huge_not_copy.close();
    }

    const auto all_files = {&file_empty, &file_tiny,      &file_tiny_copy,    &file_tiny_not_copy,
                            &file_huge,  &file_huge_copy, &file_huge_not_copy};

    const std::array<std::array<bool, 7>, 7> truth = {{
        {true, false, false, false, false, false, false},
        {false, true, true, false, false, false, false},
        {false, true, true, false, false, false, false},
        {false, false, false, true, false, false, false},
        {false, false, false, false, true, true, false},
        {false, false, false, false, true, true, false},
        {false, false, false, false, false, false, true},
    }};
    {
        unsigned int i_idx = 0;
        for (const auto& i : all_files)
        {
            unsigned int j_idx = 0;
            for (const auto& j : all_files)
            {
                if (truth[i_idx][j_idx])
                    REQUIRE(VG::fs::is_same_content(i->path_to(), j->path_to()));
                else
                    REQUIRE_FALSE(VG::fs::is_same_content(i->path_to(), j->path_to()));
                ++j_idx;
            }
            ++i_idx;
        }
    }
}

TEST_CASE("test_lib_tempdir")
{
    {
        // delete at end, manual remove
        VG::fs::temporary_directory t;
        VG::path_to_stat t_stat(t.path_to());
        REQUIRE(t_stat.is_valid());
        REQUIRE(t.path_to().str().length() > 0);
        REQUIRE(t_stat.is_a_dir());
        t.remove();
        VG::path_to_stat t_stat2(t.path_to());
        REQUIRE_FALSE(t_stat2.is_valid());
    }

    { // delete at end, auto remove
        VG::path_to_a_dir tdir;
        {
            VG::fs::temporary_directory t;
            tdir = t.path_to();
            VG::path_to_stat t_stat(t.path_to());
            REQUIRE(t_stat.is_valid());
            REQUIRE(t_stat.is_a_dir());
        }
        VG::path_to_stat t_stat(tdir);
        REQUIRE_FALSE(t_stat.is_valid());
    }

    { // throw on invalid directory
        VG::test_lib::temporary_env_variable tmpdir_invalid("TMPDIR", "/tmp/INVALIDDIRECTORY/THAT/DOES/NOT/EXIST");
        REQUIRE_THROWS(VG::fs::temporary_directory());
    }

    { // create file in temp dir, must delete file
        VG::path_to_a_dir tdir;
        {
            VG::fs::temporary_directory t;
            tdir = t.path_to();
            VG::fs::temporary_file f(t.path_to());
            VG::path_to_a_file fcopy(f.path_to());
            fcopy.file_node().name().append(".copy");
            VG::fs::copy_file(f.path_to(), fcopy);
        }
        VG::path_to_stat t_stat(tdir);
        REQUIRE_FALSE(t_stat.is_valid());
    }
}

TEST_CASE("test_lib_tempfile")
{
    VG::fs::temporary_directory tempdir;
    {
        // delete at end, manual remove
        VG::fs::temporary_file f(tempdir.path_to());
        REQUIRE(f.path_to().str().length() > 0);

        VG::path_to_stat f_stat(f.path_to());
        REQUIRE(f_stat.is_valid());
        REQUIRE(f_stat.is_a_file());

        f.remove();
        VG::path_to_stat f_stat2(f.path_to());
        REQUIRE_FALSE(f_stat2.is_valid());
    }

    { // delete at end, auto remove
        std::optional<VG::path_to_a_file> fpath;
        {
            VG::fs::temporary_file f(tempdir.path_to());
            fpath = f.path_to();
            VG::path_to_stat f_stat(f.path_to());
            REQUIRE(f_stat.is_valid());
            REQUIRE(f_stat.is_a_file());
        }
        VG::path_to_stat f_stat(fpath.value());
        REQUIRE_FALSE(f_stat.is_valid());
    }
}

TEST_CASE("test_lib_tempfile_withname")
{
    VG::fs::temporary_directory tempdir;
    {
        // delete at end, manual remove
        VG::fs::temporary_file f(VG::path_to_a_file(tempdir.path_to(), "foobar"));
        REQUIRE(f.path_to().file_node().name() == "foobar");

        VG::path_to_stat f_stat(f.path_to());
        REQUIRE(f_stat.is_valid());
        REQUIRE(f_stat.is_a_file());

        f.remove();
        VG::path_to_stat f_stat2(f.path_to());
        REQUIRE_FALSE(f_stat2.is_valid());
    }

    { // delete at end, auto remove
        std::optional<VG::path_to_a_file> fpath;
        {
            VG::fs::temporary_file f(VG::path_to_a_file(tempdir.path_to(), "foobar"));
            fpath = f.path_to();
            VG::path_to_stat f_stat(f.path_to());
            REQUIRE(f_stat.is_valid());
            REQUIRE(f_stat.is_a_file());
        }
        VG::path_to_stat f_stat(fpath.value());
        REQUIRE_FALSE(f_stat.is_valid());
    }
}
