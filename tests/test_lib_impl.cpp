#include "test_lib_impl.h"

#include <fstream>
#include <regex>
#include <string>

#include "iofile.h"

namespace VG::test_lib_impl
{
    static const std::regex no_progress_regex("0% - ", std::regex_constants::optimize);
    const std::regex found_replaygain_regex("Found ReplayGain tags in", std::regex_constants::optimize);

    void verify_file_ignore::operator()(const fs::temporary_file&)
    {
    }

    void verify_file_no_progress::operator()(const fs::temporary_file& file)
    {
        ifile ifs(file.path_to());
        while (true)
        {
            const auto line = ifs.getline();
            if (line.has_value())
            {
                if (std::regex_search(line.value(), no_progress_regex))
                    throw std::runtime_error(Stringify() << "file contains progress pattern:" << line.value());
            }
            else
            {
                break;
            }
        }
    }

    void write_buffer_to(const std::byte* src, std::size_t len, std::ostream& os)
    {
        os.write(reinterpret_cast<const char*>(src), static_cast<std::streamsize>(len));
    }
} // namespace VG::test_lib_impl
