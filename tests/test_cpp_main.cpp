#include "test_cpp_main.h"

#include <catch2/catch_session.hpp>
#include <iostream>
#include <string>

#include "path_to.h"

static std::optional<VG::path_to_a_file> vorbisgain_path_;
static std::optional<VG::path_to_a_file> vorbiscomment_path_;
static std::optional<VG::path_to_a_dir> alldefs_path_;
static std::optional<VG::path_to_a_dir> current_test_path_;

const VG::path_to_a_file& vorbisgain_path()
{
    return vorbisgain_path_.value();
}

const VG::path_to_a_file& vorbiscomment_path()
{
    return vorbiscomment_path_.value();
}

const VG::path_to_a_dir& alldefs_path()
{
    return alldefs_path_.value();
}

const VG::path_to_a_dir& current_test_path()
{
    return current_test_path_.value();
}

int main(int argc, char* argv[])
{
    Catch::Session session;

    std::string tmp_alldef;
    std::string tmp_test_path;
    std::string tmp_vorbiscomment_path;
    std::string tmp_vorbisgain_path;

    // Build a new parser on top of Catch's
    using namespace Catch::Clara;
    const auto cli = session.cli() | Opt(tmp_vorbisgain_path, "path")["--vgpath"]("path to vorbisgain to run").required() |
                     Opt(tmp_alldef, "dir")["--alldefspath"]("path with the generated ogg").required() |
                     Opt(tmp_test_path, "dir")["--test_dir"]("path with the current test").required() |
                     Opt(tmp_vorbiscomment_path, "path")["--vorbiscommentpath"]("path to vorbiscomment to run").required();

    // Now pass the new composite back to Catch so it uses that
    session.cli(cli);

    // Let Catch (using Clara) parse the command line
    const int returnCode = session.applyCommandLine(argc, argv);
    if (returnCode != 0) // Indicates a command line error
        return returnCode;

    // post process
    alldefs_path_.emplace(std::in_place, std::move(tmp_alldef));
    current_test_path_.emplace(std::in_place, std::move(tmp_test_path));
    vorbisgain_path_.emplace(VG::make_path_to_a_file(std::move(tmp_vorbisgain_path)));
    vorbiscomment_path_.emplace(VG::make_path_to_a_file(std::move(tmp_vorbiscomment_path)));

    return session.run();
}
