#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_middle_44100_2, {}},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_middle_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

TEST_CASE("no_progress")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg);

    for (const auto& arg : {"-n", "--no-progress"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok_stdout_no_progress>(inputs_desc, arg);
    }
}
