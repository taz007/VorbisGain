#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=1.23456",
                                                     "REPLAYGAIN_TRACK_GAIN=-1.98 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description& a_processed_ogg = a_togen_ogg;

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_middle_44100_2,
                                                 {
                                                     "BLABLA=somethingsomething",
                                                     "A=B=C=D",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_middle_44100_2,
                                                     {
                                                         "BLABLA=somethingsomething",
                                                         "A=B=C=D",
                                                         OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

static const OG::description incomplete_togen_ogg = {{{
                                                         {OG::ogg_samples::S512K_middle_44100_2,
                                                          {
                                                              OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                              "BLABLA=somethingsomething",
                                                              "A=B=C=D",
                                                          }},
                                                     }},
                                                     {}};

static const OG::description incomplete_processed_ogg = {{{
                                                             {OG::ogg_samples::S512K_middle_44100_2,
                                                              {
                                                                  "BLABLA=somethingsomething",
                                                                  "A=B=C=D",
                                                                  OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                                  OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                              }},
                                                         }},
                                                         {}};

static const OG::description incomplete2_togen_ogg = {{{
                                                          {OG::ogg_samples::S512K_middle_44100_2,
                                                           {
                                                               OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                               "BLABLA=somethingsomething",
                                                               "A=B=C=D",
                                                               OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                           }},
                                                      }},
                                                      {}};

static const OG::description incomplete2_processed_ogg = {{{
                                                              {OG::ogg_samples::S512K_middle_44100_2,
                                                               {
                                                                   "BLABLA=somethingsomething",
                                                                   "A=B=C=D",
                                                                   OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                                   OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                               }},
                                                          }},
                                                          {}};

TEST_CASE("fast")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_processed_ogg);
    inputs_desc.emplace_back(incomplete_togen_ogg, incomplete_processed_ogg);
    inputs_desc.emplace_back(incomplete2_togen_ogg, incomplete2_processed_ogg);

    for (const auto& arg : {"-f", "--fast"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, arg);
    }
}
