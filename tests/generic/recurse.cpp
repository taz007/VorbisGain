#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const std::string ROOT_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK=1.23";
static const std::string ROOT_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN=-11.70 dB";
static const std::string DIRB_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK=1.19";
static const std::string DIRB_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN=-11.62 dB";
static const std::string DIRC_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK=1.23";
static const std::string DIRC_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN=-11.72 dB";

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                         ROOT_ALBUM_PEAK,
                                                         ROOT_ALBUM_GAIN,
                                                     }},
                                                }},
                                                {}};

static const OG::description b_a_processed_ogg = {{{
                                                      {OG::ogg_samples::S512K_44100_2,
                                                       {
                                                           OG::ogg_samples::S512K_44100_2_PEAK,
                                                           OG::ogg_samples::S512K_44100_2_GAIN,
                                                           DIRB_ALBUM_PEAK,
                                                           DIRB_ALBUM_GAIN,
                                                       }},
                                                  }},
                                                  {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_middle_44100_2,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description b_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_middle_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                         ROOT_ALBUM_PEAK,
                                                         ROOT_ALBUM_GAIN,
                                                     }},
                                                }},
                                                {}};

static const OG::description b_b_processed_ogg = {{{
                                                      {OG::ogg_samples::S512K_middle_44100_2,
                                                       {
                                                           OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                           OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                           DIRB_ALBUM_PEAK,
                                                           DIRB_ALBUM_GAIN,
                                                       }},
                                                  }},
                                                  {}};

static const OG::description c_b_processed_ogg = {{{
                                                      {OG::ogg_samples::S512K_middle_44100_2,
                                                       {
                                                           OG::ogg_samples::S512K_middle_44100_2_PEAK,
                                                           OG::ogg_samples::S512K_middle_44100_2_GAIN,
                                                           DIRC_ALBUM_PEAK,
                                                           DIRC_ALBUM_GAIN,
                                                       }},
                                                  }},
                                                  {}};

static const OG::description c_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_1,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description c_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_1,
                                                     {
                                                         OG::ogg_samples::S512K_44100_1_PEAK,
                                                         OG::ogg_samples::S512K_44100_1_GAIN,
                                                         ROOT_ALBUM_PEAK,
                                                         ROOT_ALBUM_GAIN,
                                                     }},
                                                }},
                                                {}};

static const OG::description c_c_processed_ogg = {{{
                                                      {OG::ogg_samples::S512K_44100_1,
                                                       {
                                                           OG::ogg_samples::S512K_44100_1_PEAK,
                                                           OG::ogg_samples::S512K_44100_1_GAIN,
                                                           DIRC_ALBUM_PEAK,
                                                           DIRC_ALBUM_GAIN,
                                                       }},
                                                  }},
                                                  {}};

TEST_CASE("recurse")
{
    /*
     * dir structure will be like this:
     * ./a.ogg
     * ./b/a.ogg
     * ./b/b.ogg
     * ./b.ogg
     * ./c/b.ogg
     * ./c/c.ogg
     * ./c.ogg
     */

    // run the test 2 times with different vorbisgain arguments
    for (auto arguments_test : {0, 1})
    {
        // processed description
        std::list<std::reference_wrapper<const OG::description>> out_descs;
        // processed ogg files
        std::vector<VG::path_to_a_file> outputs;

        VG::fs::temporary_directory tempdir;
        const auto dir_b = VG::path_to_a_dir(tempdir.path_to()).add_subdir("b");
        const auto dir_c = VG::path_to_a_dir(tempdir.path_to()).add_subdir("c");

        VG::fs::mkdir(dir_b);
        VG::fs::mkdir(dir_c);

        const auto setup_one_ogg = [&](const OG::description& in, const OG::description& out, const std::string& outname)
        {
            out_descs.emplace_back(out);
            outputs.emplace_back(VG::path_to_a_file(tempdir.path_to(), outname));
            const auto& outputfilename = outputs.back();
            VG::test_lib::ogg_generator::generate(in, outputfilename, tempdir);
        };

        // first time gives the directory only
        // second time gives the 3 files + 2 subdirs
        // the behaviour must be the same in both cases
        std::vector<std::string> args_for_test_0 = {"-r", "-a", tempdir.path_to().str()};
        std::vector<std::string> args_for_test_1 = {"-r", "-a"};

        setup_one_ogg(a_togen_ogg, a_processed_ogg, "a.ogg");
        setup_one_ogg(a_togen_ogg, b_a_processed_ogg, "b/a.ogg");
        setup_one_ogg(b_togen_ogg, b_b_processed_ogg, "b/b.ogg");
        setup_one_ogg(b_togen_ogg, b_processed_ogg, "b.ogg");
        setup_one_ogg(b_togen_ogg, c_b_processed_ogg, "c/b.ogg");
        setup_one_ogg(c_togen_ogg, c_c_processed_ogg, "c/c.ogg");
        setup_one_ogg(c_togen_ogg, c_processed_ogg, "c.ogg");

        args_for_test_1.emplace_back(VG::path_to_a_file(tempdir.path_to(), "a.ogg").str());
        args_for_test_1.emplace_back(dir_b.str());
        args_for_test_1.emplace_back(VG::path_to_a_file(tempdir.path_to(), "b.ogg").str());
        args_for_test_1.emplace_back(dir_c.str());
        args_for_test_1.emplace_back(VG::path_to_a_file(tempdir.path_to(), "c.ogg").str());

        const auto& real_args = arguments_test == 0 ? args_for_test_0 : args_for_test_1;

        // start vorbisgain
        VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), real_args);
        auto out_ite = outputs.begin();
        for (const auto& i : out_descs)
        {
            const auto& outputfilename = *out_ite;

            OG::verify(i, outputfilename, tempdir);

            ++out_ite;
        }
    }
}
