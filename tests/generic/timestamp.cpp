#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

TEST_CASE("timestamp")
{
    namespace OG = VG::test_lib::ogg_generator;

    using namespace std::chrono_literals;

    const auto ts_now = std::chrono::system_clock::now();

    const OG::description old_togen_ogg = {{{
                                               {OG::ogg_samples::S512K_44100_2, {}},
                                           }},
                                           {VG::meta_filetimestamps{ts_now - 24h, ts_now - 24h}}};

    const OG::description now_togen_ogg = {{{
                                               {OG::ogg_samples::S512K_44100_2, {}},
                                           }},
                                           {{VG::meta_filetimestamps{ts_now, ts_now}}}};

    const OG::description future_togen_ogg = {{{
                                                  {OG::ogg_samples::S512K_44100_2, {}},
                                              }},
                                              {{VG::meta_filetimestamps{ts_now + 24h, ts_now + 24h}}}};

    SECTION("keep")
    {
        const OG::description oldkeep_processed_ogg = {{{
                                                           {OG::ogg_samples::S512K_44100_2,
                                                            {
                                                                OG::ogg_samples::S512K_44100_2_PEAK,
                                                                OG::ogg_samples::S512K_44100_2_GAIN,
                                                            }},
                                                       }},
                                                       {old_togen_ogg.file_timestamps}};

        const OG::description nowkeep_processed_ogg = {{{
                                                           {OG::ogg_samples::S512K_44100_2,
                                                            {
                                                                OG::ogg_samples::S512K_44100_2_PEAK,
                                                                OG::ogg_samples::S512K_44100_2_GAIN,
                                                            }},
                                                       }},
                                                       {now_togen_ogg.file_timestamps}};

        const OG::description futurekeep_processed_ogg = {{{
                                                              {OG::ogg_samples::S512K_44100_2,
                                                               {
                                                                   OG::ogg_samples::S512K_44100_2_PEAK,
                                                                   OG::ogg_samples::S512K_44100_2_GAIN,
                                                               }},
                                                          }},
                                                          {future_togen_ogg.file_timestamps}};

        std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
        inputs_desc.emplace_back(old_togen_ogg, oldkeep_processed_ogg);
        inputs_desc.emplace_back(now_togen_ogg, nowkeep_processed_ogg);
        inputs_desc.emplace_back(future_togen_ogg, futurekeep_processed_ogg);

        for (const auto& arg : {"-k", "--keep-timestamps"})
        {
            VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, arg);
        }
    }

    SECTION("update")
    {
        const OG::description now_processed_ogg = {{{
                                                       {OG::ogg_samples::S512K_44100_2,
                                                        {
                                                            OG::ogg_samples::S512K_44100_2_PEAK,
                                                            OG::ogg_samples::S512K_44100_2_GAIN,
                                                        }},
                                                   }},
                                                   {VG::meta_filetimestamps{ts_now, ts_now}}};

        std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
        inputs_desc.emplace_back(old_togen_ogg, now_processed_ogg);
        inputs_desc.emplace_back(now_togen_ogg, now_processed_ogg);
        inputs_desc.emplace_back(future_togen_ogg, now_processed_ogg);

        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(OG::verify_close_enough_ts, inputs_desc);
    }
}
