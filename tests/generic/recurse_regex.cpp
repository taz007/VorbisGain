#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

TEST_CASE("recurse_regex")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg, "file1.ogg");
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg, "file2.ogg");
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg, "file3.ogg");
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg, "file2.ogg.fake");
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg, "afile1.ogg");

    const auto regex = "file[12].*ogg";

    for (const auto& r : {"-x", "--regex"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok, true>(inputs_desc, "-r", r, regex);
    }
}

TEST_CASE("recurse_regex2")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg, "file1.ogg");
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg, "file2.ogg");
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg, "phile3.ogg");
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg, "file2.ogg.fake");
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg, "afile1.ogg");

    constexpr auto regex = "^.*file.*ogg.*$";

    for (const auto& r : {"-x", "--regex"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok, true>(inputs_desc, "-r", r, regex);
    }
}
