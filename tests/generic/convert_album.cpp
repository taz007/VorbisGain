#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "RG_PEAK=3.14000010",
                                                     "RG_RADIO=-0.23 dB",
                                                     "RG_AUDIOPHILE=-4.56 dB",
                                                     "FOO=BAR",
                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         "FOO=BAR",
                                                         "REPLAYGAIN_TRACK_PEAK=3.14000010",
                                                         "REPLAYGAIN_TRACK_GAIN=-0.23 dB",
                                                         "REPLAYGAIN_ALBUM_PEAK=3.14000010",
                                                         "REPLAYGAIN_ALBUM_GAIN=-4.56 dB",
                                                     }},
                                                }},
                                                {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "RG_PEAK=2",
                                                     "RG_RADIO=-1.23 dB",
                                                     "RG_AUDIOPHILE=-4.56 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         "REPLAYGAIN_TRACK_PEAK=2.00000000",
                                                         "REPLAYGAIN_TRACK_GAIN=-1.23 dB",
                                                         "REPLAYGAIN_ALBUM_PEAK=3.14000010",
                                                         "REPLAYGAIN_ALBUM_GAIN=-4.56 dB",
                                                     }},
                                                }},
                                                {}};

TEST_CASE("convert_album")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_processed_ogg);

    for (const auto& arg : {"-C", "--convert"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, "-a", arg);
    }
}
