#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

TEST_CASE("Cmdline")
{
    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), "-h");
    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), "--help");
    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), "-v");
    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), "--version");
    VG::test_lib::started_ex<VG::test_lib::verify_exec_fail>(vorbisgain_path(), "--notgonnahavethisflagever");
    VG::test_lib::started_ex<VG::test_lib::verify_exec_fail>(vorbisgain_path(), "--notgonnahavethisflagever", "--help");
}
