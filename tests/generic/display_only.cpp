#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=0.9876",
                                                     "REPLAYGAIN_TRACK_GAIN=-5.66 dB",
                                                     "GENRE=Classical",
                                                     "TOTALDISCS=1",
                                                     "FOO=BAR",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_zero_44100_2, {}},
                                            }},
                                            {}};

TEST_CASE("display_only")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_togen_ogg);

    for (const auto& arg : {"-d", "--display-only"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, arg);
    }
}
