#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "RG_PEAK=3.14000010",
                                                     "RG_RADIO=-1.23 dB",
                                                     "RG_AUDIOPHILE=-4.56 dB",
                                                     "FOO=BAR",
                                                     "FOO=BAR2",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "RG_PEAK=3.14000010",
                                                     "RG_RADIO=-1.23 dB",
                                                     "RG_AUDIOPHILE=-4.56 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description c_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_1,
                                                 {

                                                 }},
                                            }},
                                            {}};

TEST_CASE("convert_album_fail2")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_togen_ogg);
    inputs_desc.emplace_back(c_togen_ogg, c_togen_ogg);

    for (const auto& arg : {"-C", "--convert"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_fail_err>(inputs_desc, "-a", arg);
    }
}
