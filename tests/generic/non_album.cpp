#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

// non RG tags present
static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "GENRE=Classical",
                                                     "TOTALDISCS=1",
                                                     "FOO=BAR",
                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         "GENRE=Classical",
                                                         "TOTALDISCS=1",
                                                         "FOO=BAR",
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

// already RG tagged
static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     OG::ogg_samples::S512K_44100_2_PEAK,
                                                     OG::ogg_samples::S512K_44100_2_GAIN,
                                                 }},
                                            }},
                                            {}};

static const OG::description& b_processed_ogg = b_togen_ogg;

// wrongly RG tagged
static const OG::description c_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=1.23456",
                                                     "REPLAYGAIN_TRACK_GAIN=-1.98 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description c_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

// no tags
static const OG::description d_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_1,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description d_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_1,
                                                     {
                                                         OG::ogg_samples::S512K_44100_1_PEAK,
                                                         OG::ogg_samples::S512K_44100_1_GAIN,
                                                     }},
                                                }},
                                                {}};

// weird tags present
static const OG::description e_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "BLABLA=somethingsomething",
                                                     "A=B=C=D",
                                                 }},
                                            }},
                                            {}};

static const OG::description e_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         "BLABLA=somethingsomething",
                                                         "A=B=C=D",
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

// silent file
static const OG::description f_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_zero_44100_2,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description f_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_zero_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_zero_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_zero_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

// empty file
static const OG::description empty_togen_ogg = {{{
                                                    {OG::ogg_samples::S0K,
                                                     {

                                                     }},
                                                }},
                                                {}};

static const OG::description& empty_processed_ogg = empty_togen_ogg;

// empty file with tags
static const OG::description empty2_togen_ogg = {{{
                                                     {OG::ogg_samples::S0K,
                                                      {
                                                          "BLABLA=somethingsomething",
                                                          "A=B=C=D",
                                                          "REPLAYGAIN_TRACK_PEAK=1.89978218",
                                                          "REPLAYGAIN_TRACK_GAIN=-11.80 dB",
                                                      }},
                                                 }},
                                                 {}};

// todo RG tags here should be removed instead
static const OG::description& empty2_processed_ogg = empty2_togen_ogg;

static const OG::description low_samples_togen_ogg = {{{
                                                          {OG::ogg_samples::S16,
                                                           {

                                                           }},
                                                      }},
                                                      {}};

static const OG::description low_samples_processed_ogg = {{{

                                                              {OG::ogg_samples::S16,
                                                               {
                                                                   OG::ogg_samples::S16_PEAK,
                                                               }},
                                                          }},
                                                          {}};

TEST_CASE("non_album")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_processed_ogg);
    inputs_desc.emplace_back(c_togen_ogg, c_processed_ogg);
    inputs_desc.emplace_back(d_togen_ogg, d_processed_ogg);
    inputs_desc.emplace_back(e_togen_ogg, e_processed_ogg);
    inputs_desc.emplace_back(f_togen_ogg, f_processed_ogg);
    inputs_desc.emplace_back(empty_togen_ogg, empty_processed_ogg);
    inputs_desc.emplace_back(empty2_togen_ogg, empty2_processed_ogg);
    inputs_desc.emplace_back(low_samples_togen_ogg, low_samples_processed_ogg);

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc);
}
