#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const std::string THIS_ALBUM_PEAK = "REPLAYGAIN_ALBUM_PEAK=1.2";
static const std::string THIS_ALBUM_GAIN = "REPLAYGAIN_ALBUM_GAIN=-1.23 dB";

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "FOO=BAR",
                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         "FOO=BAR",
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                         THIS_ALBUM_PEAK,
                                                         THIS_ALBUM_GAIN,
                                                     }},
                                                }},
                                                {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=1.23456",
                                                     "REPLAYGAIN_TRACK_GAIN=-1.23 dB",
                                                     "REPLAYGAIN_ALBUM_PEAK=4.5678",
                                                     "REPLAYGAIN_ALBUM_GAIN=-9.876 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                         THIS_ALBUM_PEAK,
                                                         THIS_ALBUM_GAIN,
                                                     }},
                                                }},
                                                {}};

static const OG::description c_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_1,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description c_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_1,
                                                     {
                                                         OG::ogg_samples::S512K_44100_1_PEAK,
                                                         OG::ogg_samples::S512K_44100_1_GAIN,
                                                         THIS_ALBUM_PEAK,
                                                         THIS_ALBUM_GAIN,
                                                     }},
                                                }},
                                                {}};

TEST_CASE("album_gain")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_processed_ogg);
    inputs_desc.emplace_back(c_togen_ogg, c_processed_ogg);

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, "--album-gain=-1.23");
    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, "-g", "-1.23");
}

TEST_CASE("album_gain_fail")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg);

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_fail_err>(inputs_desc, "-g", "ABCD");
    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_fail_err>(inputs_desc, "-g", "1234");
}
