#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=2.44409299",
                                                     "REPLAYGAIN_TRACK_GAIN=-11.98 dB",
                                                     "REPLAYGAIN_ALBUM_PEAK=2.44409299",
                                                     "REPLAYGAIN_ALBUM_GAIN=-1.23 dB",
                                                     "FOO=BAR",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=1.23456",
                                                     "REPLAYGAIN_TRACK_GAIN=-1.23 dB",
                                                     "REPLAYGAIN_ALBUM_PEAK=4.5678",
                                                     "REPLAYGAIN_ALBUM_GAIN=-9.876 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description c_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_1,
                                                 {

                                                 }},
                                            }},
                                            {}};

TEST_CASE("clean2_a")
{
    // files have RG comments, and are not modified, so same description used for verification
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_togen_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_togen_ogg);

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok_stdout_has_found_replaygain<true>>(inputs_desc, "-c",
                                                                                                            "-d");
}

TEST_CASE("clean2_b")
{
    // file has no RG comment
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(c_togen_ogg, c_togen_ogg);

    VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok_stdout_has_found_replaygain<false>>(inputs_desc, "-c",
                                                                                                             "-d");
}
