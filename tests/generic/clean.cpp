#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=2.44409299",
                                                     "REPLAYGAIN_TRACK_GAIN=-11.98 dB",
                                                     "REPLAYGAIN_ALBUM_PEAK=2.44409299",
                                                     "REPLAYGAIN_ALBUM_GAIN=-1.23 dB",
                                                     "FOO=BAR",
                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         "FOO=BAR",
                                                     }},
                                                }},
                                                {}};

static const OG::description b_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {
                                                     "REPLAYGAIN_TRACK_PEAK=1.23456",
                                                     "REPLAYGAIN_TRACK_GAIN=-1.23 dB",
                                                     "REPLAYGAIN_ALBUM_PEAK=4.5678",
                                                     "REPLAYGAIN_ALBUM_GAIN=-9.876 dB",
                                                 }},
                                            }},
                                            {}};

static const OG::description b_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2, {}},
                                                }},
                                                {}};

static const OG::description c_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_1,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description& c_processed_ogg = c_togen_ogg;

TEST_CASE("clean")
{
    std::list<VG::test_lib::descs_togenerate_processed> inputs_desc;
    inputs_desc.emplace_back(a_togen_ogg, a_processed_ogg);
    inputs_desc.emplace_back(b_togen_ogg, b_processed_ogg);
    inputs_desc.emplace_back(c_togen_ogg, c_processed_ogg);

    for (const auto& arg : {"-c", "--clean"})
    {
        VG::test_lib::generate_testfiles_run_vg<VG::test_lib::verify_exec_ok>(inputs_desc, arg);
    }
}
