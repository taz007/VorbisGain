#include <catch2/catch_test_macros.hpp>

#include "test_lib.h"

namespace OG = VG::test_lib::ogg_generator;

static const OG::description a_togen_ogg = {{{
                                                {OG::ogg_samples::S512K_44100_2,
                                                 {

                                                 }},
                                            }},
                                            {}};

static const OG::description a_processed_ogg = {{{
                                                    {OG::ogg_samples::S512K_44100_2,
                                                     {
                                                         OG::ogg_samples::S512K_44100_2_PEAK,
                                                         OG::ogg_samples::S512K_44100_2_GAIN,
                                                     }},
                                                }},
                                                {}};

TEST_CASE("hugecwd")
{
    const auto longdir = VG::path_to_a_dir(
        std::in_place, "0123456789012345678901234567890123456789012345678901234567890123456789012345678901234567890123456789");

    VG::fs::temporary_directory tempdir;

    const auto oldcwd = VG::path_to_a_dir(std::in_place, VG::getcwd());

    // build huge directories;
    VG::C::chdir::call(tempdir.path_to().str().c_str());
    for (int i = 0; i < 20; ++i)
    {
        VG::fs::mkdir(longdir);
        VG::C::chdir::call(longdir.str().c_str());
    }

    const auto outputfile = VG::path_to_a_file(VG::path_to_a_dir(std::in_place, VG::getcwd()), "out.ogg");
    // get back to original cwd
    VG::C::chdir::call(oldcwd.str().c_str());

    OG::generate(a_togen_ogg, outputfile, tempdir);

    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), "-r", tempdir.path_to().str());

    OG::verify(a_processed_ogg, outputfile, tempdir);
}

TEST_CASE("debian_537043")
{
    VG::fs::temporary_directory tempdir;

    auto output = VG::path_to_a_file(tempdir.path_to(), std::string(251, 'A'));
    output.file_node().name().append(".ogg");

    OG::generate(a_togen_ogg, output, tempdir);

    VG::test_lib::started_ex<VG::test_lib::verify_exec_ok>(vorbisgain_path(), output.str());

    OG::verify(a_processed_ogg, output, tempdir);
}

TEST_CASE("noperm_dir")
{
    VG::fs::temporary_directory tempdir;

    VG::C::chmod::call(tempdir.path_to().str().c_str(), static_cast<mode_t>(0));

    VG::test_lib::started_ex<VG::test_lib::verify_exec_fail_err>(vorbisgain_path(), "-r", tempdir.path_to().str());
    constexpr auto mode = static_cast<mode_t>(S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH);
    VG::C::chmod::call(tempdir.path_to().str().c_str(), mode);

    tempdir.remove();
}
