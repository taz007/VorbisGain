#include "ogg_generator.h"

#include <list>
#include <memory>
#include <string>

#include "globals.h"
#include "iofile.h"
#include "meta_ts.h"
#include "path_to_stat.h"
#include "stringify.h"
#include "test_lib.h"
#include "test_lib_fs.h"
#include "vorbisfilehelper.h"
#include "vorbisgainexception.h"

namespace VG::test_lib
{
    using extracted_comments = std::list<ogg_generator::comment>;
    using extracted_comments_multiplexed = std::list<extracted_comments>;
    using extracted_comments_chained = std::list<extracted_comments_multiplexed>;

    void dump(Stringify& s, const extracted_comments& ec);
    void dump(Stringify& s, const extracted_comments_multiplexed& ecm);
    void dump(Stringify& s, const extracted_comments_chained& ecc);

    void vc_to_extracted_comments(const vorbis_comment& vc, extracted_comments& list)
    {
        for (int i = 0; i < vc.comments; ++i)
        {
            list.emplace_back(vc.user_comments[i], vc.comment_lengths[i]);
        }
    }

    extracted_comments_chained extract_all_comments(const path_to_a_file& inputogg)
    {
        extracted_comments_chained all_comments;

        AutoOggVorbisFile ovf(inputogg.str());

        for (int i = 0; i < ovf.streams(); ++i)
        {
            all_comments.emplace_back();
            extracted_comments_multiplexed& mp = *all_comments.rbegin();
            mp.emplace_back();
            extracted_comments& cmts = *mp.rbegin();
            const vorbis_comment& vc = *ovf.comment(i);
            vc_to_extracted_comments(vc, cmts);
        }

        return all_comments;
    }

    template<typename CONTAINER>
    void fill_comments(fs::temporary_file& tempfile, const CONTAINER& comments)
    {
        ofile ofs(tempfile.path_to());

        for (const auto& c : comments)
        {
            ofs << c << '\n';
        }
        ofs.close();
    }

    template<typename LIST>
    void multiplex(const LIST& tempoggs, const fs::temporary_file& out)
    {
        if (tempoggs.size() != 1)
            throw Exception::Generic("Multiplexed streams not supported");

        // so just copy to out for now
        fs::copy_file(tempoggs.begin()->path_to(), out.path_to());
    }

    path_to_a_file get_full_inputfile(const ogg_generator::stream& stream)
    {
        return path_to_a_file(alldefs_path(), stream.input.filename);
    }

    template<typename C1, typename C2, typename FCT>
    void iterate_until_comments(C1&& c1, C2&& c2, FCT fct)
    {
        auto f_stream = [&](auto& stream1, auto& stream2)
        {
            // stream1 is a container of comments
            // stream2 is ogg_generator::stream
            iterate_both(stream1, stream2.comments, fct);
        };

        auto f_multiplexed = [&](auto& multiplexed1, auto& multiplexed2) { iterate_both(multiplexed1, multiplexed2, f_stream); };

        iterate_both(std::forward<C1>(c1), std::forward<C2>(c2), f_multiplexed);
    }

    std::pair<float, std::string> tag_value_to_float(const std::string& str)
    {
        std::pair<float, std::string> ret;
        std::size_t pos;

        ret.first = std::stof(str, &pos);
        ret.second = str.substr(pos);
        return ret;
    }

    // format of str is "RGTAG=float dB" , last part " dB" optional
    bool fuzzy_match(const std::string& c1, const std::string& c2, float margin)
    {
        const auto min_length = TAG_TRACK_PEAK.length() + 2;
        auto throw_short = [](auto c) { throw std::runtime_error(Stringify() << "Comment '" << c << "' too short"); };

        if (c1.length() < min_length)
            throw_short(c1);
        if (c2.length() < min_length)
            throw_short(c2);

        if (c1.substr(0, min_length - 1) != c2.substr(0, min_length - 1))
            return false;

        const auto& replaygain_tag1 = tag_value_to_float(c1.substr(min_length - 1));
        const auto& replaygain_tag2 = tag_value_to_float(c2.substr(min_length - 1));

        if (std::abs(replaygain_tag1.first - replaygain_tag2.first) > margin)
            return false;

        return replaygain_tag1.second == replaygain_tag2.second;
    }

    bool compare_comment(const std::string& c1, const std::string& c2)
    {
        // fortunately the VG tags have all the same length
        const auto& replaygain_tag = c1.substr(0, TAG_TRACK_PEAK.length());
        if (replaygain_tag == TAG_TRACK_PEAK || replaygain_tag == TAG_ALBUM_PEAK)
        {
            return fuzzy_match(c1, c2, ogg_generator::OGG_PEAK_PRECISION);
        }
        if (replaygain_tag == TAG_TRACK_GAIN || replaygain_tag == TAG_ALBUM_GAIN)
        {
            return fuzzy_match(c1, c2, ogg_generator::OGG_GAIN_PRECISION);
        }
        return c1 == c2;
    }

    template<typename C1, typename C2>
    void validate_comments(const C1& c1, const C2& c2)
    {
        auto f_comment = [](const auto& comment1, const auto& comment2)
        {
            // comment1 & comment2 are string
            if (!compare_comment(comment1, comment2))
            {
                throw std::runtime_error(Stringify() << "'" << comment1 << "' != '" << comment2 << "'");
            }
        };

        try
        {
            iterate_until_comments(c1, c2, f_comment);
        }
        catch (const std::exception& ex)
        {
            Stringify s;
            s << "Error while validating comments, got:\n";
            dump(s, c1);
            s << "but expected:\n";
            dump(s, c2);
            s << "Error is: " << std::string_view(ex.what());
            throw std::runtime_error(s);
        }
    }

    template<typename C1, typename C2>
    void update_comments(const C1& c1, C2& c2)
    {
        auto f_copy_comment = [](const auto& from, auto& to) { to = from; };

        iterate_until_comments(c1, c2, f_copy_comment);
    }

    template<typename T, typename STR>
    void operator_str_recurse_info(Stringify& s, STR&& info, const T& container)
    {
        s << std::forward<STR>(info);
        for (const auto& i : container)
        {
            dump(s, i);
        }
    }

    template<typename T>
    void operator_str_comments(Stringify& s, const T& container)
    {
        s << "comments:\n";
        for (const auto& c : container)
        {
            s << c << '\n';
        }
    }

    void dump(Stringify& s, const extracted_comments& ec)
    {
        operator_str_comments(s, ec);
    }

    void dump(Stringify& s, const extracted_comments_multiplexed& ecm)
    {
        operator_str_recurse_info(s, "extracted comments multiplexed:\n", ecm);
    }

    void dump(Stringify& s, const extracted_comments_chained& ecc)
    {
        operator_str_recurse_info(s, "extracted comments chained:\n", ecc);
    }

    namespace ogg_generator
    {
        using namespace std::string_literals;

        void generate(const description& in, const path_to_a_file& outputogg, fs::temporary_directory& tempdir)
        {
            ofile dst(outputogg);

            for (const auto& multiplexed : in.cm_streams)
            {
                std::list<fs::temporary_file> temp_simple_oggs;

                for (const auto& stream : multiplexed)
                {
                    const auto fullinputfile = get_full_inputfile(stream);
                    // create tempfile with comments from description
                    fs::temporary_file tempcommentsfile(tempdir.path_to());
                    fill_comments(tempcommentsfile, stream.comments);
                    // create temp.ogg tempfile
                    temp_simple_oggs.emplace_back(tempdir.path_to());
                    const auto& tempogg = temp_simple_oggs.back();
                    // call vorbiscomment with comments to tempfile
                    started_ex<verify_exec_ok>(vorbiscomment_path(), "-w", "-R", "-e", "-c", tempcommentsfile.path_to().str(),
                                               fullinputfile.str(), tempogg.path_to().str());
                }
                fs::temporary_file multiplexed_temp(tempdir.path_to());
                multiplex(temp_simple_oggs, multiplexed_temp);
                // append the newly created file to the final outputogg
                fs::append_file(multiplexed_temp.path_to(), dst);
            }

            // make sure file is closed, so timestamp update caused by final write()
            // will not overwrite the custom values set just after
            dst.close();

            if (in.file_timestamps.has_value())
                set_timestamps(outputogg, in.file_timestamps.value());
        }

        template<bool relaxed_comments_ = true, unsigned int ts_tolerance = 0>
        struct verify_settings
        {
            static constexpr auto relaxed_comments = relaxed_comments_;
            static constexpr auto timestamp_tolerance = std::chrono::operator""s(ts_tolerance);
        };

        template<typename settings>
        void verify_ex(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir)
        {
            try
            {
                // part1 : validate comments

                // extract all comments from inputogg
                const auto all_comments = extract_all_comments(inputogg);
                // validate them against input
                validate_comments(all_comments, in.cm_streams);

                // part2 : generate new file based on template
                // update comments if relaxed==true (thus with new validated comments)
                // then verify same binary content as the input file

                std::unique_ptr<description> description_updated;
                const description* description_for_content_check;

                if constexpr (settings::relaxed_comments)
                {
                    description_updated = std::make_unique<description>(in);
                    // modify copy of description input with newly validated (and thus exactly computed) comments
                    update_comments(all_comments, description_updated->cm_streams);

                    description_for_content_check = description_updated.get();
                }
                else
                {
                    description_for_content_check = &in;
                }
                // call generator with (possibly) updated description
                fs::temporary_file reconstructed_output(tempdir.path_to());
                generate(*description_for_content_check, reconstructed_output.path_to(), tempdir);
                // verify temp file from above is identical to inputogg
                if (!fs::is_same_content(reconstructed_output.path_to(), inputogg))
                {
                    throw std::runtime_error(Stringify() << "'" << inputogg.str() << "' is not identical to regenerated file");
                }

                // part 3: verify timestamps
                if (in.file_timestamps.has_value())
                {
                    using namespace std::chrono_literals;

                    const auto meta_ts = path_to_stat_nofail(inputogg).get_timestamps();
                    const auto& orig_ts = in.file_timestamps.value();

                    if (std::chrono::abs(meta_ts.atime - orig_ts.atime) > settings::timestamp_tolerance ||
                        std::chrono::abs(meta_ts.mtime - orig_ts.mtime) > settings::timestamp_tolerance)
                    {
                        throw std::runtime_error(Stringify()
                                                 << "'" << inputogg.str() << "' has not the specified timestamps: generated "
                                                 << meta_ts.atime.time_since_epoch().count() << "-"
                                                 << meta_ts.mtime.time_since_epoch().count() << " vs expected "
                                                 << orig_ts.atime.time_since_epoch().count() << "-"
                                                 << orig_ts.mtime.time_since_epoch().count());
                    }
                }
            }
            catch (const std::exception& ex)
            {
                throw Exception::Generic(Stringify()
                                         << "Error while verifying '" << inputogg.str() << "' :" << std::string_view(ex.what()));
            }
        }

        void verify_strict(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir)
        {
            verify_ex<verify_settings<false>>(in, inputogg, tempdir);
        }

        void verify(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir)
        {
            verify_ex<verify_settings<true>>(in, inputogg, tempdir);
        }

        void verify_close_enough_ts(const description& in, const path_to_a_file& inputogg, fs::temporary_directory& tempdir)
        {
            verify_ex<verify_settings<true, 60>>(in, inputogg, tempdir);
        }

        void dump(Stringify& s, const meta_filetimestamps& ts)
        {
            const auto dump_tp = [&](const auto& tp) { s << tp.time_since_epoch().count(); };

            s << "atime: ";
            dump_tp(ts.atime);
            s << "\nmtime: ";
            dump_tp(ts.mtime);
            s << '\n';
        }

        void dump(Stringify& s, const stream& str)
        {
            s << "stream:\n";
            s << "input file: " << str.input.filename << '\n';
            operator_str_comments(s, str.comments);
        }

        void dump(Stringify& s, const multiplexed_streams& ms)
        {
            operator_str_recurse_info(s, "multiplexed:\n", ms);
        }

        void dump(Stringify& s, const chained_multiplexed_streams& cm_streams)
        {
            operator_str_recurse_info(s, "ogg_generator chained:\n", cm_streams);
        }
    } // namespace ogg_generator
} // namespace VG::test_lib
