#ifndef VG_TEST_LIB_C_H
#define VG_TEST_LIB_C_H

#include <fcntl.h>
#include <sys/stat.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>

#include "c_calls_core.h"

namespace VG::C
{
    struct forkFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::fork(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg();

        using verifretok = VerifRetNotEqual<int, -1>;
        using handle_error = get_error_msg_and_throw<forkFctor, SystemErrorExceptionFctorHelper>;
    };

    struct execv_exitFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::execv(std::forward<ARGS>(args)...);
        }

        using verifretok = VerifAlways<false>;
        using handle_error = exit_errno;
    };

    struct waitpidFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::waitpid(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(pid_t pid, int* wstatus, int options);

        using verifretok = VerifRetGreaterZeroOrMinusOneAndIntr;
        using handle_error = get_error_msg_and_throw<waitpidFctor, SystemErrorExceptionFctorHelper>;
    };

    struct rmdirFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::rmdir(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname);

        using verifretok = VerifRetNotEqual<int, -1>;
        using handle_error = get_error_msg_and_throw<rmdirFctor, SystemErrorExceptionFctorHelper>;
    };

    struct mkdtempFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::mkdtemp(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* tmplate);

        using verifretok = VerifRetNotEqual<char*, nullptr>;
        using handle_error = get_error_msg_and_throw<mkdtempFctor, SystemErrorExceptionFctorHelper>;
    };

    struct setenvFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::setenv(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* name, const char* value, int overwrite);

        using verifretok = VerifRetNotEqual<int, -1>;
        using handle_error = get_error_msg_and_throw<setenvFctor, SystemErrorExceptionFctorHelper>;
    };

    struct unsetenvFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::unsetenv(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* name);

        using verifretok = VerifRetNotEqual<int, -1>;
        using handle_error = get_error_msg_and_throw<unsetenvFctor, SystemErrorExceptionFctorHelper>;
    };

    struct unlinkFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::unlink(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* filename);

        using verifretok = VerifRetEqual<int, 0>;
        using handle_error = get_error_msg_and_throw<unlinkFctor, SystemErrorExceptionFctorHelper>;
    };

    struct dup2_exitFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::dup2(std::forward<ARGS>(args)...);
        }

        using verifretok = VerifRetGreaterOrEqual<int, 0>;
        using handle_error = exit_errno;
    };

    struct fcntl_exitFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::fcntl(std::forward<ARGS>(args)...);
        }

        using verifretok = VerifRetGreaterEqualZeroOrMinusOneAndIntr;
        using handle_error = exit_errno;
    };

    struct close_exitFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::close(std::forward<ARGS>(args)...);
        }

        using verifretok = VerifRetEqualZeroOrMinusOneAnd<EINTR>;
        using handle_error = exit_errno;
    };

    struct lseekFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::lseek(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(int fildes, off_t offset, int whence);

        using verifretok = VerifRetNotEqual<off_t, -1>;
        using handle_error = get_error_msg_and_throw<lseekFctor, SystemErrorExceptionFctorHelper>;
    };

    struct mkdirFctor
    {
        template<typename... ARGS>
        static auto call(ARGS&&... args) noexcept
        {
            return ::mkdir(std::forward<ARGS>(args)...);
        }

        static std::string getErrorMsg(const char* pathname, mode_t mode);

        using verifretok = VerifRetNotEqual<int, -1>;
        using handle_error = get_error_msg_and_throw<mkdirFctor, SystemErrorExceptionFctorHelper>;
    };

    using fork = mysyscallthrow<forkFctor>;
    using execv_or_exit = mysyscallthrow<execv_exitFctor>;
    using waitpid = mysyscallthrow<waitpidFctor>;
    using rmdir = mysyscallthrow<rmdirFctor>;
    using mkdtemp = mysyscallthrow<mkdtempFctor>;
    using setenv = mysyscallthrow<setenvFctor>;
    using unsetenv = mysyscallthrow<unsetenvFctor>;
    using unlink = mysyscallthrow<unlinkFctor>;
    using dup2_or_exit = mysyscallthrow<dup2_exitFctor>;
    using fcntl_or_exit = mysyscallthrow<fcntl_exitFctor>;
    using close_or_exit = mysyscallthrow<close_exitFctor>;
    using lseek = mysyscallthrow<lseekFctor>;
    using mkdir = mysyscallthrow<mkdirFctor>;
} // namespace VG::C

#endif
