#ifndef VG_TEST_LIB_IMPL_H
#define VG_TEST_LIB_IMPL_H

#include <array>
#include <regex>

#include "stringify.h"
#include "test_lib_fs.h"

namespace VG::test_lib_impl
{
    extern const std::regex found_replaygain_regex;

    template<typename S>
    void write_buffer_to(const std::byte* src, std::size_t len, S& stream)
    {
        stream.write(src, len);
    }

    void write_buffer_to(const std::byte* src, std::size_t len, std::ostream& os);

    template<typename OS>
    void dump_file(const path_to_a_file& file, OS& os)
    {
        ifile src(file);

        while (true)
        {
            std::array<std::byte, 65536> buf;
            std::size_t const one_read = src.read(buf.data(), buf.size());
            if (one_read != 0)
                write_buffer_to(buf.data(), one_read, os);
            else
                break;
        }
        src.close();
    }

    template<bool result>
    struct verify_wstatus
    {
        void operator()(int wstatus)
        {
            const bool ok_wstatus = WIFEXITED(wstatus) && (WEXITSTATUS(wstatus) == 0);
            if (ok_wstatus != result)
                throw std::runtime_error(Stringify() << "child returned with status: " << wstatus);
        }
    };

    struct verify_file_ignore
    {
        void operator()(const fs::temporary_file& file);
    };

    struct verify_file_no_progress
    {
        void operator()(const fs::temporary_file& file);
    };

    template<bool has_found>
    struct verify_file_has_found_replaygain
    {
        void operator()(const fs::temporary_file& file)
        {
            ifile ifs(file.path_to());

            while (true)
            {
                const auto line = ifs.getline();
                if (line.has_value())
                {
                    const auto res = std::regex_search(line.value(), found_replaygain_regex);
                    if (res != has_found)
                        throw std::runtime_error(Stringify() << "file contains \"found RG tags\" regex:" << res);
                    // todo this part above is not clear ???
                }
                else
                {
                    break;
                }
            }
        }
    };

    template<bool want_empty>
    struct verify_file_is_empty
    {
        void operator()(const fs::temporary_file& file)
        {
            struct ::stat stat_file;
            VG::C::stat::call(file.path_to().str().c_str(), &stat_file);
            const bool empty = (stat_file.st_size == 0);
            if (empty != want_empty)
            {
                throw std::runtime_error("file content not as expected");
            }
        }
    };

    template<typename V>
    constexpr void add_vect_str(V&)
    {
    }

    template<typename V, typename ARG, typename... ARGS>
    void add_vect_str(V& v, ARG&& arg, ARGS&&... args)
    {
        v.emplace_back(std::forward<ARG>(arg));
        add_vect_str(v, std::forward<ARGS>(args)...);
    }
} // namespace VG::test_lib_impl
#endif
